package tasks;

import activities.BookListActivity;
import activities.LecteurActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by simou on 28.10.2014.
 *
 */
public class WebcallTask  extends AsyncTask<String, Void, String> {

    private Handler mHandler;
    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;
    
    public WebcallTask (Context context){
        this(context, null);
   }

    public WebcallTask (Context context, Handler handler) {
        weakReferenceContext = new WeakReference<>(context);
        mHandler = handler;
   }

    @Override
    protected String doInBackground(String... JSONurl) {

        Log.d("WCT", JSONurl[0]);

        String webService = "https://webservice.bibliothequesonore.ch/mobile.php";

        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
            webService = "http://webservice.bibliothequesonore.ch/mobile.php";
        }
        try {
            String fullUrl = webService +"?func="+JSONurl[0];
            URL u = new URL(fullUrl);
            Log.d("WCT", "getJson: URL: " + u);
            InputStream is;
            int status;

            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
                HttpURLConnection c = (HttpURLConnection) u.openConnection();

                int timeout = 6000;
                c.setConnectTimeout(timeout);
                c.setReadTimeout(timeout);
                if (getCookie() != null && !getCookie().equals("")) {
                    c.setRequestProperty("Cookie", getCookie());
                }

                c.connect();
                String newCookie = c.getHeaderField("Set-Cookie");
                if ((newCookie != null && !newCookie.equals("")) || JSONurl[0].equals("Authenticate")) {
                    storeCookie(newCookie);
                }
                is = c.getInputStream();
                status = c.getResponseCode();

            } else {
                HttpsURLConnection c = (HttpsURLConnection) u.openConnection();

                int timeout = 6000;
                c.setConnectTimeout(timeout);
                c.setReadTimeout(timeout);
                if (getCookie() != null && !getCookie().equals("")) {
                    c.setRequestProperty("Cookie", getCookie());
                }

                c.connect();
                String newCookie = c.getHeaderField("Set-Cookie");
                if ((newCookie != null && !newCookie.equals("")) || JSONurl[0].equals("Authenticate")) {
                    storeCookie(newCookie);
                }
                status = c.getResponseCode();
                if (status != 200 && status != 201 && status != 202) {
                    Log.d("WCT", "Problème avec requête: " + status);
                    is = c.getErrorStream();
                }else{
                    is = c.getInputStream();
                }

            }
            if (status != 200 && status != 201 && status != 202) {
                Log.d("WCT", "Problème avec requête: " + status);
            }

            switch (status) {
                case 200:
                case 201:
                case 202:
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();
                    Log.d("WCTest", sb.toString());
                    return sb.toString();
                case 400:
                    return "login_failed";
            }
        } catch (MalformedURLException ex) {
            Log.d("WCT", "doInBackground: MalformedURLException");
            System.out.println(ex.toString());

        } catch (IOException ex) {
            Log.d("WCT", "doInBackground: IOException");
            System.out.println(ex.toString());
            ex.printStackTrace();
        } catch (Exception ex) {
            Log.d("WCT", "doInBackground: exception: "+ex.toString());
            ex.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String result) {

        Log.d("WCT", "onPostExecute: "+result);
        if (mHandler != null) {
            Message message = new Message();
            message.what = BookListActivity.WCT;
            message.obj = result;
            mHandler.sendMessage(message);
        } else {
        	 Log.d("WCT", "onPostExecute: handler null");
        }
    }
    
    private void storeCookie(String cookie) {

        Context mContext = weakReferenceContext.get();
        String PREF_TAG = "ANDROID_BSR";
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        editor.putString("cookie", cookie);
		editor.apply();
    }
    
    private String getCookie(){

        Context mContext = weakReferenceContext.get();
    	return mContext.getSharedPreferences(
				LecteurActivity.PREF_TAG, Context.MODE_PRIVATE).getString("cookie", null);
    }
}
