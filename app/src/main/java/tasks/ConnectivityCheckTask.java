package tasks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import androidx.core.content.IntentCompat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.lang.ref.WeakReference;


import activities.HomeActivity;
import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.LoginFragment;
import fragments.MyLibraryFragment;
import utils.Utils;

public class ConnectivityCheckTask extends AsyncTask<String, Void, String> {
    @SuppressLint("StaticFieldLeak")

    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;
    private Boolean refreshLibrary;

    public ConnectivityCheckTask(Context context, Boolean refreshLibrary){

        weakReferenceContext = new WeakReference<>(context);
        this.refreshLibrary = refreshLibrary;
    }

    @Override
    protected String doInBackground(String... urls) {

        Context mContext = weakReferenceContext.get();
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if(activeNetwork != null) {
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    if ((activeNetwork.getState() == NetworkInfo.State.CONNECTED ||
                            activeNetwork.getState() == NetworkInfo.State.CONNECTING) &&
                            isInternet())
                        isConnected = true;
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    if ((activeNetwork.getState() == NetworkInfo.State.CONNECTED ||
                            activeNetwork.getState() == NetworkInfo.State.CONNECTING) &&
                            isInternet())
                        isConnected = true;
                    break;
            }
        }

        if(!isConnected){
            return mContext.getResources().getString(R.string.Connexion_PasConnecte);
        }

        return "";
    }

    @Override
    protected void onPostExecute(String result) {

        Context mContext = weakReferenceContext.get();
        if(result != null && result.equals("")){
            LoginFragment.enableLogin();

            if(refreshLibrary && !MyLibraryFragment.internetOn){
                MyLibraryFragment.update();
            }

            MyLibraryFragment.internetOn = true;
        } else {
            if(!refreshLibrary) {
                Intent intents = new Intent(mContext, HomeActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intents);
            }

            Utils.showToast(mContext, result);
            LoginFragment.disableLogin();
            if(refreshLibrary && MyLibraryFragment.internetOn){
                MyLibraryFragment.update();
            }
            MyLibraryFragment.internetOn = false;

        }
    }

    private static boolean isInternet() {
        try {
            int timeoutMs = 4000;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) { return false; }

    }
}