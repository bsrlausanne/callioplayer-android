package tasks;

import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import activities.DownloadsActivity;
import activities.HomeActivity;
import bibliothequesonore.ch.livresbsrandroid.R;
import services.DownloadService;
import utils.Bibliotheque;
import utils.Utils;

/**
 * Created by simou on 28.10.2014.
 *
 */
public class UncompressTask extends AsyncTask<String, Void, String> {

    //private NotificationManager mNotifManager;
    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;
    private String mDescription;
    public String mBookNr;
    //private NotificationCompat.Builder mBuilder;
    private int id;

    private int entryCount = 0;
    private int entryTotal = 0;

    private final String BOOKS_FOLDER = "/Android/data/bibliothequesonore.ch.livresbsrandroid/files/";
    public UncompressTask(Context context, NotificationManager notifManager, String bookDescription, String bookNr){

        weakReferenceContext = new WeakReference<>(context);
    	//mNotifManager = notifManager;
    	mDescription = bookDescription;
        mBookNr = bookNr;
        id=bookDescription.hashCode();
        Log.d("UCT", "init id: "+id);
    }
   
    @Override
    protected String doInBackground(String... params) {

        Context mContext = weakReferenceContext.get();
        String mSourcePath = params[0];
        String mTargetPath = params[1];

        File zipFile = new File(mSourcePath);
        File targetFile = new File(mTargetPath);

        DownloadsActivity.updateUCT(this.mBookNr, 0);

        try {
            //Log.d("UNC", "unzipping "+zipFile.length());
            unzipAll(zipFile, targetFile);
            boolean wasSuccessful = zipFile.delete();

            if (!wasSuccessful) {
                Log.d("DeleteTask", targetFile.getName() + " was not deleted");
            }

        } catch (IOException e) {

            boolean wasSuccessful = zipFile.delete();

            if (!wasSuccessful) {
                Log.d("DeleteTask", targetFile.getName() + " was not deleted");
            }

            Log.e("UNC", "exception: "+e.toString());
            e.printStackTrace();
        } catch(SecurityException se){
            Log.e("UNC", "exception: "+se.toString());
            Utils.showToast(mContext, "Zip extraction error.");
            se.printStackTrace();
        }

        return "";
    }

    @Override
    protected void onCancelled() {

        Log.d("UCT", "oncancelled: "+id);
        //mNotifManager.cancel(id);
        super.onCancelled();
    }

    protected void onPostExecute(String newFolder) {

        Context mContext = weakReferenceContext.get();
    	/*mBuilder.setContentTitle(mDescription)
        		.setContentText(mContext.getResources().getString(R.string.Notification_LivrePret))
                .setProgress(0, 0, false)
                .setAutoCancel(true);*/
        Bibliotheque.getBibliotheque(mContext).scanLivres();
        HomeActivity.updateAllLists();

        Utils.showToast(mContext, mDescription+" "+mContext.getResources().getString(R.string.Notification_LivrePret));
        DownloadsActivity.updateUCT(this.mBookNr, 100);
        DownloadService.uctFinished(this);
    }

    private void unzipAll(File zipFile, File targetDir) throws IOException {

        Context mContext = weakReferenceContext.get();
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
        ZipEntry zentry;

        while ((zentry = zis.getNextEntry()) != null) {
            if (zentry.getName().contains(".mp3")) {
                entryTotal++;
            }
        }

        zis.close();
        zis = new ZipInputStream(new FileInputStream(zipFile));

        // if exists remove
        if (!targetDir.exists()) {
            boolean wasSuccessful = targetDir.mkdirs();
            if (!wasSuccessful) {
                Log.d("CreateDirTask", targetDir.getName() + " was not created");
            }
        }

        // unzip all entries
        while ((zentry = zis.getNextEntry()) != null) {
            String fileNameToUnzip = zentry.getName();
            File targetFile = new File(targetDir, fileNameToUnzip);

            // if directory
            if (zentry.isDirectory()) {
                boolean wasSuccessful = (new File(targetFile.getAbsolutePath())).mkdirs();
                if (!wasSuccessful) {
                    Log.d("CreateDirTask", new File(targetFile.getAbsolutePath()).getName() + " was not created");
                }
            } else {
                if (zentry.getName().contains(".mp3")) {
                    //mBuilder.setContentTitle(mContext.getResources().getString(R.string.Notification_DecompressionEnCours));
                    //Log.d("DOWNLOADS", (long)entryCount+"/"+(long)entryTotal);
                    DownloadsActivity.updateUCT(this.mBookNr, (long)(100*(float)entryCount/(float)entryTotal));
                    /*mBuilder.setProgress(entryTotal, entryCount, false);
                    mNotifManager.notify(id, mBuilder.build());*/
                    entryCount++;
                }

                // make parent dir
                boolean wasSuccessful = (new File(targetFile.getParent())).mkdirs();
                if (!wasSuccessful) {
                    Log.d("CreateDirTask", (new File(targetFile.getParent())).getName() + " was not created");
                }
                unzipEntry(zis, targetFile);
            }
        }
        zis.close();
    }

    private void unzipEntry(ZipInputStream zis, File targetFile)  throws IOException {
        FileOutputStream fos = new FileOutputStream(targetFile);

        // check for Zip Path Traversal Vulnerability as requested
        if(!targetFile.getCanonicalPath().contains(BOOKS_FOLDER)){
            throw new SecurityException("Zip Path Traversal Vulnerability");
        }

        try{
            byte[] buffer = new byte[1024];
            int len;

            while ((len = zis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } catch (IOException e) {
            Log.e("UCT", "unzipEntry exception: ");
            e.printStackTrace();
            fos.close();
        }
        fos.close();

    }

}
