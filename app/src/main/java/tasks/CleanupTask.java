package tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;


/**
 * Created by simou on 16.06.2016.
 */
public class CleanupTask extends AsyncTask<Void, Void, String> {

    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;

    public CleanupTask(Context context){

        weakReferenceContext = new WeakReference<> (context);
    }

    @Override
    protected String doInBackground(Void... noparams) {

        Context mContext = weakReferenceContext.get();
        File dlFolder = mContext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);

        if(dlFolder == null){
            return "";
        }

        File[] dlFiles = dlFolder.listFiles();
        if(dlFiles == null){
            return "rien à supprimer.";
        }

        for (File dlFile : dlFiles) {
            boolean wasSuccesfull = dlFile.delete();

            if (!wasSuccesfull) {
                return "error while deleting";
            }
        }
        return dlFiles.length +" fichiers vont être supprimés";
    }

    protected void onPostExecute(String result) {

        Log.d("CleanupTask", result);
    }
}
