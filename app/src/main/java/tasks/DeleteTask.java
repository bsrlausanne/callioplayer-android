package tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;

import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Utils;


/**
 * Created by simou on 16.06.2016.
 */
public class DeleteTask extends AsyncTask<String, Void, String> {

    private String mTitle;
    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;

    public DeleteTask(Context context, String title){

        weakReferenceContext = new WeakReference<> (context);
        mTitle = title;
    }

    @Override
    protected String doInBackground(String... JSONurl) {

        File toDelete = new File(JSONurl[0]);

        if(!toDelete.exists()){
            Log.e("DeleteTask", "N'existe pas: "+JSONurl[0]);
        }

        DeleteRecursive(toDelete);

        return toDelete.getName();
    }

    protected void onPostExecute(String result) {

        Context mContext = weakReferenceContext.get();
        Log.d("DeleteTask", result +" a bien été supprimé.");
        Utils.sendTBMessage(mContext, mTitle +" "+mContext.getResources().getString(R.string.Accueil_ABienEteSupprime));
    }

    private static void  DeleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles())
                DeleteRecursive(child);
        }
        boolean wasSuccesfull = fileOrDirectory.delete();

        if (wasSuccesfull) {
            Log.d("DeleteTask", fileOrDirectory.getName() +" a bien été supprimé.");
        } else{
            Log.d("DeleteTask", fileOrDirectory.getName() +" pas supprimé.");
        }
    }
}
