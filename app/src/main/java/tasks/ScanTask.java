package tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.core.content.ContextCompat;

import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import fragments.MyLibraryFragment;
import utils.Bibliotheque;
import utils.LivreDaisy;



/**
 * Created by simou on 16.06.2016.
 */
public class ScanTask extends AsyncTask<Void, Void, String> {

    // use of weakReference for Garbage collector
    private WeakReference<Context> weakReferenceContext;
    private Bibliotheque mBib;

    private boolean mAnyChange = false;

    public ScanTask(Context context, Bibliotheque bib){
        Log.d("LCTSRV", "ScanTask");
        weakReferenceContext = new WeakReference<>(context);
        mBib = bib;
    }


    @Override
    protected String doInBackground(Void... noparams) {
        Context context = weakReferenceContext.get();

        File directory = context.getFilesDir();
        // Log.d("InternalStorage", "folder: " + directory.getAbsolutePath());
        for (File file : directory.listFiles()) {
            // Log.d("InternalStorage", "folder: " + file.toString());
        }
        File[] externalStorageVolumes =
                ContextCompat.getExternalFilesDirs(context, null);

        // necessary, some android versions return null for this
        if(externalStorageVolumes != null) {
            for (File externalStorageVolume : externalStorageVolumes) {
                // this should not be null, but seems like it is sometimes
                if(externalStorageVolume != null && externalStorageVolume.listFiles() != null) {
                    for (File primaryFolder : externalStorageVolume.listFiles()) {
                        boolean isDaisyFolder;
                        if (primaryFolder != null && primaryFolder.listFiles() != null) {
                            // Log.d("ExternalStorage", "folder: " + primaryFolder.toString());
                            for (File bookFolder : primaryFolder.listFiles()) {
                                // Log.d("ScanTask", "En cours: " + bookFolder);
                                isDaisyFolder = false;
                                if (bookFolder.isDirectory()) {
                                    File bookFiles[] = bookFolder.listFiles();
                                    // recherche du ncc.html pour livre daisy
                                    if (bookFiles != null) {
                                        for (File bookFile : bookFiles) {
                                            if (bookFile.getName().equalsIgnoreCase("ncc.html")) {
                                                isDaisyFolder = true;
                                                // Log.d("ScanTask", "Trouvé: " + bookFolder);
                                            }
                                        }
                                    }

                                    // si c'est un livre daisy
                                    if (isDaisyFolder) {
                                        String bookName = bookFolder.getName();
                                        LivreDaisy existingBook = mBib.getLivreById(bookFolder.toString().hashCode());
                                        if (existingBook == null) {
                                            // on mettra à jour le cache
                                            LivreDaisy tempLivre = new LivreDaisy(bookFolder.getAbsolutePath(), mBib.getFiche(bookName));
                                            if (tempLivre.loadedOk() && !mBib.bookInLibraryByNoticeNr(tempLivre.getNoticeNr())) {
                                                Log.d("ScanTask", "Ajout de : " + tempLivre.getNoticeNr() + " à la biblio");
                                                mBib.ajouterLivres(tempLivre);
                                                mAnyChange = true;
                                            } else {
                                                Log.d("ScanTask", "Loaded pas ok: " + bookFolder);
                                                if (bookFolder.delete()) {
                                                    Log.d("ScanTask", "Suppression de " + bookFolder + " ok.");
                                                } else {
                                                    Log.d("ScanTask", "Suppression de " + bookFolder + " pas ok.");
                                                }
                                            }
                                        } else if (existingBook != null && (existingBook.getNoticeNr() == null || existingBook.getNoticeNr().equals(""))) {
                                            // Log.d("parseNoticeNr", "ohoh " + existingBook.getNoticeNr());
                                            existingBook.extractNoticeNr();
                                        } else {
                                            LivreDaisy tempLivre = mBib.getLivreById(bookFolder.toString().hashCode());
                                            // Log.d("ScanTask", tempLivre.getTitle());
                                        }
                                    }
                                }
                            }
                        }
                        Bibliotheque.getBibliotheque(weakReferenceContext.get()).storeLivres();
                    }
                }
            }
        }
        File bookFolders[];

        long startTime = System.currentTimeMillis();
        bookFolders = scanAllMediaFolders();
        long endTime = System.currentTimeMillis();
        // Log.d("ScanTask", "durée scan folders: " + (endTime - startTime) + " milliseconds");

        startTime = System.currentTimeMillis();
        // recherche et ajout des livres daisy pas encore enregistrés dans le cache
        boolean isDaisyFolder;
        if (bookFolders!=null) {
            for (File bookFolder : bookFolders) {
                Log.d("ScanTask", "En cours: " + bookFolder);
                isDaisyFolder = false;
                if (bookFolder.isDirectory()) {
                    File bookFiles[] = bookFolder.listFiles();
                    // Log.d("ScanTask", "En cours: " + bookFolder);
                    // recherche du ncc.html pour livre daisy
                    if (bookFiles != null) {
                        for (File bookFile : bookFiles) {
                            if (bookFile.getName().equalsIgnoreCase("ncc.html")) {
                                isDaisyFolder = true;
                                Log.d("ScanTask", "Trouvé: " + bookFolder);
                            }
                        }
                    }

                    // si c'est un livre daisy
                    if (isDaisyFolder) {
                        String bookName = bookFolder.getName();
                        LivreDaisy existingBook = mBib.getLivreById(bookFolder.toString().hashCode());
                        if ( existingBook == null) {
                            // on mettra à jour le cache
                            LivreDaisy tempLivre = new LivreDaisy(bookFolder.getAbsolutePath(), mBib.getFiche(bookName));
                            if (tempLivre.loadedOk()&& !mBib.bookInLibraryByNoticeNr(tempLivre.getNoticeNr())) {
                                Log.d("ScanTask", "Ajout de : " + tempLivre.getNoticeNr() +" à la biblio");
                                mBib.ajouterLivres(tempLivre);
                                mAnyChange = true;
                            } else{
                                Log.d("ScanTask", "Loaded pas ok: " + bookFolder);
                                if(bookFolder.delete()){
                                    Log.d("ScanTask", "Suppression de " + bookFolder +" ok.");
                                } else{
                                    Log.d("ScanTask", "Suppression de " + bookFolder +" pas ok.");
                                }
                            }
                        } else if(existingBook != null  && ( existingBook.getNoticeNr() == null || existingBook.getNoticeNr().equals(""))){
                            Log.d("parseNoticeNr", "ohoh "+existingBook.getNoticeNr());
                            existingBook.extractNoticeNr();
                        }else{
                            LivreDaisy tempLivre = mBib.getLivreById(bookFolder.toString().hashCode());
                            Log.d("ScanTask", tempLivre.getTitle());
                        }
                    }
                }
            }
        }
        Bibliotheque.getBibliotheque(weakReferenceContext.get()).storeLivres();
        endTime = System.currentTimeMillis();
        Log.d("ScanTask", "durée vérif daisy: " + (endTime - startTime) + " milliseconds");
        return "Bravo";

    }

    private File[] scanAllMediaFolders(){

        Context mContext = weakReferenceContext.get();
        ArrayList<File> result = new ArrayList<>();

        Log.d("ScanTask", "scanning getExternalStorageDirectory: "+Environment.getExternalStorageDirectory());
        checkFolder(Environment.getExternalStorageDirectory(), result);

        // for samsung galaxy a5 for example
        // Log.d("ScanTask", "ContextCompat.getExternalFilesDirs");
        File[] externalDirs = ContextCompat.getExternalFilesDirs(mContext, null);

        for (File externalDir : externalDirs) {
            if (externalDir != null) {
                // Log.d("ScanTask", "scanning external dirs: "+externalDirs[t]);
                String externalSdCardPath = externalDir.getPath().split("/Android")[0];
                //checkFolder(externalDirs[t], result);
                if (externalSdCardPath != null) {
                    File sdCardRootFile = new File(externalSdCardPath);
                    Log.d("ScanTask", "scanning external SD: " + externalSdCardPath);
                    if (sdCardRootFile.canRead()) {
                        checkFolder(sdCardRootFile, result);
                    } else {
                        Log.d("ScanTask", "cannot read: " + externalSdCardPath);
                    }
                }
            }
        }

        final String secStoragePathList = System.getenv("SECONDARY_STORAGE");
        if (secStoragePathList != null) {
            Log.d("ScanTask", "SECONDARY_STORAGE");
            final String[] secStoragePathsArray = secStoragePathList.split(File.pathSeparator);
            for (String aSecStoragePathsArray : secStoragePathsArray) {
                Log.d("ScanTask", "scanning SECONDARY_STORAGE: " + aSecStoragePathsArray);
                checkFolder(new File(aSecStoragePathsArray), result);
            }
        }

        Log.d("ScanTask", "chemin sd: "+Environment.getExternalStorageDirectory());
        File[] temp = new File[result.size()];
        for (int i=0;i<result.size();i++) {
            temp[i] = result.get(i);
        }

        return temp;
    }

    private void checkFolder(File folder, ArrayList<File> result) {

        // Log.d("ScanTask", "checkFolder: "+folder);
        if (folder.toString().contains("/Android/data/") && !folder.toString().contains("bibliothequesonore.ch")) {
            //Log.d("ScanTask", "data pas bibliotheque");
            return;
        }
        if(folder.toString().contains("/Android/media/") || folder.toString().contains("/Pictures/")){
            // whatsapp folder, ignore
            return;
        }
        // Log.d("ScanTask", "En cours2: " + folder);
        File[] filesInFolder = folder.listFiles();

        File okFiles[] = folder.listFiles(audioFilter);
        if (okFiles == null) {
            //Log.d("ScanTask", "pas ok: "+okFiles.length);
            return; }

        for (File okFile : okFiles) {
            //Log.d("ScanTask", "ok: "+okFiles.length);
            result.add(okFile.getParentFile());

        }

        if (filesInFolder != null) {
            for (File aFilesInFolder : filesInFolder) {
                if (aFilesInFolder.isDirectory()) {
                    checkFolder(aFilesInFolder, result);
                }
            }
        }
    }

    private FilenameFilter audioFilter = new FilenameFilter() {
        public boolean accept(File dir, String name) {

            return name.equalsIgnoreCase("ncc.html") && !dir.toString().contains("/recycle/");
        }
    };

    protected void onPostExecute(String result) {

        mBib.storeLivres();
        MyLibraryFragment.update();
    }
}
