package services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ServiceStartNotAllowedException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.media.AudioManager;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.support.v4.media.MediaBrowserCompat;

import androidx.media.MediaBrowserServiceCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import activities.LecteurActivity;
import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Bibliotheque;
import utils.Chapitre;
import utils.LivreDaisy;
import utils.NotificationReceiver;
import utils.Track;
import utils.Utils;

import static android.app.Notification.VISIBILITY_PUBLIC;
import static android.content.pm.ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK;
import static androidx.core.app.NotificationCompat.Builder;
import static androidx.core.app.NotificationCompat.PRIORITY_DEFAULT;
import static androidx.core.app.NotificationCompat.PRIORITY_LOW;

public class LecteurService extends MediaBrowserServiceCompat implements AudioManager.OnAudioFocusChangeListener {

    private static LocalBroadcastManager localBM;
    public static boolean isPlaying = false;
	public static boolean isRunning = false;
    private Context mContext;
    public final static String PREF_TAG = "ANDROID_BSR";

    private static UpdateUITask updateTask;
    // longueur de texte max pour la notification
	private final static int NOTIFICATION_TEXT_LENGTH = 73;

	private static LivreDaisy mCurrentBook;
	private static String mCurrentChapterId = "";

    private Bibliotheque mBib;

	private static boolean initialized = false;
    private static AudioManager am;
    private static boolean hasAudioFocus = false;

    private Track mTrack = null;

    private int counterForward = 0;
    private int counterBackward = 0;
    private Timer timerSkipForward;
    private Timer timerSkipBackward;
    private TimerTask timerTaskForward;
    private TimerTask timerTaskBackward;
    private boolean timerForwardRunning = false;
    private boolean timerBackwardRunning = false;

    private boolean wasPlayingBeforeFocusLoss = false;

    public static String CHANNEL_ID = "callio_ch_01";
    public static CharSequence name = "callio_ch";
    public static String Description = "CallioPlayer Channel";

	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

	    @Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case "play":

                        if (mCurrentBook != null){
                            mCurrentBook.updatePlayDate();
                        }

                        play();
                        break;
                    case "pause":
                        pause();
                        break;
                    case "unconditional_play":
                        if (mCurrentBook != null) mCurrentBook.updatePlayDate();
                        unconditional_play();
                        break;
                    case "jumpNext":
                        listenerSkipForw(mCurrentBook.getJumpSize());
                        break;
                    case "jumpPrev":
                        listenerSkipBack(mCurrentBook.getJumpSize());
                        break;
                    case "closeApp":
                        Log.d("NOTIFF", "case closeApp" );
                        if(isPlaying) pause();
                        stopForeground(true);
                        //System.exit(0);
                        stopSelf();
                        break;
                    case "next":
                        playNextChapter();
                        break;
                    case "prev":
                        playPreviousChapter();
                        break;
                    case "end_of_file":
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                        }
                        nextFile();
                        break;
                    case "goto_chapter":
                        String chapterId = intent.getStringExtra("new_chapter");
                        playChapter(mCurrentBook.getChapitreById(chapterId));
                        break;
                    case "goto_position":
                        long position = Long.parseLong(intent.getStringExtra("new_chapter"));
                        mCurrentBook.setAbsolutePosition(position);
                        playCurrentChapter(true);
                        break;
                    case "update_setting":
                        updateSetting(intent);
                        break;
                    case "hide_notif":
                        Log.d("NOTIFF", "CASE hide_notif");
                        hideNotification(context);
                        break;
                    case "update_service":
                        updateService();
                        break;
                    case "stop_service":
                        stopSelf();
                        break;
                    default:
                        throw new IllegalStateException("LecteurService onReceive bizarre: " + action);
                }
            }
        }
    };

    private void unconditional_play() {
        if(!isTrackPlaying()){
            play();
        }
    }

    private static CountDownTimer cDT;
	private static boolean cDTActive = false;
	private static int cDTDuration = 0;

	static Notification notification;

	private void updateSetting(Intent intent) {

		String command = intent.getStringExtra("update_setting");
        switch (command) {
            case "speed":
                Log.d("LecteurService", "updatesetting speed");
                float newSpeed = intent.getFloatExtra("new_value", 1.0f);

                if (mTrack != null) {
                    mTrack.setPlaybackSpeed(newSpeed);
                }
                break;
            case "tonality":
                float newPitch = intent.getFloatExtra("new_value", 1.0f);

                if (mTrack != null) {
                    mTrack.setPlaybackPitch(newPitch);
                }
                break;
            case "sleep":
                int newDuration = intent.getIntExtra("new_value", 0);
                if (newDuration == 0) {
                    cDTActive = false;
                    cDTDuration = 0;
                    resetSnoozeCD(false);
                } else {
                    cDTActive = true;
                    cDTDuration = 1000 * 60 * newDuration;

                    resetSnoozeCD(isPlaying);
                }
                break;
        }
        Log.d("LCT", "things from  updatesetting");
		updateThings(false);
	}

    @Override
    public void onAudioFocusChange(int focusChange) {
	    if (!isRunning){ return; }
        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
            Log.d("AUDFOCUS", "AUDIOFOCUS_LOSS_TRANSIENT");// Pause playback because your Audio Focus was
            if (isPlaying) {
                wasPlayingBeforeFocusLoss = true;
                pause();
            }
            //am.abandonAudioFocus(afChangeListener);
        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            Log.d("AUDFOCUS", "AUDIOFOCUS_LOSS");
            if (isPlaying) {
                wasPlayingBeforeFocusLoss = true;
                pause();
            }
            am.abandonAudioFocus(this);
            hasAudioFocus = false;
        } else if (focusChange ==
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
            Log.d("AUDFOCUS", "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
            if (isPlaying) {
                wasPlayingBeforeFocusLoss = true;
                pause();
            }
            //am.abandonAudioFocus(afChangeListener);
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            Log.d("AUDFOCUS", "AUDIOFOCUS_GAIN");
            if (wasPlayingBeforeFocusLoss) {
                play();
            }
        } else {
            Log.d("AUDFOCUS", "pas trouvé");
        }
    }

    @Override
    public void onCreate() {
	    // isRunning is set from lecteurActivity before starting that service
        if(!isRunning){
            Log.d("LCTSRV", "trop tôt!");
            stopSelf();
            return;
        }

        if (mBib == null) {
            mBib = Bibliotheque.getBibliotheque(this);
        }
        mCurrentBook = mBib.getActiveLivre();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            MediaSession ms = new MediaSession(getApplicationContext(), getPackageName());
            ms.setCallback(new MediaSession.Callback() {
                @Override
                public boolean onMediaButtonEvent(Intent mediaButtonIntent) {

                    Log.d("Bluetooth", "action");
                    return super.onMediaButtonEvent(mediaButtonIntent);
                }

                public void onPause() {

                    Log.d("Bluetooth", "pause");
                    play();
                    super.onPause();
                }

                public void onPlay() {

                    Log.d("Bluetooth", "play");
                    play();
                    super.onPlay();
                }

                public void onSkipToNext() {

                    Log.d("Bluetooth", "jumpForward");
                    jumpForward(mCurrentBook.getJumpSize());
                    super.onSkipToNext();
                }

                public void onSkipToPrevious() {

                    Log.d("Bluetooth", "jumpBackward");
                    jumpBackward(mCurrentBook.getJumpSize());
                    super.onSkipToPrevious();
                }

            });

            PlaybackState state = new PlaybackState.Builder()
                    .setActions(PlaybackState.ACTION_PLAY | PlaybackState.ACTION_PLAY_PAUSE |
                            PlaybackState.ACTION_PAUSE | PlaybackState.ACTION_SKIP_TO_NEXT | PlaybackState.ACTION_SKIP_TO_PREVIOUS)
                    .setState(PlaybackState.STATE_STOPPED, 0, 1)
                    .build();

            ms.setPlaybackState(state);
            ms.setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS |
                    MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
            ms.setActive(true);

        }
        updateNotification();

    }

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
        String action = "";
        if (intent != null ) {
            action = intent.getStringExtra("action");
        }

        mContext = getApplicationContext();
		localBM = LocalBroadcastManager.getInstance(this);

		if (!initialized) {

            IntentFilter filter = new IntentFilter("play");
            filter.addAction("pause");
            filter.addAction("unconditional_play");
            filter.addAction("next");
            filter.addAction("prev");
            filter.addAction("jumpPrev");
            filter.addAction("jumpNext");
            filter.addAction("goto_chapter");
            filter.addAction("goto_position");
            filter.addAction("update_setting");
            filter.addAction("end_of_file");
            filter.addAction("update_service");
            filter.addAction("stop_service");
            filter.addAction("closeApp");
            filter.addAction("hide_notif");


            localBM.registerReceiver(mMessageReceiver, filter);

            am = (AudioManager) getSystemService(AUDIO_SERVICE);

            initialized = true;
        }

        updateService();

        if (action != null) {
            if (action.equals("play")) {
                play();
            }
            if (action.equals("unconditional_play")) {
                unconditional_play();
            }
            if (action.equals("prev")) {
                playPreviousChapter();
            }
            if (action.equals("next")) {
                playNextChapter();
            }
            if (action.equals("jumpPrev")) {
                if (mCurrentBook != null) {
                    jumpBackward(mCurrentBook.getJumpSize());
                }
            }
            if (action.equals("jumpNext")) {
                if (mCurrentBook != null) {
                    jumpForward(mCurrentBook.getJumpSize());
                }
            }
            if (action.equals("goto_chapter")) {
                String chapterId = intent.getStringExtra("new_chapter");
                playChapter(mCurrentBook.getChapitreById(chapterId));
            }
            if (action.equals("goto_position")) {
                long position = Long.parseLong(intent.getStringExtra("new_chapter"));
                mCurrentBook.setAbsolutePosition(position);
                playCurrentChapter(true);
            }
        }

		return(START_NOT_STICKY);
	}

    public void updateService() {
        if (mBib == null) {
            mBib = Bibliotheque.getBibliotheque(this);
        } else {
            if (mCurrentBook != null && mCurrentBook != mBib.getActiveLivre()) {
                resetSnoozeCD(false);
                pause();
                if (mTrack != null) {
                    mTrack.release();
                    mTrack = null;
                }
            }
        }

        mCurrentBook = mBib.getActiveLivre();
        updateNotification();
    }

	public static void sendLocalBroadCast() {

		Intent intent = new Intent("update_ui");
		localBM.sendBroadcast(intent);
	}

	public void play() {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.d("brouz", "areNotificationsEnabled: "+notificationManager.areNotificationsEnabled());
        }

        if (isTrackPlaying()) {
			pause();
		} else {
            Log.d("AUDFOCUS", "hasaudiofocus: "+hasAudioFocus);
            if (!hasAudioFocus && am != null) {
                int result = am.requestAudioFocus(this,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    hasAudioFocus = true;
                    playCurrentChapter(true);
                    Log.d("AUDFOCUS", "audiofocus granted");
                } else {
                    Log.d("AUDFOCUS", "audiofocus failed");
                }
            } else {
                playCurrentChapter(true);
            }
		}

        mCurrentBook.updatePlayDate();
	}

	public void pause() {

        if (cDT != null && cDTActive) {
            cDT.cancel();
            cDT = null;
        }

	    if (mCurrentBook == null) {
            return;
        }

        mCurrentBook.setPlaying(false);

        if (mTrack != null) {
            mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());
            // parfois ça plante sur le stop (illegalstate), mais je ne sais pas comment tester cet état.
            try {
                mTrack.stop();
            } catch(Exception e) {
                e.printStackTrace();
            }

            mTrack.release();
            mTrack = null;
        }

        isPlaying = false;
        updateThings(false);
        mBib.storeLivres();

        /*if (stopService) {
            stopSelf();
        }*/

        if (updateTask != null) {
            updateTask.cancel();
            updateTask = null;
        }

        // am.abandonAudioFocus(afChangeListener);

	}

	public void listenerSkipForw (final int jumpLength) {

	    if (!timerForwardRunning) {
	        counterForward = 0;
        }

        counterForward ++;

	    if (timerTaskForward != null) {
            timerTaskForward.cancel();
        }

	    if (timerSkipForward != null) {
            timerSkipForward.cancel();
            timerSkipForward.purge();
        }

        timerTaskForward = new TimerTask() {
            private volatile Thread thread;

            @Override
            public void run() {
                thread = Thread.currentThread();
                timerForwardRunning = true;
                try {
                    Thread.sleep(1000);
                    jumpForward(jumpLength * counterForward);
                    timerForwardRunning = false;
                } catch (Exception e) {
                    // do nothing, as it is thrown when interrupt thread while sleeping
                }
            }

            public boolean cancel() {
                Thread thread = this.thread;
                if (thread != null) {
                    thread.interrupt();
                }
                return super.cancel();
            }
        };

        timerSkipForward = new Timer();
        timerSkipForward.schedule(timerTaskForward, 0);
    }

    public void listenerSkipBack (final int jumpLength) {

        if (!timerBackwardRunning) {
            counterBackward = 0;
        }

        counterBackward ++;

        if (timerTaskBackward != null) {
            timerTaskBackward.cancel();
        }

        if (timerSkipBackward != null) {
            timerSkipBackward.cancel();
            timerSkipBackward.purge();
        }

        timerTaskBackward = new TimerTask() {
            private volatile Thread thread;

            @Override
            public void run() {
                thread = Thread.currentThread();
                timerBackwardRunning = true;
                try {
                    Thread.sleep(1000);
                    jumpBackward(jumpLength * counterBackward);
                    timerBackwardRunning = false;
                } catch (Exception e) {
                    // do nothing, as it is thrown when interrupt thread while sleeping
                }
            }

            public boolean cancel() {
                Thread thread = this.thread;
                if (thread != null) {
                    thread.interrupt();
                }
                return super.cancel();
            }
        };

        timerSkipBackward = new Timer();
        timerSkipBackward.schedule(timerTaskBackward, 0);
    }

    public void jumpForward (int jumpLength) {

        long bookDuration = mCurrentBook.getDuration();
        long newPosition = mCurrentBook.getCurrentPosition() + jumpLength*1000;
        if (newPosition < bookDuration){
            mCurrentBook.setAbsolutePosition(newPosition);
        } else{
            return;
        }
        if (isTrackPlaying()) {
            playCurrentChapter(true);
            updateThings(true);
        } else {
            updateThings(true);
        }
    }

    public void jumpBackward(int jumpLength) {

        long newPosition = mCurrentBook.getCurrentPosition() - jumpLength*1000;

        if (newPosition<0) {
            mCurrentBook.setAbsolutePosition(0);
        } else {
            mCurrentBook.setAbsolutePosition(newPosition);
        }

        if (isTrackPlaying()) {
            playCurrentChapter(true);
            updateThings(true);
        } else {
            updateThings(true);
        }
    }

    public void nextFile(){
        playNextChapter(true);
    }

    public void playNextChapter() {

        playNextChapter(false);
    }

	public void playNextChapter(boolean ignoreLevel) {
        Chapitre nextChapter = mCurrentBook.getNextChapitre(ignoreLevel);
        if (nextChapter == null) {
            if (mTrack != null && !mTrack.isPlaying()) {
                endOfLivre();
            }
            return;
        }
        mCurrentBook.setCurrentChapitreId(nextChapter.getId());

		if (isPlaying) {
            playChapter(nextChapter);
		}

        updateThings(false);
	}

	public void playPreviousChapter() {

        if (mTrack != null && mTrack.getCurrentPosition() > 3000){
            playChapter(mCurrentBook.getCurrentChapitre());
            return;
        }

        Chapitre prevChapter = mCurrentBook.getPreviousChapitre();
        if (prevChapter == null) {
            Utils.sendTBMessage(mContext, mContext.getResources().getString(R.string.Lecteur_DejaDebut));
            return;
        }

        mCurrentBook.setCurrentChapitreId(prevChapter.getId());

        if (isPlaying) {
            playChapter(prevChapter);
        }

        updateThings(true);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return null;
	}

    @Override
    public void onTaskRemoved(Intent rootIntent) {
	    stopSelf();
    }

	@Override
	public void onDestroy() {
        initialized = false;

        if (mTrack != null) {
            mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());
            try {
                mTrack.stop();
                mTrack.release();
            } catch (IllegalStateException iSE) {
                // plantage sur un track non initialisé
                iSE.printStackTrace();
            }
        }

        if (mBib != null) {
            mBib.storeLivres();
        }

        //theService = null;
        if (updateTask != null) {
            updateTask.cancel();
            updateTask = null;
        }

        mTrack = null;
        if(am!=null) {
            am.abandonAudioFocus(this);
        }

        try {
            localBM.unregisterReceiver(mMessageReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        isRunning = false;
        isPlaying = false;
        hideNotification(this);
        stopForeground(true);
		super.onDestroy();
	}

	public void endOfLivre() {

        mCurrentBook.resetPosition();
        mCurrentBook.setPlaying(false);

        updateThings(false);
        hideNotification(this);
        stopSelf();

	}

    public void playCurrentChapter(boolean skipToNewPosition) {

        if (!skipToNewPosition) {
            if (mTrack == null) {
                playChapter(mCurrentBook.getCurrentChapitre());
            } else {
                mCurrentBook.setPlaying(true);
                mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());
            }
        } else {
            if(mCurrentBook.getRelativePosition()<500){
                playChapter(mCurrentBook.getCurrentChapitre());
            } else {
                playChapter(mCurrentBook.getCurrentChapitre(), mCurrentBook.getRelativePosition() - 500);
            }
        }

        updateThings(false);

        if(cDTActive && cDT == null) {
            Log.d("Snooze", "snooze départ");
            startSnoozeCD();
        }
    }

    public void playChapter(Chapitre chapter) {

        playChapter(chapter, 0);
    }

	public void playChapter(Chapitre chapter, long posInMs) {
        mCurrentBook.setPlaying(true);

        if (!mCurrentChapterId.equals(chapter.getId()) || mTrack == null) {
            mCurrentBook.setCurrentChapitreId( chapter.getId());
            mCurrentChapterId = chapter.getId();
            String mp3Path = chapter.getAudioFile();
            File mp3File = new File(mp3Path);
            if (!(mp3File.exists())) {
                Utils.showToast(mContext, mContext.getResources().getString(R.string.Lecteur_ErreurFichierManquant));
                Log.e("LecteurService", "Fichier manquant: " + mp3Path);
                return;
            }

            final Uri uri = Uri.parse(mp3Path);
            if (mTrack != null) {
                try {
                    mTrack.stop();
                    mTrack.release();
                } catch (Exception e){
                    Log.e("LecteurService", "IllegalStateException uninitialized AudioTrack");
                }
            }
            mTrack = new Track(mContext);
            mTrack.setDataSourceUri(uri);
            mTrack.prepare();
        }
        mTrack.setPlaybackSpeed(mCurrentBook.getSpeed());
        mTrack.setPlaybackPitch(mCurrentBook.getPitch());

        // Log.d("Position", "pos: " + posInMs);

        mTrack.seekTo((int) posInMs);
        mTrack.start();
        isPlaying = true;

        if (updateTask==null) {

            updateTask = new UpdateUITask();
            Timer timer = new Timer(true);
            timer.scheduleAtFixedRate(updateTask, 0, 3000);
        }
        updateThings(false);
	}

	public void initNotification() {
		RemoteViews remoteViews = new RemoteViews(getPackageName(),
				R.layout.minilecteur);

		String strtitle = "";
		String strtext = "";

		Intent defaultIntent = new Intent(this.getApplicationContext(),
				LecteurActivity.class);
		defaultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent pendingDefaultIntent = PendingIntent.getActivity(this,
                100, defaultIntent, PendingIntent.FLAG_IMMUTABLE);

		Intent toggleIntent = new Intent(
				getApplicationContext(), NotificationReceiver.class);
		toggleIntent.setAction("ch.bsr.lecteurdaisyandroid.ACTION_PLAY");
		PendingIntent pendingToggleIntent = PendingIntent.getBroadcast(this,
                0, toggleIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);


		remoteViews.setOnClickPendingIntent(R.id.playButton,
				pendingToggleIntent);

		Intent forwardIntent = new Intent(
                getApplicationContext(), NotificationReceiver.class);
		forwardIntent.setAction("ch.bsr.lecteurdaisyandroid.ACTION_NEXT");
		PendingIntent pendingFwdIntent = PendingIntent.getBroadcast(this,
                100, forwardIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

		Intent backIntent = new Intent(
                getApplicationContext(), NotificationReceiver.class);
		backIntent.setAction("ch.bsr.lecteurdaisyandroid.ACTION_PREV");
		PendingIntent pendingBackIntent = PendingIntent.getBroadcast(this,
                100, backIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        remoteViews.setOnClickPendingIntent(R.id.skipBackward,
                pendingBackIntent);

        remoteViews.setOnClickPendingIntent(R.id.skipForward,
                pendingFwdIntent);

        Intent closeIntent = new Intent(
                getApplicationContext(), NotificationReceiver.class);
        closeIntent.setAction("ch.bsr.lecteurdaisyandroid.ACTION_CLOSE");
        PendingIntent pendingCloseIntent = PendingIntent.getBroadcast(this,
                100, closeIntent, PendingIntent.FLAG_IMMUTABLE);

        remoteViews.setOnClickPendingIntent(R.id.close,
                pendingCloseIntent);

        Builder builder = new Builder(this, CHANNEL_ID)
		// Set Icon
		.setSmallIcon(R.drawable.logo_trans)
        .setShowWhen(false)
		// Dismiss Notification
		.setAutoCancel(false)
        //.setStyle(new NotificationCompat.DecoratedCustomViewStyle())
		// Set RemoteViews into Notification
		.setContent(remoteViews)
		.setContentIntent(pendingDefaultIntent)
        .setPriority(PRIORITY_LOW)
        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        .setDeleteIntent(pendingCloseIntent).setAutoCancel(true);


		// Locate and set the Text into customnotificationtext.xml TextViews
		remoteViews.setTextViewText(R.id.bookTitle,strtitle);
		remoteViews.setTextViewText(R.id.currentChapter,strtext);
		 
		notification = 	builder.build();

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                // Android 14 (API 34) and above
                startForeground(
                        2,
                        notification,
                        FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                );
            } else {
                // For Android 13 and below
                startForeground(2, notification);
            }
        } catch (Exception e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (e instanceof ServiceStartNotAllowedException) {
                    // Handle the case where the service can't be started
                    stopSelf();
                } else {
                    throw e;
                }
            }
        }
    }

	public void updateNotification() {
	    if(mCurrentBook == null){
	        return;
        }

		if (notification == null) {
			initNotification();
		}
		Log.d("Notifiction", "isplaying: "+isPlaying);
		NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		RemoteViews remoteView = notification.contentView;

        remoteView.setTextViewText(R.id.bookTitle,
                Utils.truncate(mCurrentBook.getTitle(), NOTIFICATION_TEXT_LENGTH));

		if (isPlaying) {
			remoteView.setImageViewResource(R.id.playButton, R.drawable.ic_pause_black_48dp);
        } else {
			remoteView.setImageViewResource(R.id.playButton, R.drawable.ic_play_arrow_black_48dp);
        }

		remoteView.setTextViewText(
				R.id.currentChapter,
				Utils.truncate(mCurrentBook.getCurrentChapitreTitle(),NOTIFICATION_TEXT_LENGTH));
        if (notificationManager != null) {
            notificationManager.notify(2, notification);
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                // Android 14 (API 34) and above
                startForeground(
                        2,
                        notification,
                        ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                );
            } else {
                // For Android 13 and below
                startForeground(2, notification);
            }
        } catch (Exception e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (e instanceof ServiceStartNotAllowedException) {
                    // Handle the case where the service can't be started
                    stopSelf();
                } else {
                    throw e;
                }
            }
        }
    }

	private void startSnoozeCD() {

		if (cDTActive) {
			if (cDT == null) {
				cDT = new CountDownTimer(cDTDuration, 2000) {

					public void onTick(long millisUntilFinished) {
                        //Log.d("Snooze", "tick");
                    }

					public void onFinish() {
                        Log.d("Snooze", "snooze terminé, mise en pause.");
                        Utils.showToast(mContext, mContext.getResources().getString(R.string.Lecteur_MiseEnSommeil));
						cDTActive = false;
                        SharedPreferences prefs = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("sleepDuration", 0);
                        editor.apply();
                        pause();
                    }
				}.start();
			}
		}
	}

	public void resetSnoozeCD(boolean restart) {

        SharedPreferences prefs = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        int duration = prefs.getInt("sleepDuration", 0);
        if (duration > 0) {
            cDTActive = true;
            cDTDuration = duration*60*1000;
        }

        if (cDT != null) {
			cDT.cancel();
			cDT = null;
		}

		if (restart && cDTActive) {
			startSnoozeCD();
		}
	}

    private void updateThings(boolean resetSnooze) {

        if (mTrack != null) {
            mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());
        }

        sendLocalBroadCast();
        updateNotification();

        if (resetSnooze) {
            resetSnoozeCD(true);
        }
    }

    public void updateProgress() {
        if (isPlaying && mCurrentBook != null) {
            mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());mCurrentBook.setRelativePosition(mTrack.getCurrentPosition());
            sendLocalBroadCast();
        }
    }

    public boolean isTrackPlaying() {
        return mTrack != null && mTrack.isPlaying();
    }

    public static void hideNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    //Not important for general audio service, required for class
    @Override
    public BrowserRoot onGetRoot(String clientPackageName, int clientUid, Bundle rootHints) {

	    if (TextUtils.equals(clientPackageName, getPackageName())) {
            return new BrowserRoot(getString(R.string.app_name), null);
        }

        return null;
    }

    //Not important for general audio service, required for class
    @Override
    public void onLoadChildren( String parentId,  Result<List<MediaBrowserCompat.MediaItem>> result) {

	    result.sendResult(null);
    }

    private class UpdateUITask extends TimerTask {

        @Override
        public void run() {
            completeTask();
        }

        private void completeTask() {
            if (LecteurService.isPlaying) {
                updateProgress();
            }
        }
    }
}
