package services;

import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.MyLibraryFragment;
import tasks.UncompressTask;
import activities.DownloadsActivity;
import utils.Bibliotheque;
import utils.DownloadData;
import utils.Utils;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import androidx.core.app.ServiceCompat;
import android.app.ServiceStartNotAllowedException;
import android.content.pm.ServiceInfo;

public class DownloadService extends Service {

    private static DownloadManager mDm;
    private static BroadcastReceiver mReceiver;
    private static ArrayList<String> mCurrentDl = new ArrayList<>();
    private static ArrayList<UncompressTask> mCurrentUCT = new ArrayList<>();
    private static DownloadService theService;
    private Bibliotheque mBib;
    private Notification mNotification;
    private static NotificationManager mNotificationManager;

    public static void uctFinished(UncompressTask uct) {
        mCurrentUCT.remove(uct);
        if(mCurrentDl.isEmpty() && mCurrentUCT.isEmpty()){
            MyLibraryFragment.disableDownloadsButton();
            theService.stopSelf();
        }
    }
    public static void stopService(){
        theService.stopForeground(true);
        theService.stopSelf();

    }
    public static void cancelUCT(String bookNr) {

        for(int i = 0 ; i<mCurrentUCT.size();i++){
            UncompressTask uct = mCurrentUCT.get(i);
            if(uct.mBookNr.equals(bookNr)) {
                uct.cancel(true);

                if (mCurrentDl.isEmpty() && mCurrentUCT.isEmpty()) {
                    MyLibraryFragment.disableDownloadsButton();
                }
                return;
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("DownloadService", "onStartCommand");

        if (intent == null) {
            if(mCurrentDl.size()==0 && mCurrentUCT.size()==0) {
                stopSelf();
                return START_NOT_STICKY ;
            }
        } else {


            String zipUrl = intent.getStringExtra("zipUrl");
            if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
                zipUrl = zipUrl.replace("https", "http");
                Log.d("SSLHACK", zipUrl );
            }

            String noticeNr = intent.getStringExtra("noticeNr");
            String description = intent.getStringExtra("description");

            mCurrentDl.add(noticeNr);
            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(zipUrl));

            Log.d("getExternalFilesDir", getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getFreeSpace() + " " + mBib.getFreeSpace());
            request.setDestinationInExternalFilesDir(getApplicationContext(), Environment.DIRECTORY_DOWNLOADS, noticeNr + ".zip");
            request.allowScanningByMediaScanner();

            request.setDescription(noticeNr);
            request.setTitle(description);

            MyLibraryFragment.enableDownloadsButton();

            long id = mDm.enqueue(request);
            DownloadsActivity.downloads.add(new DownloadData(id, description, noticeNr, 0));
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.d("DownloadService", "onCreate");
        theService = this;

        mDm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        mBib = Bibliotheque.getBibliotheque(this);


        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                // marche pas tout à fait (après une annulation)
                long downloadId = intent.getLongExtra(
                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);

                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(downloadId);
                    Cursor c = mDm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
                        Log.d("uct", "status: "+c
                                .getInt(columnIndex));

                        if (DownloadManager.STATUS_SUCCESSFUL == c
                                .getInt(columnIndex)) {

                                    /*String fileName = c
                                            .getString(c
                                                    .getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME)); */

                            String downloadFileLocalUri = "";
                            if (c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI) > 0) {
                                int index = c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
                                downloadFileLocalUri = c.getString(index);
                            }

                            File mFile = new File(Uri.parse(downloadFileLocalUri).getPath());
                            String fileName = mFile.getAbsolutePath();

                            String description = "";
                            if (c.getColumnIndex(DownloadManager.COLUMN_TITLE) > 0) {
                                int index = c.getColumnIndex(DownloadManager.COLUMN_TITLE);
                                description = c.getString(index);
                            }

                            String noticeNr = "";
                            if (c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION) > 0) {
                                int index = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
                                noticeNr = c.getString(index);
                            }

                            String destString = Bibliotheque.getBookFolder();

                            UncompressTask uct = new UncompressTask(getApplicationContext(), (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE), description, noticeNr);
                            uct.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, fileName, destString);

                            mCurrentUCT.add(uct);
                            mCurrentDl.remove(noticeNr);
                            Log.d("uct", "action: "+mCurrentDl.size()+"/"+mCurrentUCT.size());
//                                    if (mCurrentDl.size() == 0 && mCurrentUCT.size() == 0) {
                            if (mCurrentDl.size() == 0) {
                                stopForeground(true);
                                stopSelf();
                            }

                        } else if (DownloadManager.ERROR_INSUFFICIENT_SPACE == c.getInt(columnIndex)) {
                            String noticeNr = "";
                            if (c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION) > 0) {
                                int index = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
                                noticeNr = c.getString(index);
                            }

                            mCurrentDl.remove(noticeNr);
                            Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_PasAssezEspace));
                            Log.e("DownloadService", "Erreur de téléchargement: pas assez d'espace: " + noticeNr);
                            if (mCurrentDl.size() == 0) {
                                Log.d("uct", "action: victoire");
                                stopForeground(true);
                                stopSelf();
                            }

                        } else {
                            String noticeNr = "";
                            if (c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION) > 0) {
                                int index = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
                                noticeNr = c.getString(index);
                            }

                            mCurrentDl.remove(noticeNr);

                            Log.e("DownloadService", "Erreur de téléchargement: autre raison: " + noticeNr);
                            if (mCurrentDl.size() == 0) {
                                Log.d("uct", "action: victoire");
                                stopForeground(true);
                                stopSelf();
                            }

                        }
                    } else{
                        Log.d("uct", "not c.moveToFirst(): "+mCurrentDl.size()+"/"+mCurrentUCT.size());
                        if(mCurrentDl.size() == 0) {
                            stopForeground(true);
                            stopSelf();
                        }

                    }
                    c.close();
                } else if (DownloadManager.ACTION_NOTIFICATION_CLICKED.equals(action)) {

                    Intent dlIntent = new Intent(getBaseContext(), DownloadsActivity.class);
                    dlIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    startActivity(dlIntent);
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationContext().registerReceiver(mReceiver, new IntentFilter(
                    DownloadManager.ACTION_DOWNLOAD_COMPLETE), RECEIVER_EXPORTED);
        }else{
            registerReceiver(mReceiver, new IntentFilter(
                    DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationContext().registerReceiver(mReceiver, new IntentFilter(
                    DownloadManager.ACTION_NOTIFICATION_CLICKED), RECEIVER_EXPORTED);
        }else{
            registerReceiver(mReceiver, new IntentFilter(
                    DownloadManager.ACTION_NOTIFICATION_CLICKED));
        }

        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "callio_channel_01";
        CharSequence name = "callio_channel";
        String Description = "CallioPlayer Channel";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.setShowBadge(false);
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(mChannel);
        }
        Intent defaultIntent = new Intent(this.getApplicationContext(),
                DownloadsActivity.class);
        defaultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingDefaultIntent = PendingIntent.getActivity(this,
                100, defaultIntent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                // Set Icon
                .setSmallIcon(R.drawable.logo_trans)
                .setContentText("Téléchargement en cours!")
                // Dismiss Notification
                .setAutoCancel(true)
                // Set RemoteViews into Notification
                .setContentIntent(pendingDefaultIntent)
                .setPriority(NotificationCompat.PRIORITY_LOW);

        mNotification = builder.build();

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                // Android 14 (API 34) and above
                startForeground(
                        1,
                        mNotification,
                        ServiceInfo.FOREGROUND_SERVICE_TYPE_DATA_SYNC
                );
            } else {
                // For Android 13 and below
                startForeground(1, mNotification);
            }
        } catch (Exception e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (e instanceof ServiceStartNotAllowedException) {
                    // Handle the case where the service can't be started
                    stopSelf();
                } else {
                    throw e;
                }
            }
        }
    }
    @Override
    public IBinder onBind(Intent intent) {

        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {

        Log.d("DownloadService", "destroyed");
        if (mReceiver!=null) {
            try {
                unregisterReceiver(mReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        stopForeground(true);
        super.onDestroy();
    }


    public static void cancellAll(Context context) {

        Log.d("UCT", "cancellall");
        for(int i = 0 ; i<mCurrentUCT.size();i++){
            UncompressTask uct = mCurrentUCT.get(i);
            uct.cancel(true);
        }
        DownloadManager dm = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterByStatus(DownloadManager.STATUS_RUNNING);
        // query.orderBy(DownloadManager.COLUMN_TITLE);

        Cursor c = null;
        if (dm != null) {
            c = dm.query(query);
        }
        if (c != null) {
            while (c.moveToNext()) {
                // récupération des index
                /*int descIndex = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
                int titleIndex = c.getColumnIndex(DownloadManager.COLUMN_TITLE);*/
                int idIndex = c.getColumnIndex(DownloadManager.COLUMN_ID);

                // récupération des valeurs
                long dlId = c.getLong(idIndex);

                dm.remove(dlId);
            }
        }

        if (c != null) {
            c.close();
        }
    }
}
