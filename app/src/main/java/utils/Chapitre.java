package utils;

import android.media.MediaMetadataRetriever;
import android.util.Log;

public class Chapitre {

	// id du chapitre dans le ncc.html
	private final String mChapterId;
	// niveau du chapitre (1 à 6)
	private final int mLevel;
	// fichier smil correspondant
	private final String mSmilFile;
	// titre du chapitre tel que trouvé dans le ncc.html
	private final String mTitle;

    private final String mAudioFile;
    private long mDuration;

	Chapitre(String chapterId, int chapterLevel,
			 String smilFile, String mp3File, String chapterTitle) {

		this.mChapterId = chapterId;
		this.mLevel = chapterLevel;
		this.mSmilFile = smilFile;
		this.mTitle = chapterTitle;
        this.mAudioFile = mp3File;

        if(!mAudioFile.equals("")) {
            // extraction des métadonnées du fichier audio.
			try {
				MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
				metaRetriever.setDataSource(mAudioFile);

				String sDuration =
						metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

				metaRetriever.release();
				mDuration = Long.parseLong(sDuration);
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("DaisyParser", "android.media.MediaMetadataRetriever.setDataSource plante.");
			}
        } else{
            Log.e("DaisyParser", "instantiation chapitre: manque le fichier audio");
        }
	}

	@Override
	public String toString() {
		return "niv. "
				+ mLevel + ", smil=" + mSmilFile + ", title="
				+ mTitle + ", audio=" + mAudioFile;
	}

	/*public String getTexteDurée(){
        String append0 = "";
        long seconds = mDuration /1000;
        long minutes = seconds / 60;
        long leftSeconds = seconds % 60;
        if(leftSeconds<10){
            append0="0";
        }
        return minutes+":"+append0+leftSeconds;
    }*/
	public String getId() {
		return mChapterId;
	}

	public int getNiveau() {
		return mLevel;
	}

	/*public String getSmilFile() {
		return mSmilFile;
	}*/

    public String getAudioFile() {
        return mAudioFile;
    }
    public long getDuration() {
        return mDuration;
    }

	public String getTitle() {
		return mTitle;
	}


}
