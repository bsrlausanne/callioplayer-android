package utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import bibliothequesonore.ch.livresbsrandroid.R;

public class MemoryCache {

    //cache for images
    private Context context;
    private String pictureFolder;
    private Bitmap defaultCover;

    public MemoryCache(Context context) {
        this.context = context;
        if(context == null) {
            Log.d("MEMORYCACHE", "context null");
            return;
        }
        defaultCover = BitmapFactory.decodeResource(context.getResources(), R.drawable.nocover);


        pictureFolder = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString();
        pictureFolder +="/.covers/";

        File pf = new File(pictureFolder);
        if (!pf.mkdirs()) {
            pf.mkdirs();

        }

        Log.d("MEMORYCACHE", "pictureFolder: "+pictureFolder);
    }

    class DownloadCoverImageTask extends AsyncTask<String, Void, Bitmap> {
        // Inner class to download cover image as an async class to avoid that the activity
        // waits on the task to be completed

        private ImageView bmImage;
        private String noticeNr;

        private DownloadCoverImageTask(ImageView bmImage, String noticeNr) {

            this.bmImage = bmImage;
            this.noticeNr = noticeNr;
        }

        protected Bitmap doInBackground(String... urls) {

            String urldisplay = urls[0];
            JSONObject bookNode;
            if(urldisplay == null || urldisplay.equals("")){
                Log.d("MEMORYCACHETAG", "null null null");
                try {
                    String webService = "https://webservice.bibliothequesonore.ch/mobile.php";
                    String fullUrl = webService +"?func=FindBook&p1="+noticeNr;
                    URL u = new URL(fullUrl);
                    Log.d("MEMORYCACHETAG", "url: "+fullUrl);

                    HttpURLConnection c = (HttpURLConnection) u.openConnection();

                    int timeout = 6000;
                    c.setConnectTimeout(timeout);
                    c.setReadTimeout(timeout);

                    c.connect();

                    int status = c.getResponseCode();
                    if (status!=200 && status != 201) {
                        Log.d("WCT", "Problème avec requête: "+status);
                    }

                    switch (status) {
                        case 200:
                        case 201:
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line).append("\n");
                            }
                            br.close();

                            bookNode = JSonManager.getResultNode(sb.toString(), "FindBook");
                            urldisplay = (String)bookNode.get("cover");
                            Log.d("MEMORYCACHETAG", "resultat: "+urldisplay);
                            break;
                        case 400:
                            Log.e("MEMORYCACHETAG", "resultat: pas bon");
                            return null;
                        //  BufferedReader br2 = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    }

                } catch (Exception e) {
                    System.out.println(e.toString());
                    return null;
                }
            }
            Bitmap bitmap = null;
            if(urldisplay == null){
                Log.e("MEMORYCACHETAG", "urldisplay null");
                return defaultCover;
            }
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            if(bitmap == null){
                return defaultCover;
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if(result==null){
                Log.d("MEMORYCACHETAG", "onPostExecute bitmap null");
                return;
            }
            Log.d("MEMORYCACHETAG", "onPostExecute " +noticeNr+"/"+bmImage.getTag());
            if(noticeNr.equals(bmImage.getTag())) {
                bmImage.setImageBitmap(result);
            }
            if(context != null && noticeNr != null){
                StoreCoverImageTask d = new StoreCoverImageTask(result, noticeNr);
                d.execute();
            }
        }
    }
    class StoreCoverImageTask extends AsyncTask<String, Void, Bitmap> {
        // Inner class to download cover image as an async class to avoid that the activity
        // waits on the task to be completed

        private Bitmap bitmap;
        private String noticeNr;

        private StoreCoverImageTask(Bitmap bitmap, String noticeNr) {

            this.bitmap = bitmap;
            this.noticeNr = noticeNr;
        }

        protected Bitmap doInBackground(String... urls) {

            if(context != null && noticeNr != null){
                storeBitmap(noticeNr, bitmap);
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            Log.d("StoreCoverImageTask", noticeNr+"ok");
        }
    }

    public void loadBitmapCoverImage(ImageView iv, String url, String noticeNr) {
        try {
            Integer.parseInt(noticeNr);
        } catch (NumberFormatException e) {
            Log.d("MEMORYCACHE", "not a number : "+ noticeNr);
            return;
        }
        Log.d("MEMORYCACHE", "loadBitmapCoverImage: "+noticeNr+" / "+url);

        if (noticeNr != null) {
            Log.d("MEMORYCACHE", noticeNr);
            Log.d("MEMORYCACHE2", pictureFolder);
            File file = new File(pictureFolder, noticeNr + ".jpg");

            if(file.exists()){
                Log.d("MEMORYCACHE", "chargement depuis fichier");
                Bitmap imgCover = BitmapFactory.decodeFile(file.toString());
                iv.setImageBitmap(imgCover);
            } else {
                Log.d("MEMORYCACHE", "télécharger cover");
                iv.setTag(noticeNr);
                DownloadCoverImageTask d = new DownloadCoverImageTask(iv, noticeNr);
                d.execute(url);
            }
        }
    }

    private void storeBitmap(String bookNr, Bitmap bitmap) {
        Log.d("MEMORYCACHE", "storing bitmap "+bookNr);

        OutputStream fOut;
        File file = new File(pictureFolder, bookNr + ".jpg");
        if(file.exists()){
            Log.d("MEMORYCACHE", "bitmap existe"+bookNr);
            return;
        }
        try {
            fOut = new FileOutputStream(file);
            Bitmap result = resizeBitmap(bitmap);
            result.compress(Bitmap.CompressFormat.JPEG, 90, fOut);
            fOut.flush();
            fOut.close();

            // MediaStore.Images.Media.insertImage(context.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            Log.d("MEMORYCACHE2", "resolver: "+file.getAbsolutePath());
        }catch(Exception e){
            Log.e("MEMORYCACHE","erreur de stockage bitmap: "+bookNr);
            if (file.exists()) {
                file.delete();
            }
            e.printStackTrace();
        }
    }
    public Bitmap resizeBitmap(Bitmap bitmap) {

        double xFactor;
        double width = Double.valueOf(bitmap.getWidth());
        double height = Double.valueOf(bitmap.getHeight());

        if (width > height) {
            xFactor = 200 / width;
        } else {
            xFactor = 320 / height;
        }

        int Nheight = (int) ((xFactor * height));
        int NWidth = (int) (xFactor * width);

        return Bitmap.createScaledBitmap(bitmap, NWidth, Nheight, true);

    }
}
