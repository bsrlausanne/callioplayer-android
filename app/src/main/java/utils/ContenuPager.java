package utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import fragments.BookmarksFragment;
import fragments.TocFragment;

public class ContenuPager extends FragmentStatePagerAdapter {

    private TocFragment tocTab;
    private BookmarksFragment bookmarkTab;
    //integer to count number of tabs
    private int tabCount;

    //Constructor to the class
    public ContenuPager(FragmentManager fm, int tabCount) {
        super(fm);
        tocTab = new TocFragment();
        bookmarkTab = new BookmarksFragment();
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return tocTab;
            case 1:
                return bookmarkTab;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }


}