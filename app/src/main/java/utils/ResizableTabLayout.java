package utils;

import android.content.Context;
import com.google.android.material.tabs.TabLayout;
import android.util.AttributeSet;

/**
 * Created by simou on 23.06.2016.
 */
public class ResizableTabLayout extends TabLayout {

    public ResizableTabLayout(Context context) {
        super(context);
    }

    public ResizableTabLayout(Context context, AttributeSet attrs){
        super(context, attrs);
    }
    public ResizableTabLayout(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
    }

    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        super.setMeasuredDimension(widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.EXACTLY));
    }
}
