package utils;

import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public abstract class SimpleTwoFingerDoubleTapDetector {
    private static final int TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
    private long mFirstDownTime = 0;
    private boolean mSeparateTouches = false;
    private byte mTwoFingerTapCount = 0;

    private void reset(long time) {
        mFirstDownTime = time;
        mSeparateTouches = false;
        mTwoFingerTapCount = 0;
    }

    public boolean onTouchEvent(MotionEvent event) {
        //Log.d("multitouch", "touchevent "+ event.getPointerCount()+ " "+ event.getActionMasked());
        /*if(LecteurActivity.talkbackOn) {
            Log.d("multitouch", "count: "+event.getPointerCount()+event.);
            if (event.getPointerCount() == 2 && event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
                Log.d("multitouch", "talkback bing");
                onTwoFingerDoubleTap();
            }
            if (event.getActionMasked() == MotionEvent.ACTION_UP) {
                Log.d("multitouch", "count: " + event.getPointerCount());
            }

            return true;
        }*/
        switch(event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if(mFirstDownTime == 0 || event.getEventTime() - mFirstDownTime > TIMEOUT)
                    reset(event.getDownTime());
                break;
            case MotionEvent.ACTION_POINTER_UP:
                if(event.getPointerCount() == 2) {
                    mTwoFingerTapCount++;
                }
                else
                    mFirstDownTime = 0;
                break;
            case MotionEvent.ACTION_UP:
                if(!mSeparateTouches)
                    mSeparateTouches = true;
                else if(mTwoFingerTapCount == 2 && event.getEventTime() - mFirstDownTime < TIMEOUT) {
                    Log.d("multitouch", "bing");
                    onTwoFingerDoubleTap();
                    mFirstDownTime = 0;
                    return true;
                }
        }

        return false;
    }

    public abstract void onTwoFingerDoubleTap();
}