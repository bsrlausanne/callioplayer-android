package utils;

/**
 * Created by simou on 06.07.2016.
 */
public class Signet {
    public String title;
    public long position;

    Signet(String title, long position){
        this.title = title;
        this.position = position;
    }
}
