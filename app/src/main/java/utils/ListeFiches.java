package utils;

import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by simou on 24.10.2014.
 */
public class ListeFiches {

    private HashMap<String, FicheLivre> bookMap;
    private ArrayList<FicheLivre> bookList;
    private static ListeFiches theList;
    public final static int WISHES_TYPE = 0;
    public final static int NEWBOOKS_TYPE = 1;
    public final static int SEARCH_TYPE = 2;
    public final static int CATEGS_TYPE = 3;
    public final static int SIMILAR_TYPE = 4;
    public final static int TRIAL_TYPE = 5;
    public final static int VOTATIONS_TYPE = 6;
    public static final int FILMS_TYPE = 7;

    public ListeFiches(Object[] bookArray){
        this.bookMap = new HashMap<>();

        this.bookList = new ArrayList<>();

        addFromBookArray(bookArray);

        // supprimé pour remettre de l'ordre dans les résultats.
        //Collections.sort(bookList);
        theList = this;
    }

    public void addFromBookArray(Object[] bookArray) {

        for(Object book : bookArray ){
            JSONObject currentBook = (JSONObject)book;
            Log.d("ListeFiches", "adding "+((String) currentBook.get("code")).trim());
            // skipping results without file
            if(!((JSONObject)currentBook.get("files")).containsKey("zip")){
                Log.d("ListeFiches", "padzip ");
                continue;
            }

            String title = (String)currentBook.get("title");
            String author = (String)currentBook.get("author");
            String genre = (String)currentBook.get("genre");
            String code = ((String) currentBook.get("code")).trim();
            String reader = (String)currentBook.get("reader");
            String summary = (String)currentBook.get("summary");
            String media = (String)currentBook.get("media");
            String producer = (String)currentBook.get("producer");
            String editor = "";
            if(currentBook.get("editor") != null) {
                editor= currentBook.get("editor") + ", " + currentBook.get("year");
            }
            String uriCover = (String)currentBook.get("cover");

            String mp3Url = "";
            JSONArray audioArray = (JSONArray)((JSONObject)currentBook.get("files")).get("samples");
            if(audioArray != null) {
                if(audioArray.size() == 2){
                    mp3Url = (String) audioArray.get(1);
                } else if(audioArray.size() == 1){
                    mp3Url = (String) audioArray.get(0);
                } else{
                    Log.d("ListeFiches", "pas d'extrait pour "+code);
                }
            }

            JSONObject zipInfo = (JSONObject)((JSONObject)currentBook.get("files")).get("zip");
            String zipFile = (String)zipInfo.get("uri");

            long zipSize = Long.parseLong((zipInfo.get("size")).toString());
            Log.d("ListeFiches", "adding "+code);
            addItem(new FicheLivre(code, title, author, editor, media, reader, genre, summary, producer, mp3Url, zipFile, zipSize, uriCover));
        }
    }

    public int getCount() {
        return bookList.size();
    }

    public FicheLivre getItem(int i) {
        return bookList.get(i);
    }

    public FicheLivre getItemByNumber(String noticeNr) {
        return bookMap.get(noticeNr);
    }

    public static ListeFiches getTheList(){
        if(theList != null ){
            return theList;
        }else{
            return new ListeFiches(new Object[0]);
        }
    }

    void addItem(FicheLivre book) {
        if (bookMap.containsKey(book.getNoticeNr())) {
            return;
        }
        bookMap.put(book.getNoticeNr(), book);
        bookList.add(book);
    }

    void removeItem(String noticeNr){
        FicheLivre removedBook = bookMap.remove(noticeNr);
        bookList.remove(removedBook);
    }

    boolean isOnList(String noticeNr){
        if(bookMap.containsKey(noticeNr)){
            return true;
        } else{
            return false;
        }
    }
}
