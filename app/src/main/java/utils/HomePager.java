package utils;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import fragments.InReadingFragment;
import fragments.LoginFragment;
import fragments.MyLibraryFragment;


//Extending FragmentStatePagerAdapter
public class HomePager extends FragmentStatePagerAdapter {

    private InReadingFragment inReadingTab;
    private MyLibraryFragment libraryTab;
    private LoginFragment loginTab;
    //integer to count number of tabs
    private int tabCount;

    //Constructor to the class
    public HomePager(FragmentManager fm, int tabCount) {
        super(fm);
        loginTab = new LoginFragment();
        inReadingTab = new InReadingFragment();
        libraryTab = new MyLibraryFragment();

        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                return inReadingTab;
            case 1:
                return libraryTab;
            case 2:
                return loginTab;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }


}