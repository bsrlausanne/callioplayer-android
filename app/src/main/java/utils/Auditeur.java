package utils;

import org.json.simple.JSONObject;

public class Auditeur {

	private int id;
	
	private String firstname;
	private String lastname;
	private String country;

	public Auditeur(JSONObject auditeurNode) {
		if(auditeurNode==null){
			return;
		}
		this.id = Integer.parseInt((String) auditeurNode.get("id"));
        this.firstname = (String) auditeurNode.get("firstName");
		if (this.firstname == null) this.firstname = "";
        this.lastname = (String) auditeurNode.get("lastName");
		if (this.lastname == null) this.lastname = "";
		this.country = (String) auditeurNode.get("country");
		if (this.country == null) this.country = "";

   	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}



}