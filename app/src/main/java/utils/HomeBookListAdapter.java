package utils;

import static utils.Utils.prepareAndShowAlert;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import activities.DetailActivity;
import activities.HomeActivity;
import activities.LecteurActivity;
import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.InReadingFragment;
import fragments.MyLibraryFragment;
import services.LecteurService;

public class HomeBookListAdapter extends BaseAdapter {

    private static class ViewHolder {
        private TextView titleText;
        private TextView authorText;
        private TextView mediaText;
        private ImageView coverImage;

        private ImageView function1Button;
        private ImageView function2Button;
        private ImageView function3Button;
    }

    private final static int INREADING = 0;
    private final static int LIBRARY = 1;

    private int mMode;

    private MemoryCache memoryCache;

    private int toDeleteLivreId;
    private Context mContext;
    private ArrayList<LivreDaisy> mBookList;
    private static int sortBy = R.id.search_dl_date;

    private String viewType; // "text", "cover", "text_cover"

    public HomeBookListAdapter(ArrayList<LivreDaisy> bookList, Context context, int mode) {
        mBookList = bookList;
        mContext = context;
        mMode = mode;
        viewType = HomeActivity.getViewType();
        memoryCache = new MemoryCache(mContext);

        sortBook(mode);
    }

    public void updateList() {
        mBookList = Bibliotheque.getBibliotheque(mContext).getAllLivres();
        sortBook(mMode);
        notifyDataSetChanged();
    }
    public static void setSortBy(int pos) {
        sortBy = pos;
    }

    private void sortBook(int mMode) {

        if(mMode == INREADING){
            Collections.sort(mBookList, new Comparator<LivreDaisy>() {
                @Override public int compare(LivreDaisy l1, LivreDaisy l2) {

                    return (int) (l2.getPlayDate() - l1.getPlayDate());
                }
            });

            return;
        }

        switch (sortBy) {
            case R.id.search_dl_date: //download date
                Log.d("Spinner", "sorted default");
                if (mMode == LIBRARY) {
                    Collections.sort(mBookList, new Comparator<LivreDaisy>() {
                        @Override public int compare(LivreDaisy l1, LivreDaisy l2) {

                            return (int) (l2.getDownloadDate() - l1.getDownloadDate());
                        }
                    });
                }
                break;
            case R.id.search_title: // title
                Log.d("Spinner", "sorted title");
                Collections.sort(mBookList, new Comparator<LivreDaisy>() {
                    @Override
                    public int compare(LivreDaisy o1, LivreDaisy o2) {

                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });
                break;
            case R.id.search_author: // author
                Log.d("Spinner", "sorted author");
                Collections.sort(mBookList, new Comparator<LivreDaisy>() {
                    @Override
                    public int compare(LivreDaisy o1, LivreDaisy o2) {

                        return o1.getAuthor().compareToIgnoreCase(o2.getAuthor());
                    }
                });
                break;

            default:
                break;
        }
    }

    private View.OnClickListener playBookListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            int bookId = (int) view.getTag(R.string.Tags_BookId);
            Intent playerIntent = new Intent(mContext, LecteurActivity.class);
            playerIntent.putExtra("bookName", bookId);
            mContext.startActivity(playerIntent);
        }

    };
    @Override
    public int getCount() {
        return mBookList.size();
    }


    @Override
    public LivreDaisy getItem(int i) {
        return mBookList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (convertView == null) {

            assert li != null;
            switch (viewType) {
                case "text":
                    convertView = li.inflate(R.layout.book_row_text, viewGroup, false);
                    break;
                case "cover":
                    convertView = li.inflate(R.layout.book_row_cover, viewGroup, false);
                    break;
                case "text_cover":
                    convertView = li.inflate(R.layout.book_row_text_cover, viewGroup, false);
                    break;
            }
            assert (convertView != null);

            holder = new ViewHolder();

            if (viewType.equals("text") || viewType.equals("text_cover")) {
                holder.titleText = convertView.findViewById(R.id.titleText);
                holder.authorText = convertView.findViewById(R.id.authorText);
                holder.mediaText = convertView.findViewById(R.id.mediaText);
            }

            if (!viewType.equals("text")) {
                holder.coverImage = convertView.findViewById(R.id.coverImage);
            }

            holder.function1Button = convertView.findViewById(R.id.book_action_1);
            holder.function2Button = convertView.findViewById(R.id.book_action_2);
            holder.function3Button = convertView.findViewById(R.id.book_action_3);

            convertView.setTag(R.string.Tags_Holder, holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag(R.string.Tags_Holder);
        }

        final LivreDaisy currentLivre = mBookList.get(i);

        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        LivreDaisy activeLivre = Bibliotheque.getBibliotheque(mContext).getActiveLivre();
                        if(currentLivre.equals(activeLivre)){
                            if(LecteurService.isRunning){
                                LocalBroadcastManager localBM = LocalBroadcastManager.getInstance(mContext);
                                Intent intent = new Intent("stop_service");
                                localBM.sendBroadcast(intent);
                            }
                        }
                        Bibliotheque.getBibliotheque(mContext).effacerLivre(toDeleteLivreId);
                        Bibliotheque.getBibliotheque(mContext).storeLivres();

                        mBookList.remove(currentLivre);
                        notifyDataSetChanged();
                        Utils.sendTBMessage(mContext, currentLivre.getTitle()+" "+mContext.getResources().getString(R.string.Accueil_ABienEteSupprime));
                        MyLibraryFragment.update();
                        InReadingFragment.update();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // on y retourne
                        break;
                }
            }
        };

        final DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        LivreDaisy activeLivre = Bibliotheque.getBibliotheque(mContext).getActiveLivre();
                        if(currentLivre.equals(activeLivre)){
                            if(LecteurService.isRunning){
                                LocalBroadcastManager localBM = LocalBroadcastManager.getInstance(mContext);
                                Intent intent = new Intent("stop_service");
                                localBM.sendBroadcast(intent);
                            }
                        }
                        currentLivre.resetPlayDate();
                        Bibliotheque.getBibliotheque(mContext).storeLivres();
                        mBookList.remove(currentLivre);
                        Utils.sendTBMessage(mContext, currentLivre.getTitle() +" "+mContext.getResources().getString(R.string.Accueil_ABienEteOublie));
                        notifyDataSetChanged();
                        InReadingFragment.update();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // on y retourne
                        break;
                }
            }
        };

        View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDeleteLivreId = (int)v.getTag();
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.Accueil_ConfirmationSuppression)+"\n"+currentLivre.getTitle()+"?").setPositiveButton(mContext.getResources().getString(R.string.Global_Oui), dialogClickListener)
                        .setNegativeButton(mContext.getResources().getString(R.string.Global_Non), dialogClickListener);

                final AlertDialog alert = builder.create();
                prepareAndShowAlert(mContext, alert);
            }
        };

        View.OnClickListener clearListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.Accueil_ConfirmationOubli)+"\n"+currentLivre.getTitle()+"?").setPositiveButton(mContext.getResources().getString(R.string.Global_Oui), dialogClickListener2)
                        .setNegativeButton(mContext.getResources().getString(R.string.Global_Non), dialogClickListener2);

                AlertDialog alert = builder.create();
                prepareAndShowAlert(mContext, alert);

            }
        };

        View.OnClickListener infoBook = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyLibraryFragment.internetOn) {
                    int bookId = (int) view.getTag();
                    LivreDaisy selectedBook = Bibliotheque.getBibliotheque(mContext).getLivreById(bookId);
                    Log.d("BOOKID", "hello " + selectedBook.getNoticeNr());
                    Intent detailIntent = new Intent(mContext, DetailActivity.class);
                    detailIntent.putExtra("bookNr", selectedBook.getNoticeNr());
                    mContext.startActivity(detailIntent);
                }
            }

        };

        if(mMode == INREADING ){
            holder.function1Button.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
            holder.function1Button.setImageResource(R.drawable.ic_clear_white_48dp);
            holder.function1Button.setOnClickListener(clearListener);
            holder.function1Button.setContentDescription(mContext.getResources().getString(R.string.Accueil_Retirer)+":\n"+currentLivre.getTitle());

            if (!viewType.equals("cover")) {
                holder.mediaText.setText((round(((float) currentLivre.getCurrentPosition() / (float) currentLivre.getDuration() * 100)) + "%"));
                holder.mediaText.setContentDescription(mContext.getResources().getString(R.string.Global_Progression) + round(((float) currentLivre.getCurrentPosition() / (float) currentLivre.getDuration() * 100)) + "%");
            }

            holder.function2Button.setVisibility(View.GONE);
            holder.function3Button.setVisibility(View.GONE);
        } else if (mMode == LIBRARY) {
            holder.function2Button.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
            holder.function2Button.setImageResource(R.drawable.ic_delete_white_48dp);
            holder.function2Button.setOnClickListener(deleteListener);
            holder.function2Button.setContentDescription(mContext.getResources().getString(R.string.Accueil_Effacer)+":\n "+currentLivre.getTitle());

            if(!(new File(currentLivre.getBookPath()).canWrite())){
                holder.function2Button.setVisibility(View.INVISIBLE);
            }else{
                holder.function2Button.setVisibility(View.VISIBLE);
            }
            if(currentLivre.getNoticeNr() != null && !currentLivre.getNoticeNr().equals("") && MyLibraryFragment.internetOn) {
                holder.function1Button.setVisibility(View.VISIBLE);
                holder.function1Button.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
                holder.function1Button.setImageResource(R.drawable.ic_more_info_white_48dp);
                holder.function1Button.setOnClickListener(infoBook);
                holder.function1Button.setContentDescription(mContext.getResources().getString(R.string.Accueil_Plus_informations)+":\n "+currentLivre.getTitle());
            } else{
                holder.function1Button.setVisibility(View.INVISIBLE);
            }
            //holder.function2Button.setContentDescription(mContext.getResources().getString(R.string.));

            holder.function3Button.setVisibility(View.GONE);

            if (!viewType.equals("cover")) {
                holder.mediaText.setText(currentLivre.getDurationText());
            }
        }

        if (!viewType.equals("cover")) {
            holder.titleText.setText(currentLivre.getTitle());
            holder.authorText.setText(currentLivre.getAuthor());

            holder.titleText.setTag(R.string.Tags_BookId, currentLivre.getId());
            holder.mediaText.setTag(R.string.Tags_BookId, currentLivre.getId());
            holder.authorText.setTag(R.string.Tags_BookId, currentLivre.getId());
        }

        if (viewType.equals("cover") || viewType.equals("text_cover")) {
            holder.coverImage.setImageBitmap(null);
                memoryCache.loadBitmapCoverImage(holder.coverImage, currentLivre.getCover(), currentLivre.getNoticeNr());
                holder.coverImage.setTag(R.string.Tags_BookId, currentLivre.getId());
        }

        convertView.setTag(R.string.Tags_BookId, currentLivre.getId());
        convertView.setTag(R.string.Tags_NoticeNr, currentLivre.getNoticeNr());

        holder.function1Button.setTag(currentLivre.getId());
        holder.function2Button.setTag(currentLivre.getId());
        holder.function3Button.setTag(currentLivre.getId());

        if (!viewType.equals("cover")) {
            holder.titleText.setOnClickListener(playBookListener);
            holder.mediaText.setOnClickListener(playBookListener);
            holder.authorText.setOnClickListener(playBookListener);
        }

        if (!viewType.equals("text")) {
            holder.coverImage.setOnClickListener(playBookListener);
        }
        convertView.setOnClickListener(playBookListener);

        convertView.setContentDescription(currentLivre.getTitle()+ mContext.getResources().getString(R.string.Global_De)+currentLivre.getAuthor()+". "+currentLivre.getLongDurationText());
        return convertView;
    }


    private static float round(float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}
