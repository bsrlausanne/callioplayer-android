package utils;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import bibliothequesonore.ch.livresbsrandroid.R;

public class LivreDaisy {

    private String mAuthor = "";
    private String mTitle = "";
    private long mDuration = 0;
    private String uriCover = "";
    private String mNoticeNr = "";

    private long mDownloadDate = 0;
    private long mLastPlayDate = 0;

    private boolean playing = false;

    // associatif chapterid -> position du début en ms
    private HashMap<String, Long> mChapterStarts = new HashMap<>();

    //chemin absolu du livre sur l'appareil
    private String mBookPath = "";
    // nom unique identifiant le livre (pour l'instant le nom du dossier
    private int mId;

    private float mSpeed = 1.0f;
    private float mPitch = 1.0f;

    private int mJumpSize = 30;

    private int mNavigLevel = 6;
    private int mMaxLevel = 0;

    // position in the book in millisecond
    private long mCurrentPosition = 0;
    private String mCurrentChapterId = "";

    private ArrayList<Chapitre> mChapters = new ArrayList<>();
    
    private boolean mLoaded = false;

    private ArrayList<Signet> mSignets = new ArrayList<>();

    public ArrayList<Signet> getSignets(){
        return mSignets;
    }

    public void addSignet(){
        mSignets.add(new Signet(getCurrentChapitreTitle(), getCurrentPosition()));
    }

    private Signet getSignetByPos(long posInMs){
        for(int i =0; i<mSignets.size();i++){
            if(mSignets.get(i).position == posInMs){
                return mSignets.get(i);
            }
        }
        return null;
    }
    public void removeSignet(long posInMs){

        Signet signet = getSignetByPos(posInMs);
        if(signet == null){
            Log.e("DaisyBook", "Remove: Ce signet n'existe pas: "+posInMs);
        }else{
            mSignets.remove(signet);
        }
    }

    /*public void renameSignet(long posInMs, String newName){
        Signet signet = getSignetByPos(posInMs);
        if(signet == null){
            Log.e("DaisyBook", "rename: Ce signet n'existe pas: "+posInMs);
        }else {
            signet.title = newName;
        }
    }*/

    /*public String getLoadingProblem() {
		return mLoadingProblem;
	}*/
	public ArrayList<Chapitre> getChapters() {
		return mChapters;
	}

    /*public LivreDaisy(String fullBookPath) {
       this(fullBookPath, null);
    }*/

    public LivreDaisy(String fullBookPath, FicheLivre fiche) {
    	Document doc;
    	
    	File bookFolder = new File(fullBookPath);
    	if(!bookFolder.exists()){
    		
    		Log.e("DaisyBook", "Ce dossier n'existe pas: "+bookFolder);
    		mLoaded = false;
    		return;
    	}

        String nccFilePath =  fullBookPath+ "/ncc.html";

        this.mBookPath = fullBookPath;

        File fileFolder = new File(fullBookPath);

        this.mId = fullBookPath.hashCode();

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Log.d("DaisyParser", nccFilePath);
            doc = db.parse(new InputSource(new FileInputStream(nccFilePath)));

            if(doc != null){ doc.getDocumentElement().normalize();}

        } catch (MalformedURLException e) {
            Log.e("DaisyParser", e.toString());
            Log.e("DaisyParser", e.toString());
            e.printStackTrace();
            return;
        } catch (ParserConfigurationException e) {
            Log.e("DaisyParser", e.toString());
            Log.e("DaisyParser", e.toString());
            e.printStackTrace();
            return;
        } catch (SAXException e) {
            Log.e("DaisyParser", e.toString());
            Log.e("DaisyParser", e.toString());
            e.printStackTrace();
            return;
        } catch (IOException e) {
            Log.e("DaisyParser", e.toString());
            e.printStackTrace();
            return;
        }
        if (doc == null) {
            Log.e("DaisyParser", "doc null");
            return;

        }
        // on ne parse les métas que si on a pas de fiche qui vient du catalogue
        if (fiche == null) {
            parseMetas(doc);
            mDownloadDate = fileFolder.lastModified();
        } else {
            mAuthor = fiche.getAuthor();
            mTitle = fiche.getTitle();
            mDownloadDate = fiche.getDownloadDate();
            uriCover = fiche.getCover();
            mNoticeNr = fiche.getNoticeNr();
        }

        //long startTime = System.currentTimeMillis();

        if(!parseChapitres(doc, fullBookPath)){
            return;
        }

        //long endTime = System.currentTimeMillis();

        // initialisation de l'id du premier chapitre
        if(mChapters.size() > 0) {
            mCurrentChapterId = mChapters.get(0).getId();
        }
        mLoaded = true;
    }
    
    private void parseMetas(Document doc){
    	NodeList nodeList = doc.getElementsByTagName("meta");
    	Log.d("Detail", nodeList.toString());
        for (int i = 0; i < nodeList.getLength(); i++) {

            Element currentElement = (Element) nodeList.item(i);
            String elementName = currentElement.getAttribute("name");

            // removing the daisy prefix
            elementName = elementName.substring(elementName.indexOf(":") + 1);
            if (elementName.equals("creator")) {
                mAuthor = currentElement.getAttribute("content");
            }
            if (elementName.equals("title")) {
                mTitle = currentElement.getAttribute("content");
            }
        }
    }
    public void extractNoticeNr(){
        Log.d("parseNoticeNr", "extracting noticeNr for "+mTitle);
        String folderName  = (new File(mBookPath).getName()).toString();

        try {
           Integer.parseInt(folderName);
        } catch (NumberFormatException e) {
            Log.d("parseNoticeNr", "not a number : "+ folderName);
            return;
        }

        mNoticeNr = new File(mBookPath).getName();

        Log.d("parseNoticeNr", "foldername "+mNoticeNr );

    }
    public String getCover() {
        return uriCover;
    }

    private boolean parseChapitres(Document doc, String fullBookPath){

    	NodeList nodeList = doc.getElementsByTagName("body").item(0).getChildNodes();

    	for (int i = 0; i < nodeList.getLength(); i++) {

    		if(nodeList.item(i).getNodeName().startsWith("h")){
    			Element currentH = (Element)nodeList.item(i);
    			
    			String elementId = currentH.getAttribute("id");
    			int elementLevel = Character.getNumericValue(currentH.getNodeName().charAt(1));
                if (elementLevel > mMaxLevel){
                    mMaxLevel = elementLevel;
                    mNavigLevel = elementLevel;
                }
                Element currentElement;
                if(nodeList.item(i).getFirstChild().getNodeType() == Node.TEXT_NODE){
                    currentElement = (Element) nodeList.item(i).getFirstChild().getNextSibling();
                } else{
                    currentElement = (Element) nodeList.item(i).getFirstChild();
                }



    			String nomChapitre = currentElement.getTextContent();
                if(nomChapitre == null){nomChapitre = "";}
    			String href = currentElement.getAttribute("href");
    			String smilCorrespondant = href.substring(0, href.indexOf('#'));
                String mp3Correspondant = getAudioFile(fullBookPath+"/"+smilCorrespondant);
                if (mp3Correspondant!= null && mp3Correspondant.equals("")){
                    Log.e("DaisyParser", "Pas trouvé mp3 pour : "+smilCorrespondant);
                    return false;
                }

                File audioFile = new File(fullBookPath+"/"+mp3Correspondant);
                if(!audioFile.exists()){
                    Log.e("DaisyParser", "Le fichier n'existe pas: "+audioFile);
                    return false;
                }

    			Chapitre newChapitre = new Chapitre(elementId, elementLevel, smilCorrespondant, fullBookPath+"/"+mp3Correspondant, nomChapitre);

                mChapters.add(newChapitre);
                mChapterStarts.put(elementId, mDuration);
                mDuration += newChapitre.getDuration();

    		}

    	}
    	return true;

    }
    public String toString(){
        StringBuilder result = new StringBuilder("Title: " + mTitle + "\n");
        result.append(mChapters.size()).append(" éléments\n");

        for (int i = 0; i< mChapters.size(); i++){
            Chapitre temp = mChapters.get(i);

            String title = temp.getTitle();
            if (title.length() >25) title = title.substring(0,24);

            String audioFile = temp.getAudioFile();
            audioFile = audioFile.substring(audioFile.lastIndexOf('/')+1);

            result.append(temp.getNiveau()).append(" - ").append(audioFile).append(" | ").append(temp.getDuration() / 1000).append("s | ").append(title).append("\n");
        }

        return result.toString();
    }
    public String getTitle() {
        return mTitle;
    }

    public int getId() {

        return mId;
    }

    String getAuthor() {
        return mAuthor;
    }

    /*public String getNarrator() {
        return mNarrator;
    }

    public String getProducer() {
        return mProducer;
    }*/

    String getFolderName() {
        return new File(mBookPath).getName();
    }
    
    String getBookPath() {
        return mBookPath +"/";
    }

	public float getSpeed() {
		return mSpeed;
	}

	public void setSpeed(float newSpeed) {
        mSpeed = newSpeed;
	}

	public float getPitch() {
		return mPitch;
	}

	public void setPitch(float newPitch) {
		mPitch = newPitch;
	}

	private String getCurrentChapitreId() {
		return mCurrentChapterId;
	}

    public Chapitre getChapitreById(String id){
        for(int i = 0; i< mChapters.size(); i++){
            if(mChapters.get(i).getId().equals(id)){
                return mChapters.get(i);
            }
        }
        Log.e("DaisyBook", "Chapitre pas trouvé. Id: "+ getCurrentChapitreId());
        return null;
    }

	public Chapitre getCurrentChapitre(){
        if (!getCurrentChapitreId().equals("")) {
            return getChapitreById(mCurrentChapterId);
        } else{

            return null;
        }
	}

	public void setCurrentChapitreId(String chapterId) {
		mCurrentChapterId = chapterId;
        mCurrentPosition = mChapterStarts.get(chapterId);
	}

    public void setCurrentChapitre(Chapitre chapitre) {
        mCurrentPosition = mChapterStarts.get(chapitre.getId());
        mCurrentChapterId = chapitre.getId();
    }

	public boolean loadedOk(){
		return mLoaded;
	}

    private String getAudioFile(String smilPath){
        Document smilDoc = null;
        String audioFile = "";

        File smilFile = new File(smilPath);

        if(!smilFile.exists()){
            Log.e("DaisyParser", "Fichier introuvable: "+smilPath);
            return "";
        }
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder db = dbf.newDocumentBuilder();
            smilDoc = db.parse(new InputSource(new FileInputStream(smilPath)));
            smilDoc.getDocumentElement().normalize();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(smilDoc != null) {
            NodeList capsuleNodeList = smilDoc.getElementsByTagName("audio");
            for (int l = 0; l < capsuleNodeList.getLength(); l++) {
                Node nodeAudio = capsuleNodeList.item(l);
                Element ElmntAudio = (Element) nodeAudio;
                if (!ElmntAudio.getAttribute("src").equals("")) {
                    audioFile = ElmntAudio.getAttribute("src");
                    break;
                }
            }
        }
        return audioFile;
    }

    public Chapitre getNextChapitre(boolean ignoreLevel){
        Chapitre currentChapitre = getChapitreById(mCurrentChapterId);
        int currentIndex = mChapters.indexOf(currentChapitre);
        if(currentIndex < mChapters.size()-1){
            if(ignoreLevel){
                return mChapters.get(currentIndex+1);
            } else {
                for (int i = currentIndex + 1; i < mChapters.size(); i++) {

                    if (mChapters.get(i).getNiveau() <= mNavigLevel) {
                        return mChapters.get(i);
                    }
                }
            }
            return null;
            // return mChapters.get(currentIndex+1);
        } else{
            return null;
        }
    }

    public Chapitre getPreviousChapitre(){
        Chapitre currentChapitre = getChapitreById(mCurrentChapterId);
        int currentIndex = mChapters.indexOf(currentChapitre);
        if( currentIndex >0 ){
            for(int i = currentIndex-1; i>=0;i--) {
                if(mChapters.get(i).getNiveau() <= mNavigLevel){
                    return mChapters.get(i);
                }
            }
            return null;
        } else{
            return null;
        }
    }

    public void resetPosition(){
        mCurrentChapterId = mChapters.get(0).getId();
        mCurrentPosition = 0;
        mLastPlayDate = 0;
    }

    public long getCurrentPosition() {
        return mCurrentPosition;
    }

    public String getCurrentChapitreTitle() {
        return getCurrentChapitre().getTitle();
    }

    public void setRelativePosition(long currentPosition) {
        Log.d("LCT", "setting relative position to "+currentPosition);
        mCurrentPosition = mChapterStarts.get(mCurrentChapterId)+currentPosition;

    }
    public void setAbsolutePosition(long currentPosition) {
        Log.d("LCT", "setting absolute position to "+currentPosition);
        mCurrentPosition = currentPosition;
        for(int i = 0; i< mChapters.size()-1; i++){
            String chapterId = mChapters.get(i).getId();
            String nextChapterId = mChapters.get(i+1).getId();
            if(currentPosition >= mChapterStarts.get(chapterId) &&  currentPosition < mChapterStarts.get(nextChapterId)) {
                 mCurrentChapterId = chapterId;
            }
        }
        Log.d("DaisyBook", "Setting CurrentPos to: "+mCurrentPosition);
    }
    public long getRelativePosition(){
        return mCurrentPosition - mChapterStarts.get(mCurrentChapterId);
    }

    public void updatePlayDate(){
        Calendar cal = Calendar.getInstance();
        mLastPlayDate = cal.getTimeInMillis();
    }

    void resetPlayDate(){
        mLastPlayDate = 0;
    }
    long getPlayDate(){
        return mLastPlayDate;
    }

    public long getDuration() {
        return mDuration;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public int getJumpSize() {
        return mJumpSize;
    }

    public void setJumpSize(int jumpSize) {
        this.mJumpSize = jumpSize;
    }

    public int getMaxLevel() {
        return mMaxLevel;
    }

    public int getNavigLevel() {
        return mNavigLevel;
    }

    public void setNavigLevel(int navigLevel) {
        this.mNavigLevel = navigLevel;
    }

    public static String posToLongTexte(long position, boolean giveSeconds) {
        long seconds = position / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long leftMinutes = minutes % 60;
        long leftSeconds = seconds % 60;

        String result = "";
        if(hours > 0){
            result+=hours+"h. ";
        }
        if(leftMinutes > 0){
            result+=leftMinutes+"min. ";
        }
        if(giveSeconds && leftSeconds > 0){
            result += leftSeconds+"sec. ";
        }

        /*if (leftSeconds < 10) {
            append0 = "0";
        }
        if (leftMinutes < 10) {
            append1 = "0";
        }
        if (hours > 0) {
            return hours + "h" + append1 + leftMinutes+"min.";
        } else {
            return leftMinutes+"min.";
        }*/
        if(result.equals("")){

            result = Bibliotheque.getString(R.string.Lecteur_PositionInitiale);
        }
        return result;
    }
    public static String posToDescription(long position, boolean giveSeconds) {
        Log.d("posToDescription", position+"");

        long seconds = position / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long leftMinutes = minutes % 60;
        long leftSeconds = seconds % 60;

        /*if(seconds == 0 && minutes == 0 && hours ==0){
            return
        }*/
        String result = "";
        if(hours > 0){
            result+=hours+Bibliotheque.getString(R.string.Global_Heures)+" ";
        }
        if(leftMinutes > 0){
            result+=leftMinutes+Bibliotheque.getString(R.string.Global_Minutes)+" ";
        }
        if(giveSeconds && leftSeconds > 0) {
            result += leftSeconds + Bibliotheque.getString(R.string.Global_Secondes) + " ";
        }
        if(result.equals("")){
            result = Bibliotheque.getString(R.string.Lecteur_PositionInitiale);
        }
        return result;
    }

    public static String posToShortTexte(long position) {

        long seconds = position / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long leftMinutes = minutes % 60;
        long leftSeconds = seconds % 60;

        String result = "";
        /*if(hours == 0){
            //result+="00:";
        }else */
        if(hours < 10){
            result+="0"+hours+":";
        }else{
            result+=hours+":";
        }
        if(leftMinutes == 0){
            result+="00:";
        }else if(leftMinutes < 10){
            result+="0"+leftMinutes+":";
        }else{
            result+=leftMinutes+":";
        }

        if(leftSeconds == 0){
            result+="00";
        }else if(leftSeconds < 10){
            result+="0"+leftSeconds;
        }else{
            result+=leftSeconds;
        }

        return result;
    }
    String getDurationText() {
        String result = "";

        long secondsLeft = mDuration/1000;

        long hours = secondsLeft / 3600;

        if (hours != 0) {
            result += hours+Bibliotheque.getString(R.string.Global_H)+".";
        }
        secondsLeft = secondsLeft % 3600;

        long minutes = secondsLeft / 60;
        if (minutes != 0) {
            // if (minutes<10){result += "0"; }
            result +=" "+minutes+Bibliotheque.getString(R.string.Global_M)+".";
        }
        if(result.startsWith(" ")) result = result.substring(1);
        return result;

    }
    String getLongDurationText() {
        String result = getDurationText();
        result = result.replace(Bibliotheque.getString(R.string.Global_H)+".", " "+Bibliotheque.getString(R.string.Global_Heures));
        result = result.replace(Bibliotheque.getString(R.string.Global_M)+".", Bibliotheque.getString(R.string.Global_Minutes));
        return Bibliotheque.getString(R.string.Global_Duree)+": "+result;

    }
    long getDownloadDate() {
        return mDownloadDate;
    }

    public String getNoticeNr() {
        return mNoticeNr;
    }
}

