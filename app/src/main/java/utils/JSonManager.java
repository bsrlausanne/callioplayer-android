package utils;

import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JSonManager {
    private static JSONParser parser = new JSONParser();

    /*public static String getFunction(String jSonString){
        try{
            JSONObject obj = (JSONObject)parser.parse(jSonString);
            Set<String> keySet = ((JSONObject) ((JSONObject)obj.get("result"))).keySet();
            //Log.d("WCT", "getFunction: " + keySet.toString());
            return keySet.toString();
        } catch (Exception e) {
            //Log.d("WCT", "getFunction: exception: "+e.toString());
            e.printStackTrace();
            return "";
        }
    }*/

    public static String getError(String jSonString){
        try{
            JSONObject obj = (JSONObject)parser.parse(jSonString);
            if (obj.get("error") != null){

                JSONObject error = (JSONObject)obj.get("error");
                Log.d("WCT", "getError: "+error.toString());

                String errorName = (String)error.get("name");
                String errorReason = (String)error.get("reason");

                return errorName+": "+errorReason;
            } else{
                return "";
            }
        } catch (Exception e) {
            Log.d("WCT", "getError: exception: "+e.toString());
            e.printStackTrace();
            return "";
        }
    }
    
    /*public static String getErrorName(String jSonString){
        try{
            JSONObject obj = (JSONObject)parser.parse(jSonString);
            if (((JSONObject)obj.get("error")) != null){

                JSONObject error = (JSONObject)obj.get("error");
                Log.d("WCT", "getError: "+error.toString());

                String errorName = (String)error.get("name");

                return errorName;
            } else{
                return "";
            }
        } catch (Exception e) {
            Log.d("WCT", "getError: exception: "+e.toString());
            e.printStackTrace();
            return "";
        }
    }*/
    public static JSONObject getResultNode(String jSonString, String functionName){
        //Log.d("NB.NB", "getResultNode");

		try{
			JSONObject obj = (JSONObject)parser.parse(jSonString);
			if (obj.get("error") != null){
                Log.d("NB.NB", "getResultNode: return error");
				return ((JSONObject)obj.get("error"));
			} else{
				if (obj.get("result") != null){
                    //Log.d("NB.NB", "getResultNode: return result");
					return (JSONObject) ((JSONObject)obj.get("result")).get(functionName);
				}else{
                    Log.d("NB.NB", "getResultNode: result=null");
					return null;
				}
			}
		} catch (Exception e) {
            Log.d("NB.NB", "getResultNode: exception");
			e.printStackTrace();
			return null;
		} 
		
	}
	
	public static JSONArray getResultArray(String jSonString, String functionName){
       // Log.d("NB.NB", "getResultArray");

		try{
			JSONObject obj = (JSONObject)parser.parse(jSonString);
			if (obj.get("error") != null){
                Log.d("NB.NB", "JSonManager: error, return null");
				// return (JSONArray)((Object[])obj.get("error"));
				return null;
			} else{
                //Log.d("NB.NB", "getResultArray: return array");
				return (JSONArray) ((JSONObject)obj.get("result")).get(functionName);
			}
		} catch (Exception e) {
            Log.d("NB.NB", "getResultArray: exception");
			e.printStackTrace();
			return null;
		} 
		
	}
}