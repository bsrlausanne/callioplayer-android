package utils;

import androidx.annotation.NonNull;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by simou on 22.10.2014.
 */
public class FicheLivre implements Comparable{

    private String noticeNr;
    private String title;
    private String author;
    private String genre;
    private String uriCover;

    private long downloadDate = 0;

    private String editor;

    private String summary;
    private URL mp3Url;

    private String zipUrl;
    private long zipSize;

    private String media;
    private String reader;

    FicheLivre(String noticeNr, String title, String author, String editor, String media, String reader, String genre, String summary, String producer, String mp3Url, String zipUrl, long zipSize, String uriCover){
        this.title = title;
        this.author = author;
        this.media = media;
        this.reader = reader;
        this.noticeNr = noticeNr;
        this.genre = genre;
        this.summary = summary;
        this.producer = producer;
        this.zipUrl = zipUrl;
        this.zipSize = zipSize;
        this.editor = editor;
        this.uriCover = uriCover;

        try {
            this.mp3Url = new URL(mp3Url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public long getZipSize() {
		return zipSize;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    /*public void setAuthor(String author) {
        this.author = author;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }*/

    public URL getMp3Url() {
        return mp3Url;
    }

    /*public void setMp3Url(URL mp3Url) {
        this.mp3Url = mp3Url;
    }*/

    public String getNoticeNr() {
        return noticeNr;
    }

   /* public void setNoticeNr(String noticeNr) {
        this.noticeNr = noticeNr;
    }*/

    public String getMedia() {
        return media;
    }

    /*public void setMedia(String media) {
        this.media = media;
    }*/

    public String getReader() {

        if(reader == null){
            return "Non spécifié";
        }else
        return reader;
    }

    /*public void setReader(String reader) {
        this.reader = reader;
    }*/

    public String getProducer() {
        return producer;
    }

    /*public void setProducer(String producer) {
        this.producer = producer;
    }*/

    public String getGenre() {
        return genre;
    }

    /*public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }*/

    private String producer;

    public String getSummary() {
        return summary;
    }

    /*public void setSummary(String summary) {
        this.summary = summary;
    }*/


    public String getZipUrl() {
        return zipUrl;
    }

    /*public void setZipUrl(String zipUrl) {
        this.zipUrl = zipUrl;
    }*/

    public String getEditor() {
        return editor;
    }

    /*public void setEditor(String editor) {
        this.editor = editor;
    }*/

    @Override
    public int compareTo(@NonNull Object o) {
        String otherNoticeNr = ((FicheLivre)o).getNoticeNr();
        return otherNoticeNr.compareTo(noticeNr);
    }

    long getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(long downloadDate) {
        this.downloadDate = downloadDate;
    }

    public String getCover() {
        return uriCover;
    }
}
