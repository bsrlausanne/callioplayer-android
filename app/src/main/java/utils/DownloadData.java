package utils;

import androidx.annotation.NonNull;

/**
 * Created by Simon on 30.05.2017.
 */

public class DownloadData implements Comparable<DownloadData>{
    public long downloadId;
    public String description;
    public  String noticeNr;
    public long downloadPercent;
    public long uncompressPercent;

    public DownloadData(long downloadId, String description, String noticeNr, long percent){
        this.downloadId =downloadId;
        this.description =description;
        this.noticeNr = noticeNr;
        this.downloadPercent =percent;
        this.uncompressPercent = -1;
    }

    @Override
    public int compareTo(@NonNull DownloadData another) {
        if(downloadId > another.downloadId){
            return 1;
        }else if(downloadId < another.downloadId){
            return -1;
        }
        // if a.downloadId==b.downloadId
        return 0;
    }
}
