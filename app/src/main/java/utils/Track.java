package utils;

import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Build;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.locks.ReentrantLock;

public class Track {
    private AudioTrack mTrack;
    private Sonic mSonic;
    private MediaExtractor mExtractor;
    private MediaCodec mCodec;
    private Thread mDecoderThread;
    private String mPath;
    private Uri mUri;
    private int mFormat = 0;
    private final ReentrantLock mLock;
    private boolean mContinue;
    private boolean mIsDecoding;
    private long mDuration;
    private float mCurrentSpeed;
    private float mCurrentPitch;
    private int mCurrentState;
    private final Context mContext;

    private final static int TRACK_NUM = 0;
    private final static String TAG_TRACK = "PrestissimoTrack";

    private final static int STATE_IDLE = 0;
    private final static int STATE_INITIALIZED = 1;
    private final static int STATE_PREPARED = 3;
    private final static int STATE_STARTED = 4;
    private final static int STATE_PAUSED = 5;
    private final static int STATE_STOPPED = 6;
    private final static int STATE_PLAYBACK_COMPLETED = 7;
    private final static int STATE_END = 8;
    private final static int STATE_ERROR = 9;
    private static LocalBroadcastManager localBM;

    public Track(Context context) {
        mCurrentState = STATE_IDLE;
        mCurrentSpeed = (float) 1.0;
        mCurrentPitch = (float) 1.0;
        mContinue = false;
        mIsDecoding = false;
        mContext = context;
        mPath = null;
        mUri = null;
        mLock = new ReentrantLock();

        localBM = LocalBroadcastManager.getInstance(context);
    }

    public int getCurrentPosition() {
        switch (mCurrentState) {
            case STATE_ERROR:
                error();
                break;
            default:
                if(mExtractor != null ) {
                    try {
                        return (int) (mExtractor.getSampleTime() / 1000);
                    }catch(Exception e){
                        e.printStackTrace();
                        return 0;
                    }
                }
        }
        return 0;
    }

    public boolean isPlaying() {
        switch (mCurrentState) {
            case STATE_ERROR:
                error();
                break;
            default:
                return mCurrentState == STATE_STARTED;
        }
        return false;
    }

    public void prepare() {
        Log.e("TAG", "prepare mCurrentState: "+mCurrentState);
        switch (mCurrentState) {
            case STATE_INITIALIZED:
            case STATE_STOPPED:
                initStream();
                mCurrentState = STATE_PREPARED;
                Log.d(TAG_TRACK, "State changed to STATE_PREPARED");

                break;
            default:
                error();
        }
    }

    public void stop() {
        switch (mCurrentState) {
            case STATE_PREPARED:
            case STATE_STARTED:
            case STATE_STOPPED:
            case STATE_PAUSED:
            case STATE_PLAYBACK_COMPLETED:
                mCurrentState = STATE_STOPPED;
                Log.d(TAG_TRACK, "State changed to STATE_STOPPED");
                mContinue = false;
                mTrack.pause();
                mTrack.flush();
                break;
            default:
                error();
        }
    }

    public void start() {
        Log.e("TAG", "start mCurrentState: "+mCurrentState);
        switch (mCurrentState) {
            case STATE_PREPARED:
            case STATE_PLAYBACK_COMPLETED:
                mCurrentState = STATE_STARTED;
                mContinue = true;
                mTrack.play();
                decode();
            case STATE_STARTED:
                break;
            case STATE_PAUSED:
                mCurrentState = STATE_STARTED;
                mDecoderThread.interrupt();
                mTrack.play();
                break;
            default:
                mCurrentState = STATE_ERROR;
                if (mTrack != null) {
                    error();
                } else {
                    Log.d("start",
                            "Attempting to start while in idle after construction.  Not allowed by no callbacks called");
                }
        }
    }

    public void release() {
        reset();

        mCurrentState = STATE_END;
    }

    private void reset() {
        mLock.lock();
        mContinue = false;
        try {
            if (mDecoderThread != null
                    && mCurrentState != STATE_PLAYBACK_COMPLETED) {
                mDecoderThread.interrupt();
                while (mIsDecoding) {
                    Thread.sleep(1);
                }
            }
        } catch (InterruptedException e) {
            // WTF is happening?
            Log.e(TAG_TRACK,
                    "Interrupted in reset while waiting for decoder thread to stop.",
                    e);
        }
        if (mCodec != null) {
            mCodec.release();
            mCodec = null;
        }
        if (mExtractor != null) {
            mExtractor.release();
            mExtractor = null;
        }
        if (mTrack != null) {
            mTrack.release();
            mTrack = null;
        }
        mCurrentState = STATE_IDLE;
        Log.d(TAG_TRACK, "State changed to STATE_IDLE");
        mLock.unlock();
    }

    public void seekTo(final int msec) {
        switch (mCurrentState) {
            case STATE_PREPARED:
            case STATE_STARTED:
            case STATE_PAUSED:
            case STATE_PLAYBACK_COMPLETED:
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            mTrack.setVolume(0.0f);
                        }
                        mLock.lock();
                        if (mTrack == null) {
                            return;
                        }
                        mTrack.flush();
                        mExtractor.seekTo(((long) msec * 1000),
                                MediaExtractor.SEEK_TO_CLOSEST_SYNC);
                        try {
                            Thread.sleep(800);
                            } catch (Exception e) {
                             e.printStackTrace();
                            }
                        if (mTrack != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                mTrack.setVolume(1.0f);
                        }
                        mLock.unlock();
                    }
                });
                t.setDaemon(true);
                t.start();
                break;
            default:
                error();
        }
    }

    public void setDataSourceUri(Uri uri) {
        switch (mCurrentState) {
            case STATE_IDLE:
                mUri = uri;
                mCurrentState = STATE_INITIALIZED;
                Log.d(TAG_TRACK, "Moving state to STATE_INITIALIZED");
                break;
            default:
                error();
        }
    }

    public void setPlaybackPitch(float f) {
        mCurrentPitch = f;
    }

    public void setPlaybackSpeed(float f) {
        mCurrentSpeed = f;
    }

    private void error() {
        Log.e(TAG_TRACK, "Moved to error state!");
        mCurrentState = STATE_ERROR;

    }

    private int findFormatFromChannels(int numChannels) {
        if(mFormat != 0){
            return mFormat;
        } else{
            switch (numChannels) {
                case 1:
                    return AudioFormat.CHANNEL_OUT_MONO;
                case 2:
                    Log.d(TAG_TRACK, "stereo");
                    return AudioFormat.CHANNEL_OUT_STEREO;
                default:
                    Log.d(TAG_TRACK, "mono");
                    return -1; // Error
            }
        }
    }

    private void initStream() {
        mLock.lock();
        mExtractor = new MediaExtractor();
        if (mPath != null) {
            try {
                mExtractor.setDataSource(mPath);
            } catch (IOException e) {
                Log.e(TAG_TRACK, "Failed setting data source!", e);
                error();
            }
        } else if (mUri != null) {
            try {
                mExtractor.setDataSource(mContext, mUri, null);
            } catch (IOException e) {
                Log.e(TAG_TRACK, "Failed setting data source!", e);
                error();
            }
        }

        final MediaFormat oFormat = mExtractor.getTrackFormat(TRACK_NUM);
        int sampleRate = oFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        int channelCount = oFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        final String mime = oFormat.getString(MediaFormat.KEY_MIME);
        mDuration = oFormat.getLong(MediaFormat.KEY_DURATION);

        Log.v(TAG_TRACK, "Sample rate: " + sampleRate);
        Log.v(TAG_TRACK, "Mime type: " + mime);

        initDevice(sampleRate, channelCount);
        mExtractor.selectTrack(TRACK_NUM);
        try {
            mCodec = MediaCodec.createDecoderByType(mime);
        } catch(IOException ioE){
            ioE.printStackTrace();
        }
        mCodec.configure(oFormat, null, null, 0);
        mLock.unlock();
    }

    private void initDevice(int sampleRate, int numChannels) {

        mLock.lock();
        mFormat = findFormatFromChannels(numChannels);
        final int minSize = AudioTrack.getMinBufferSize(sampleRate, mFormat,
                AudioFormat.ENCODING_PCM_16BIT);
        mTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, mFormat,
                AudioFormat.ENCODING_PCM_16BIT, minSize * 4,
                AudioTrack.MODE_STREAM);
        mSonic = new Sonic(sampleRate, numChannels);
        mLock.unlock();
    }

    private void decode() {
        mDecoderThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mIsDecoding = true;
                try {
                    mCodec.start();

                    ByteBuffer[] inputBuffers = mCodec.getInputBuffers();
                    ByteBuffer[] outputBuffers = mCodec.getOutputBuffers();

                    boolean sawInputEOS = false;
                    boolean sawOutputEOS = false;

                    while (!sawInputEOS && !sawOutputEOS && mContinue) {
                        if (mCurrentState == STATE_PAUSED) {
                            try {
                                Thread.sleep(99999999);
                            } catch (InterruptedException e) {
                                // Purposely not doing anything here
                            }
                            continue;
                        }

                        if (null != mSonic) {
                            mSonic.setSpeed(mCurrentSpeed);
                            mSonic.setPitch(mCurrentPitch);
                        }
                        Log.e("TAG", "decode mCurrentSpeed " + mCurrentSpeed);
                        int inputBufIndex = mCodec.dequeueInputBuffer(200);
                        if (inputBufIndex >= 0) {
                            ByteBuffer dstBuf = inputBuffers[inputBufIndex];
                            int sampleSize = mExtractor.readSampleData(dstBuf, 0);
                            long presentationTimeUs = 0;
                            if (sampleSize < 0) {
                                sawInputEOS = true;
                                sampleSize = 0;
                            } else {
                                presentationTimeUs = mExtractor.getSampleTime();
                            }
                            mCodec.queueInputBuffer(
                                    inputBufIndex,
                                    0,
                                    sampleSize,
                                    presentationTimeUs,
                                    sawInputEOS ? MediaCodec.BUFFER_FLAG_END_OF_STREAM
                                            : 0);
                            if (!sawInputEOS) {
                                mExtractor.advance();
                            }
                        }

                        final MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                        byte[] modifiedSamples = new byte[info.size];

                        int res;
                        do {
                            res = mCodec.dequeueOutputBuffer(info, 200);
                            if (res >= 0) {
                                final byte[] chunk = new byte[info.size];
                                outputBuffers[res].get(chunk);
                                outputBuffers[res].clear();

                                if (chunk.length > 0) {
                                    mSonic.writeBytesToStream(chunk, chunk.length);
                                } else {
                                    mSonic.flushStream();
                                }
                                int available = mSonic.samplesAvailable();
                                if (available > 0) {
                                    if (modifiedSamples.length < available) {
                                        modifiedSamples = new byte[available];
                                    }
                                    mSonic.readBytesFromStream(modifiedSamples, available);
                                    mTrack.write(modifiedSamples, 0, available);
                                }

                                mCodec.releaseOutputBuffer(res, false);

                                if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                                    sawOutputEOS = true;
                                }
                            } else if (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                                outputBuffers = mCodec.getOutputBuffers();
                                Log.d("PCM", "Output buffers changed");
                            } else if (res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                                mTrack.stop();
                                mLock.lock();
                                mTrack.release();
                                final MediaFormat oformat = mCodec
                                        .getOutputFormat();
                                Log.d("PCM", "Output format has changed to"
                                        + oformat);
                                initDevice(
                                        oformat.getInteger(MediaFormat.KEY_SAMPLE_RATE),
                                        oformat.getInteger(MediaFormat.KEY_CHANNEL_COUNT));
                                outputBuffers = mCodec.getOutputBuffers();
                                mTrack.play();
                                mLock.unlock();
                            }
                        } while (res == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED
                                || res == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED);
                    }
                    Log.d(TAG_TRACK,
                            "Decoding loop exited. Stopping codec and track");
                    Log.d(TAG_TRACK, "Duration: " + (int) (mDuration / 1000));
                    Log.d(TAG_TRACK,
                            "Current position: "
                                    + (int) (mExtractor.getSampleTime() / 1000));
                    mCodec.stop();
                    mTrack.stop();
                    Log.d(TAG_TRACK, "Stopped codec and track");
                    Log.d(TAG_TRACK,
                            "Current position: "
                                    + (int) (mExtractor.getSampleTime() / 1000));
                    mIsDecoding = false;
                    if (mContinue && (sawInputEOS || sawOutputEOS)) {
                        mCurrentState = STATE_PLAYBACK_COMPLETED;
                        Intent intent = new Intent("end_of_file");
                        localBM.sendBroadcast(intent);
                        Thread t = new Thread(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                        t.setDaemon(true);
                        t.start();
                    } else {
                        Log.d(TAG_TRACK,
                                "Loop ended before saw input eos or output eos");
                        Log.d(TAG_TRACK, "sawInputEOS: " + sawInputEOS);
                        Log.d(TAG_TRACK, "sawOutputEOS: " + sawOutputEOS);
                    }
                }catch(Exception e){
                    // aucune idée mais ça a planté 1x
                }
            }
        });

        mDecoderThread.setDaemon(true);
        mDecoderThread.start();
    }
}