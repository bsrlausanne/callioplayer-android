package utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import services.LecteurService;
import tasks.DeleteTask;
import tasks.ScanTask;
import tasks.WebcallTask;

/**
 * Created by simou on 24.05.2016.
 */
public class Bibliotheque {

    private final static String PREF_TAG = "bibliotheque";
    static private LocalBroadcastManager localBM = null;

    private ArrayList<LivreDaisy> mLivres = new ArrayList<>();

    private Map<String, FicheLivre> mCacheFiches = new HashMap<>();

    private static Context mContext;
    private SharedPreferences mPrefs;
    private static String mBookFolder;

    private static ListeFiches mWishList = null;

    private int mCurrentBookId;

    private static Bibliotheque theBibliotheque = null;

    public Bibliotheque(Context context){
        mContext = context;
        mPrefs = context.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        mCurrentBookId = mPrefs.getInt("active_book", 0);
        localBM = LocalBroadcastManager.getInstance(context);

        checkExternalStorage();
        // Pour ne pas utiliser le cache, on clean au début
        //freshScanLivres();
        scanLivres();

    }

    public static Bibliotheque getBibliotheque(Context context) {

        if(theBibliotheque == null){
            theBibliotheque = new Bibliotheque(context);
        }
        return theBibliotheque;
    }

    public static String getString(int id) {
        return mContext.getResources().getString(id);
    }

    public void scanLivres(){
        boolean anyChange = false;

        if(mLivres.size() == 0){
            restoreLivres();
        }

        // vérification de l'existence des livres stockés dans le cache
        for (int i = 0 ; i < mLivres.size() ; i++){
            String bookPath = mLivres.get(i).getBookPath();

            if( !(new File(bookPath).exists() )){
                retirerLivre(mLivres.get(i));
                anyChange = true;
            }else{
                if(!LecteurService.isRunning) {
                    mLivres.get(i).setPlaying(false);
                }
            }
        }
        ScanTask st = new ScanTask(mContext, this);
        st.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        if(anyChange) {
            storeLivres();
        }
    }

    public ArrayList<LivreDaisy> getAllLivres(){
        return mLivres;
    }

    public void storeLivres(){
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(mLivres.toArray());
        prefsEditor.putString("livres", json);
        prefsEditor.apply();
    }

    private void restoreLivres(){
        Gson gson = new Gson();
        String json = mPrefs.getString("livres", "");

        if(!json.equals("")) {
            try {
                ArrayList<Integer> tempList = new ArrayList<>();
                LivreDaisy[] livresObjects = gson.fromJson(json, LivreDaisy[].class);
                ArrayList<LivreDaisy> tempLivres = new ArrayList<>(Arrays.asList(livresObjects));

                // effacement doublons
                for(int i = 0; i< tempLivres.size(); i++){

                    LivreDaisy currentLivre = tempLivres.get(i);
                    if(tempList.contains( currentLivre.getId())){
                        Log.d("TESTSCAN","doublon: "+ currentLivre.getTitle());
                    } else{
                        tempList.add(currentLivre.getId());
                        mLivres.add(tempLivres.get(i));
                    }
                }
            } catch(JsonSyntaxException e){
                Log.e("Bibliotheque", "Pas pu restaurer les livres, probablement structure changée.");
            }
        } else{
            Log.d("Bibliotheque", "Pas encore de livres stockés.");
        }
    }

    /*private void freshScanLivres(){
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("livres", "");
        prefsEditor.putInt("active_book", 0);
        prefsEditor.putInt("last_book", 0);
        prefsEditor.commit();
        scanLivres();
    }*/

    public void ajouterLivres(LivreDaisy livre){
        if(!mLivres.contains(livre)) {
            mLivres.add(livre);
        }
    }

    private void retirerLivre(LivreDaisy livre){
        int bookId = livre.getId();
        if(mCurrentBookId == bookId){
            sendToService("pause");
            sendToService("hide_notif");
            mCurrentBookId=0;
        }
        DeleteTask dt = new DeleteTask(mContext, livre.getTitle());
        dt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, livre.getBookPath());
        mLivres.remove(livre);
    }

    private void effacerLivre(LivreDaisy livre){
        String path = livre.getBookPath();
        File file = new File(path);
        if(!file.exists()){
            Log.e("Bibliotheque", "Le livre n'existe pas à cet endroit: "+path);
            return;
        }

        retirerLivre(livre);
    }

    void effacerLivre(int bookId){
        LivreDaisy bookToBeDeleted = getLivreById(bookId);
        if(bookToBeDeleted != null) {
            effacerLivre(bookToBeDeleted);
        } else{
            Log.e("Bibliotheque", "Pas pu charger livre à effacer: "+bookId);
        }
    }

    public LivreDaisy getLivreById(int bookIdentifier){
        for(int i = 0 ; i< mLivres.size(); i++ ){
            LivreDaisy tempLivre = mLivres.get(i);
            if(tempLivre==null){
                Log.e("Bibliotheque", "tempLivre null");
                return null;
            }
            if( tempLivre.getId() == bookIdentifier){
                return tempLivre;
            }
        }
        return null;
    }

    public LivreDaisy getActiveLivre(){
        return getLivreById(mCurrentBookId);
    }

    public void setActiveLivre(int bookId){
        mCurrentBookId = bookId;
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        editor.putInt("active_book", bookId);
        editor.apply();
    }

    public int size() {
        return mLivres.size();
    }

    public boolean bookInLibrary(String bookName){
        int tempId = (mBookFolder+bookName).hashCode();
        return getLivreById(tempId) != null;
    }
    public boolean bookInLibraryByNoticeNr(String noticeNr) {
        for (int i = 0; i < mLivres.size(); i++) {
            if(mLivres.get(i).getFolderName().equals(noticeNr)){
                Log.d("bookInLibraryByNoticeNr", "trouvé: "+mLivres.get(i).getTitle());
                return true;
            }
        }
        return false;
    }


    private void checkExternalStorage() {
        File bookPath;
        // getExternalStorageState(File).
        File[] externalFolders = ContextCompat.getExternalFilesDirs(mContext, Environment.DIRECTORY_MUSIC);

        int biggestIndex = 0;
        for(int i=0; i<externalFolders.length; i++){
            if(externalFolders[i] == null){
                continue;
            }
            boolean isMountedWritable = true;
            if(!externalFolders[i].exists()){
                externalFolders[i].mkdir();
            }

            String state;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                state = Environment.getExternalStorageState(externalFolders[i]);
                if(!(state.equals(Environment.MEDIA_MOUNTED))) isMountedWritable = false;
            }

            if(isMountedWritable && externalFolders[i].getFreeSpace() > externalFolders[biggestIndex].getFreeSpace()){
                biggestIndex = i;
            }
            Log.d("Storage", externalFolders[i].toString() + "/"+externalFolders[i].getFreeSpace()/1000000);
        }
        bookPath = externalFolders[biggestIndex];

        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        String bookFolder = "";
        if(bookPath == null){
            bookPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
            if(!bookPath.exists()){
                bookPath.mkdir();
            }
        }else {
            bookFolder = bookPath.getAbsolutePath() + "/";
        }
        mBookFolder = bookFolder;

        editor.putString("book_folder", bookFolder);
        Log.d("Storage", "Dossier de stockage: " + bookFolder + " / espace libre" + bookPath.getTotalSpace() / 1000000 + "Go.");

        editor.apply();
    }
    public long getFreeSpace(){
        return new File(mBookFolder).getFreeSpace();
    }

    public void addFiche(FicheLivre fiche){
        mCacheFiches.put(fiche.getNoticeNr(), fiche);
    }

    public FicheLivre getFiche(String noticeNr){
        return mCacheFiches.get(noticeNr);
    }
    public static String getBookFolder() {
        return mBookFolder;
    }

    public ArrayList<LivreDaisy> getLivresEnLecture() {
        ArrayList<LivreDaisy> result = new ArrayList<>();
        for (int i = 0; i < mLivres.size(); i++) {
            if (mLivres.get(i).getPlayDate() > 0) {
                result.add(mLivres.get(i));
            }

        }
        return result;
    }

    public void login(Handler handler) {

        SharedPreferences prefs = mContext.getSharedPreferences("ANDROID_BSR", Context.MODE_PRIVATE);
        String lastUsername = prefs.getString("username", null);
        String lastPassword = prefs.getString("password", null);
        String appVersion = prefs.getString("app_version", "");
        String androidVersion = prefs.getString("android_version", "");

        String[] paramsList = new String[3];
        paramsList[0]=lastUsername;
        paramsList[1]=lastPassword;
        paramsList[2]="BSR_android_v"+appVersion+"_android_"+androidVersion;

        StringBuilder params = new StringBuilder();
        for(
                int i = 0;
                i<paramsList.length;i++) {
            params.append("&p").append(i).append("=").append(paramsList[i]);
        }

        String request = "Authenticate" + params;

        WebcallTask wt = new WebcallTask(mContext, handler);
        wt.execute(request);
    }

    public static void setWishList(ListeFiches wishList){
        mWishList = wishList;
    }

    public void addWishToList(FicheLivre fiche){
        if(mWishList != null){
            mWishList.addItem(fiche);
        }
    }
    public void removeWishFromList(String noticeNr){
        if(mWishList != null){
            mWishList.removeItem(noticeNr);
        }
    }
    public boolean isOnWishList(String noticeNr) {
        if(mWishList != null){
            return mWishList.isOnList(noticeNr);
        } else{
            return false;
        }
    }

    private void sendToService(String action) {
        if(LecteurService.isRunning) {
            sendLocalBroadCast(action);
        } /*else{
            Intent lecteurServiceIntent = new Intent(context.getApplicationContext(), LecteurService.class);
            if(!action.equals("")){
                lecteurServiceIntent.putExtra("action", action);
            }
            context.startService(lecteurServiceIntent);
        }*/
    }

    private static void sendLocalBroadCast(String intentName) {
        Intent intent = new Intent(intentName);
        localBM.sendBroadcast(intent);
    }

    public void storeSetting(String settingName, int value) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        editor.putInt(settingName, value);
        editor.apply();
    }

    public int readSetting(String settingName) {
        mPrefs = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        return mPrefs.getInt(settingName, 0);
    }
    
}
