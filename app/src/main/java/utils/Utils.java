package utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import bibliothequesonore.ch.livresbsrandroid.R;
import tasks.ConnectivityCheckTask;

/**
 * Created by Simon on 24.08.2017.
 */

public class Utils {
    static public void prepareAndShowAlert(final Context context, final AlertDialog alert){
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alert.getButton(Dialog.BUTTON_POSITIVE).setTextSize(context.getResources().getDimension(R.dimen.alertFontSize));
                alert.getButton(Dialog.BUTTON_NEGATIVE).setTextSize(context.getResources().getDimension(R.dimen.alertFontSize));
            }
        });

        alert.show();
        TextView textView = alert.findViewById(android.R.id.message);
        if(textView != null) {
            textView.setTextSize(context.getResources().getDimension(R.dimen.alertFontSize));
        }

        Button negativeButton = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        Button positiveButton = alert.getButton(AlertDialog.BUTTON_POSITIVE);

        negativeButton.setTextColor(ContextCompat.getColor(context, R.color.bsr_blue));
        positiveButton.setTextColor(ContextCompat.getColor(context, R.color.bsr_blue));
    }
    static public void showToast(Context context, String message){
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        ViewGroup group = (ViewGroup) toast.getView();
        if(group != null) {
            TextView messageTextView = (TextView) group.getChildAt(0);
            messageTextView.setTextSize(context.getResources().getDimension(R.dimen.alertFontSize));
        }
        toast.show();
    }

    static public void showSnack(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.bsr_blue));
        TextView tv = (snackbar.getView()).findViewById(com.google.android.material.R.id.snackbar_text);

        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(snackBarView.getResources().getColor(R.color.white));

        tv.setTextSize(15);

        snackbar.show();
    }

    public static void isInternetAvailable(Context context, Boolean refreshLibrary) {
        ConnectivityCheckTask cct = new ConnectivityCheckTask(context, refreshLibrary);
        cct.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void sendTBMessage(Context context, String message){
        AccessibilityManager manager = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        assert manager != null;
        if (!manager.isEnabled()) {
            return;
        }
        AccessibilityEvent e = AccessibilityEvent.obtain();
        e.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
        e.setClassName("Bibliotheque");
        e.setPackageName(context.getPackageName());
        e.getText().add(message);
        manager.sendAccessibilityEvent(e);
    }

    public static String truncate(final String content, final int length) {

        if (content == null) { return ""; }

        int lastIndex = length-1;
        if (content.length() < length) {
            return content;
        }

        int firstSpacePos = content.indexOf(" ");
        if (firstSpacePos > lastIndex || firstSpacePos <= 0){
            return content;
        }

        String result = content.substring(0, lastIndex+1);
        result = result.substring(0, result.lastIndexOf(" "));

        result += "...";

        return result;
    }

    public static String formatDuration(String nbDuration, Context context){
        String result = nbDuration.replace(":", "h. ") + "min.";
        result = result.replace("00h. ", "");
        if(result.startsWith("0")){result = result.substring(1);}
        if(result.startsWith("h. ")){result = result.replace("h. ", "");}
        if(result.contains("h. 0")){result = result.replace("h. 0", "h. ");}
        if(result.contains("h. 0min.")){result = result.replace("h. 0min.", "h.");}
        result = result.replace("h", context.getResources().getString(R.string.Global_H));
        result = result.replace("min", context.getResources().getString(R.string.Global_M));
        return result;
    }
    public static String formatAccessibleDuration(String nbDuration, Context context){
        String result = nbDuration.replace(":", "heures ") + "minutes";
        result = result.replace("00heures ", "");
        if(result.startsWith("0")){result = result.substring(1);}
        if(result.startsWith("h. ")){result = result.replace("h. ", "");}
        if(result.contains("heures 0")){result = result.replace("heures 0", "heures ");}
        if(result.contains("heures 0minutes")){result = result.replace("heures 0minutes", "heures");}
        result = result.replace("heures", " "+context.getResources().getString(R.string.Global_Heures));
        result = result.replace("minutes", " "+context.getResources().getString(R.string.Global_Minutes));
        return result;
    }

}
