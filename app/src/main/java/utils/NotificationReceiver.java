package utils;

import services.LecteurService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

public class NotificationReceiver extends BroadcastReceiver {

    static private LocalBroadcastManager localBM = null;

    public NotificationReceiver() {
        super();
    }

    private static void sendLocalBroadCast(String intentName) {

        Intent intent = new Intent(intentName);
        localBM.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        Log.d("NotificationReceiver", "onreceive: "+action);
        if (action != null && !(
                action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_PLAY") ||
                        action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_NEXT") ||
                        action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_CLOSE") ||
                        action.equalsIgnoreCase("android.intent.action.ACTION_SHUTDOWN") ||
                        action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_PREV")
        )) {
            Log.d("NotificationReceiver", "reçu une action pas liée à Callioplayer:" + action);
            return;
        }

        if(localBM == null) {
            localBM = LocalBroadcastManager.getInstance(context);
        }

        sendToService(context, "update_service");

        if (action != null && action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_PLAY")) {
            Log.d("NotificationReceiver", "play");
            sendToService(context, "play");
        }
        if (action != null && action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_NEXT")) {
            sendToService(context, "jumpNext");
            Log.d("NotificationReceiver", "jumpNext");

        }
        if (action != null && action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_PREV")) {
            Log.d("NotificationReceiver", "jumpPrev");
            sendToService(context, "jumpPrev");
        }
        if (action != null && action.equalsIgnoreCase("ch.bsr.lecteurdaisyandroid.ACTION_CLOSE")) {
            Log.d("NotificationReceiver", "close, isrunning: "+LecteurService.isRunning );
            sendToService(context, "hide_notif");
            sendToService(context, "closeApp");
        }
        if (action.equalsIgnoreCase("android.intent.action.ACTION_SHUTDOWN")){
            Log.d("destruction2", "onReceive: destruction");
            sendToService(context, "closeApp");
        }
    }

    private void sendToService(Context context, String action) {
        Log.d("NOTIFF", "sendToService" );
        if(LecteurService.isRunning) {
            Log.d("NOTIFF", "sendlocalbc: "+action );
            sendLocalBroadCast(action);

        } /* else{
            Intent lecteurServiceIntent = new Intent(context.getApplicationContext(), LecteurService.class);
            if(!action.equals("")){
                lecteurServiceIntent.putExtra("action", action);
            }
            context.startService(lecteurServiceIntent);
        }*/
    }
}

