package activities;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collections;

import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.MyLibraryFragment;
import services.DownloadService;
import utils.DownloadData;
import utils.Utils;

public class DownloadsActivity extends AppCompatActivity implements Runnable{

	public static ArrayList<DownloadData> downloads = new ArrayList<>();
    DownloadManager dm;
    boolean isActivityVisible = false;
    ListView dlListView;
    TextView downloadTitle;
    ImageButton previousActivityButton;

    DownloadsAdapter dla;

    static int toDeleteLivreId = 0;
    static boolean activeDownloads = false;

    public static void updateUCT(String noticeNr, long progress) {

        Log.d("DOWNLOADS", noticeNr+"/"+progress+"/"+downloads.size());

        for (int i = 0 ; i<downloads.size();i++) {
            if (downloads.get(i).noticeNr.equals(noticeNr)) {
                if (progress == 100) {
                    Log.d("DOWNLOADS", noticeNr+" terminé");
                    downloads.remove(i);
                    if (downloads.size() == 0) {
                        Log.d("DOWNLOADS", "c'était le dernier");
                        activeDownloads = false;
                        MyLibraryFragment.disableDownloadsButton();
                    }

                    return;
                } else {
                    downloads.get(i).uncompressPercent = progress;
                }
            }
        }
    }

    void removeDownload(long downloadId) {

        Log.d("DOWNLOADS", " suppression download");
        for(int i = 0 ; i<downloads.size();i++) {
            if(downloads.get(i).downloadId == downloadId){
                if(downloads.get(i).uncompressPercent >= 0){
                    Log.d("DOWNLOADS", " suppression task: "+downloads.get(i).noticeNr);
                    DownloadService.cancelUCT(downloads.get(i).noticeNr);
                }
                downloads.remove(i);

                Log.d("DOWNLOADS", " trouvé pour annulation.");
            }
        }
        Log.d("DOWNLOADS", "size: " + downloads.size());
        if(downloads.size() == 0){
            MyLibraryFragment.disableDownloadsButton();
            DownloadService.stopService();
        }
    }

    private void updateDownloads(long dlId, String description, String noticeNr, long progress) {

        // Log.d("DOWNLOADS", "update dl: "+noticeNr);
        for (int i = 0 ; i<downloads.size();i++) {
            if(downloads.get(i).noticeNr.equals(noticeNr)){
                //Log.d("DOWNLOADS", "update 2 dl: "+noticeNr+"/"+progress);
                downloads.get(i).downloadPercent = progress;
                return;
            }
        }

        Log.d("DOWNLOADS", "ajout dl: "+noticeNr);
        // pas trouvé dans les downloads, c'est un nouveau
        downloads.add(new DownloadData(dlId, description, noticeNr, progress));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_downloads);
        
        dlListView = (ListView) findViewById(R.id.downloadsView);
        downloadTitle = (TextView)findViewById(R.id.downloadTitle);
        previousActivityButton = findViewById(R.id.previousBarButton);
        previousActivityButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        displayDownloadList();

        Thread thread = new Thread(this);    
        thread.start();
        isActivityVisible = true;
    }

    @Override                    
    public void run() {       
    	while(activeDownloads ){
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                Log.e("DownloadActivity", "run: "+e.toString());
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                	displayDownloadList();
               }
           });
    	}
    }
    
    void displayDownloadList() {
    	//downloads.clear();

        Query query = new Query();
        query.setFilterByStatus(DownloadManager.STATUS_RUNNING);

        Cursor c = dm.query(query);
        while (c.moveToNext()) {

            // récupération des index
            int sizeIndex = c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
            int downloadedIndex = c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
            int descIndex = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
            int titleIndex = c.getColumnIndex(DownloadManager.COLUMN_TITLE);
            int idIndex = c.getColumnIndex(DownloadManager.COLUMN_ID);

            // récupération des valeurs
            String description = c.getString(titleIndex);
            String noticeNr = c.getString(descIndex);
            long dlId = c.getLong(idIndex);
            long size = c.getInt(sizeIndex);
            long downloaded = c.getInt(downloadedIndex);
            
            // calcul du progres
            long progress = 0;
            if (size != -1 && size != 0) progress = downloaded * 100 / size;
            updateDownloads(dlId, description, noticeNr, progress);

        }
        c.close();
        if (downloads.size() == 0) {
        	activeDownloads = false;
        	if(isActivityVisible) {
                this.onBackPressed();
            }
        	downloadTitle.setText(getResources().getString(R.string.Catalogue_AucunTelechargement));
        	dlListView.setVisibility(View.INVISIBLE);

        } else {
        	activeDownloads = true;
		    if(dla == null){
                dla = new DownloadsAdapter(downloads);
                dlListView.setAdapter(dla);
            } else{
                dla.updateDownloads(downloads);
                dla.notifyDataSetChanged();
            }
            dlListView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), android.R.color.white));
		    // dlListView.setBackgroundColor(getResources().getColor(android.R.color.white));
		    dlListView.setVisibility(View.VISIBLE);
        }
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        Log.d("CANCELBUTTON", "OnClickListener");
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                //Log.d("dialogClickListener", "Cancelling Id: "+currentDownloadId);
                dm.remove(DownloadsActivity.toDeleteLivreId);
                removeDownload(toDeleteLivreId);
                displayDownloadList();
                Utils.sendTBMessage(getApplicationContext(), getDescriptionForId(toDeleteLivreId)+ getApplicationContext().getResources().getString(R.string.Catalogue_ConfirmationAnnulation)+"?");
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                //Log.d("dialogClickListener", "Not Cancelling Id: "+currentDownloadId);
                break;
        }
        }
    };

    private static class ViewHolder {

        private TextView titleText;
        private TextView stepText;
        private TextView progressText;

        private ImageView deleteImage;
        private ImageView wishlistImage;
        private ImageView downloadImage;

    }

    class DownloadsAdapter extends BaseAdapter {
    	ArrayList<DownloadData> downloads;

        DownloadsAdapter(ArrayList<DownloadData> downloads) {
        	Collections.sort(downloads);
            this.downloads = downloads;
        }

        void updateDownloads(ArrayList<DownloadData> newDownloads){
            downloads = newDownloads;
        }

        View.OnClickListener cancelDownloadListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toDeleteLivreId = (int)v.getTag();
                AlertDialog.Builder builder = new AlertDialog.Builder(DownloadsActivity.this);
                builder.setMessage(DownloadsActivity.this.getResources().getString(R.string.Catalogue_ConfirmationAnnulation)+":\n"+getDescriptionForId(toDeleteLivreId)).setPositiveButton(DownloadsActivity.this.getResources().getString(R.string.Global_Oui), dialogClickListener)
                        .setNegativeButton(getResources().getString(R.string.Global_Non), dialogClickListener);

                AlertDialog alert = builder.create();
                Utils.prepareAndShowAlert(getApplicationContext(), alert);
            }
        };

        @Override
        public int getCount() {
            return downloads.size();
        }

        @Override
        public DownloadData getItem(int i) {
            return downloads.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            // Log.d("HomeBookListAdapter", "getView " + i);
            LayoutInflater li = (LayoutInflater) DownloadsActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
            assert (li != null);
            ViewHolder holder;

            if (convertView == null) {
                convertView = li.inflate(R.layout.book_row_text, viewGroup, false);
                assert (convertView != null);

                holder = new ViewHolder();
                holder.titleText = (TextView) convertView.findViewById(R.id.titleText);
                holder.stepText = (TextView) convertView.findViewById(R.id.authorText);
                holder.progressText = (TextView) convertView.findViewById(R.id.mediaText);

                holder.deleteImage = (ImageView)convertView.findViewById(R.id.book_action_1);
                holder.wishlistImage = (ImageView)convertView.findViewById(R.id.book_action_2);
                holder.downloadImage = (ImageView)convertView.findViewById(R.id.book_action_3);

                convertView.setTag(R.string.Tags_Holder, holder);
            } else {
                holder = (ViewHolder) convertView.getTag(R.string.Tags_Holder);
            }


            holder.wishlistImage.setVisibility(View.GONE);
            holder.downloadImage.setVisibility(View.GONE);

            DownloadData currentDownload = downloads.get(i);
            holder.deleteImage.setContentDescription(getResources().getString(R.string.Catalogue_AnnulerTelechargement)+"\n\n "+currentDownload.description);
            holder.deleteImage.setImageResource(R.drawable.ic_delete_white_48dp);
            holder.deleteImage.setOnClickListener(cancelDownloadListener);
            convertView.setOnClickListener(cancelDownloadListener);

            convertView.setContentDescription(currentDownload.description);

            holder.titleText.setText(currentDownload.description);
            if (currentDownload.uncompressPercent < 0) {
                holder.stepText.setText(getResources().getString(R.string.Notification_Telechargemenent));
                holder.progressText.setText((getResources().getString(R.string.Global_Progression)+ " " + currentDownload.downloadPercent/2 + "%"));
                convertView.setContentDescription(currentDownload.description+ " " + currentDownload.downloadPercent/2 + "%");
            } else if (currentDownload.uncompressPercent == 0) {
                holder.stepText.setText(getResources().getString(R.string.Notification_Verification));
                holder.progressText.setText((getResources().getString(R.string.Global_Progression)+ " 50%"));
                convertView.setContentDescription(currentDownload.description+ " 50%");
            } else{
                holder.stepText.setText(getResources().getString(R.string.Notification_Decompression));
                holder.progressText.setText((getResources().getString(R.string.Global_Progression)+ " " + (50+(currentDownload.uncompressPercent)/2) + "%"));
                convertView.setContentDescription(currentDownload.description+ " " + (50+(currentDownload.uncompressPercent)/2) + "%");
            }
            // noticeText.setText(currentDownload.noticeNr);

            holder.deleteImage.setTag((int)currentDownload.downloadId);
            convertView.setTag((int)currentDownload.downloadId);
            holder.deleteImage.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
            return convertView;
        }
    }

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // do nothing
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public String getDescriptionForId(int id){

        for(int i = 0 ; i<downloads.size();i++){
            if(downloads.get(i).downloadId == id){
                return downloads.get(i).description;
            }
        }

        return "";
    }

    @Override
    public void onBackPressed() {

        Log.d("DownloadsActivity", "onBackPressed Called");
        isActivityVisible = false;
        //if(isTaskRoot()) {
        try {
            Intent setIntent = new Intent(Intent.ACTION_MAIN, Uri.EMPTY, getApplicationContext(), HomeActivity.class);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(setIntent);
            super.onBackPressed();
        } catch (Exception e) {
            // This will catch any exception, because they are all descended from Exception
            System.out.println("Error onbackpressed " + e.getMessage());
        }
        //}
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityVisible = false;
    }
}
