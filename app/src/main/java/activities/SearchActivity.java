package activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import bibliothequesonore.ch.livresbsrandroid.R;
import services.DownloadService;
import tasks.WebcallTask;
import utils.Bibliotheque;
import utils.FicheLivre;
import utils.JSonManager;
import utils.ListeFiches;
import utils.MemoryCache;
import utils.Utils;

//TODO: -language
//      -search history using shared preferences


public class SearchActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private final static int WCT = 1;

    // For endless scrolling
    private final static int VISIBLE_THRESHOLD = 4;
    private final static int ITEM_PER_PAGE = 10;
    private int page = 0;
    private boolean isLoading = false;
    private boolean new_request = true;
    private String request;
    private int widthScreen;

    public static ListeFiches wishList = null;
    private static boolean playing = false;
    public static MediaPlayer mediaPlayer;

    Bibliotheque mBib;
    ListeFiches resultList = null;
    int resultCount = 0;
    private long mLastClickTime = 0;
    private View mLastClickedView;
    private MemoryCache memoryCache;
    private String viewType = "cover";

    private List<CheckBoxItem> listCheckBoxTypeId;
    private List<CheckBoxItem> listCheckBoxGenreId;
    private List<CheckBoxItem> listCheckBoxProducerId;
    ListView listCheckBoxType;
    ListView listCheckBoxGenre;
    ListView listCheckBoxProducer;
    SearchOptionsAdapter listCheckBoxTypeAdapter;
    SearchOptionsAdapter listCheckBoxGenreAdapter;
    SearchOptionsAdapter listCheckBoxProducerAdapter;

    private Map<String, Integer> listFilterAll;
    ListView listFilterAllView;
    FilterViewAdapter listFilterAdapterAll;

    View.OnClickListener playBookListener;
    DialogInterface.OnClickListener dialog2ClickListener;

    EditText searchText;
    ImageButton previousButton, clearTextButton;
    Button filtersButton;
    ListView bookListView;
    TextView pageAmount;
    ImageView lastPlayedImage = null;

    String query_type = "";
    String query_producer = "ALL";
    List<String> queryGenreList = new ArrayList<>();
    boolean youth_literature_only = false;
    boolean exclude_tts = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Add All as default list for search query
        queryGenreList.add("ALL");

        //Screen size
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        widthScreen = size.x;

        memoryCache = new MemoryCache(this);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);

        mBib = Bibliotheque.getBibliotheque(getApplicationContext());
        searchText = findViewById(R.id.searchQueryText);
        previousButton = findViewById(R.id.previousBarButton);
        clearTextButton = findViewById(R.id.clearTextButton);
        filtersButton = findViewById(R.id.filtersButton);

        bookListView = findViewById(R.id.bookListView);
        pageAmount = findViewById(R.id.pageAmount);

        listFilterAllView= findViewById(R.id.filterAll);
        listFilterAll = new HashMap<>();

        addCheckboxes();

        initListeners();
    }

    @Override
    protected void onResume() {

        super.onResume();

        // init the view type which can be cover, text or mixed of the two
        viewType = HomeActivity.getViewType();

        Utils.isInternetAvailable(getApplicationContext(), true);
        mBib.login(null);

        Log.d("SearchResult", "v: " + searchText.getText().toString().equals(""));
        listFilterAdapterAll = new FilterViewAdapter(listFilterAll);
        listFilterAllView.setAdapter(listFilterAdapterAll);
        if (!searchText.getText().toString().equals("")) {
            search();
        }
    }

    @Override
    protected void onPause() {

        if(mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        playing = false;
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // override to prevent crash with physical menu button on some phones.

        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            // do nothing
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void addCheckboxes() {

        listCheckBoxTypeId = new ArrayList<>();
        listCheckBoxTypeId.add(new CheckBoxItem("", R.string.Recherche_TypeAll));
        listCheckBoxTypeId.add(new CheckBoxItem("title", R.string.Recherche_TypeTitle));
        listCheckBoxTypeId.add(new CheckBoxItem("author", R.string.Recherche_TypeAuthor));
        listCheckBoxTypeId.add(new CheckBoxItem("code", R.string.Recherche_TypeNumber));
        listCheckBoxTypeId.add(new CheckBoxItem("reader", R.string.Recherche_TypeReader));

        listCheckBoxGenreId = new ArrayList<>();
        listCheckBoxGenreId.add(new CheckBoxItem("ALL", R.string.Recherche_GenreAll));
        listCheckBoxGenreId.add(new CheckBoxItem("AR", R.string.Recherche_GenreArts));
        listCheckBoxGenreId.add(new CheckBoxItem("AU", R.string.Recherche_GenreAudioDescr));
        listCheckBoxGenreId.add(new CheckBoxItem("B", R.string.Recherche_GenreBioTem));
        listCheckBoxGenreId.add(new CheckBoxItem("CT", R.string.Recherche_GenreContesLeg));
        listCheckBoxGenreId.add(new CheckBoxItem("D", R.string.Recherche_GenreDoc));
        listCheckBoxGenreId.add(new CheckBoxItem("EL", R.string.Recherche_GenreEssay));
        listCheckBoxGenreId.add(new CheckBoxItem("HG", R.string.Recherche_GenreHistGeo));
        listCheckBoxGenreId.add(new CheckBoxItem("U", R.string.Recherche_GenreHumour));
        listCheckBoxGenreId.add(new CheckBoxItem("X", R.string.Recherche_GenreLitErot));
        listCheckBoxGenreId.add(new CheckBoxItem("N", R.string.Recherche_GenreNouvelle));
        listCheckBoxGenreId.add(new CheckBoxItem("SE", R.string.Recherche_GenrePeriodique));
        listCheckBoxGenreId.add(new CheckBoxItem("PR", R.string.Recherche_GenrePhiloReliSpir));
        listCheckBoxGenreId.add(new CheckBoxItem("PS", R.string.Recherche_GenrePoem));
        listCheckBoxGenreId.add(new CheckBoxItem("P", R.string.Recherche_GenrePolice));
        listCheckBoxGenreId.add(new CheckBoxItem("PC", R.string.Recherche_GenrePratUseCook));
        listCheckBoxGenreId.add(new CheckBoxItem("PM", R.string.Recherche_GenrePsychMed));
        listCheckBoxGenreId.add(new CheckBoxItem("BSR", R.string.Recherche_GenrePubBSR));
        listCheckBoxGenreId.add(new CheckBoxItem("R", R.string.Recherche_GenreRomAdven));
        listCheckBoxGenreId.add(new CheckBoxItem("RA", R.string.Recherche_GenreRomAmour));
        listCheckBoxGenreId.add(new CheckBoxItem("A", R.string.Recherche_GenreRomAnimal));
        listCheckBoxGenreId.add(new CheckBoxItem("FA", R.string.Recherche_GenreRomFanta));
        listCheckBoxGenreId.add(new CheckBoxItem("H", R.string.Recherche_GenreRomHist));
        listCheckBoxGenreId.add(new CheckBoxItem("RT", R.string.Recherche_GenreRomTerroir));
        listCheckBoxGenreId.add(new CheckBoxItem("F", R.string.Recherche_GenreSciFi));
        listCheckBoxGenreId.add(new CheckBoxItem("ST", R.string.Recherche_GenreSciTech));
        listCheckBoxGenreId.add(new CheckBoxItem("EP", R.string.Recherche_GenreSociEcoPol));
        listCheckBoxGenreId.add(new CheckBoxItem("TH", R.string.Recherche_GenreTheater));
        listCheckBoxGenreId.add(new CheckBoxItem("VO", R.string.Recherche_GenreVoyExplo));

        listCheckBoxProducerId = new ArrayList<>();
        listCheckBoxProducerId.add(new CheckBoxItem("ALL", R.string.Recherche_ProducerAny));
        listCheckBoxProducerId.add(new CheckBoxItem("AVH", R.string.Recherche_ProducerAVH));
        listCheckBoxProducerId.add(new CheckBoxItem("BBR", R.string.Recherche_ProducerBBR));
        listCheckBoxProducerId.add(new CheckBoxItem("BSR", R.string.Recherche_ProducerBSR));
        listCheckBoxProducerId.add(new CheckBoxItem("CM", R.string.Recherche_ProducerCommerce));
        listCheckBoxProducerId.add(new CheckBoxItem("GIAA", R.string.Recherche_ProducerGIAA));
        listCheckBoxProducerId.add(new CheckBoxItem("LTA", R.string.Recherche_ProducerLAcom));
        listCheckBoxProducerId.add(new CheckBoxItem("LB", R.string.Recherche_ProducerLBB));
        listCheckBoxProducerId.add(new CheckBoxItem("AUT", R.string.Recherche_ProducerOthers));
    }

    private void search() {

        String queryText = searchText.getText().toString();
        queryText = queryText.replace(" ", "%20");

        request = "";
        new_request = true;
        page = 0;

        boolean itsANumber = false;
        if(listFilterAll.size() == 0) {
            try {
                int queryValue = Integer.parseInt(queryText);
                if ((String.valueOf(queryValue)).equals(queryText)) {
                    if (queryValue > 1002) {
                        itsANumber = true;
                    }
                }
            } catch (Exception e) {
                Log.d("QUERYY", "NaN");
            }
        }
        if(itsANumber){
            request = "NewSearch&values=" + "{\"queryText\":\"" + queryText + "\",\"queryType\":\"code\"}";
            getResultRequest(0);
            return;
        }
        // checking if search by number that number is input
        if (query_type.equals("code")) {
            try {
                int searchCode = Integer.parseInt(queryText);
                queryText = Integer.toString(searchCode);
            } catch (NumberFormatException e) {
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.Recherche_ErreurFormatNumero));
                return;
            }
        }

        if (query_type.equals("reader")) {
            request = "NewSearch&values=" + "{\"reader\":\"" + queryText + "\",\"count\":\"" + ITEM_PER_PAGE + "\"";
        } else if(!queryText.equals("")) {
            request = "NewSearch&values=" + "{\"queryText\":\"" + queryText + "\",\"queryType\":\"" + query_type + "\",\"count\":\"" + ITEM_PER_PAGE + "\"";
        } else{
            request = "NewSearch&values=" + "{\"count\":\"" + ITEM_PER_PAGE + "\"";
        }

        if (youth_literature_only) {
            request += ",\"jeunesse\":{\"filtrer\":\"filtrer\"}";
        }

        if (exclude_tts) {
            request += ",\"mediaType\":\"noCDS\"";
        }

        if (!query_producer.equals("ALL")) {
            request += ",\"producerCode\":\"" + query_producer + "\"";
        }

        if (!queryGenreList.get(0).equals("ALL")) {
            // genreQuery = ",\"genre\":[\"" + genreCode + "\"],\"sort\":\"author_fr%20desc\"";
            StringBuilder sTemp = new StringBuilder();
            for (String s : queryGenreList) {
                sTemp.append("\"").append(s).append("\",");
            }
            String query_genre = sTemp.substring(0, sTemp.length() -1);
            Log.d("Checkbox", query_genre);
            request += ",\"genre\":[" + query_genre + "]";
        }

        // end of request
        request += "}";

        Log.d("SearchQuery", "Query: " + request);

        getResultRequest(0);
    }

    private final WeakReference<SearchActivity> weakReferenceThis = new WeakReference<>(this);

    private final IncomingHandler handler = new IncomingHandler(weakReferenceThis);

    static class IncomingHandler extends Handler {

        private final WeakReference<SearchActivity> wRefBook;
        BookListAdapter bla;

        private IncomingHandler(WeakReference<SearchActivity> w) {
            wRefBook = w;
        }

        @Override
        public void handleMessage(Message msg) {
            if(msg.obj == null) { return; }
            Log.d("BRSS",  msg.obj.toString());
            if (msg.what == WCT) {
                String jsonString = (String) msg.obj;

                if (!JSonManager.getError(jsonString).equals("")) {
                    Toast.makeText(wRefBook.get().getApplicationContext(), "Erreur: " + JSonManager.getError(jsonString), Toast.LENGTH_SHORT).show();
                } else {

                    JSONObject jResult;
                    Iterator it;

                    jResult = JSonManager.getResultNode(jsonString, "NewSearch");
                    if (jResult == null) {
                        System.out.println("Erreur dans handler search: " + jsonString);
                        return;
                    }

                    Log.d("SearchResult", jResult.toJSONString());

                    wRefBook.get().resultCount = ((Long) jResult.remove("count")).intValue();
                    jResult.remove("facets");

                    if (wRefBook.get().resultCount > 0) {
                        wRefBook.get().bookListView.setVisibility(View.VISIBLE);
                        if (wRefBook.get().resultCount == 1) {
                            wRefBook.get().pageAmount.setText((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitreTrouve)));
                            wRefBook.get().pageAmount.setContentDescription((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitreTrouve)));

                        } else {
                            wRefBook.get().pageAmount.setText((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitresTrouves)));
                        }
                        Utils.sendTBMessage(wRefBook.get(), (wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitresTrouves)));
                    } else {
                        wRefBook.get().pageAmount.setText(wRefBook.get().getResources().getString(R.string.Recherche_PasDeResultats));
                        wRefBook.get().bookListView.setVisibility(View.INVISIBLE);
                    }
                    it = jResult.keySet().iterator();
                    Object[] resultArray = new Object[jResult.size()];

                    while (it.hasNext()) {
                        Object currentIndex = it.next();
                        // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                        int intIndex = Integer.parseInt(currentIndex.toString());
                        resultArray[intIndex] = jResult.get(currentIndex);
                    }
                    Log.d("BRSS", "newrequest: " + wRefBook.get().new_request);
                    if (wRefBook.get().new_request) {
                        wRefBook.get().resultList = new ListeFiches(resultArray);
                        bla = wRefBook.get().new BookListAdapter(wRefBook.get().resultList);
                        wRefBook.get().bookListView.setAdapter(bla);
                    } else {
                        wRefBook.get().resultList.addFromBookArray(resultArray);
                        bla.addFromBookArray(resultArray);
                        bla.notifyDataSetChanged();
                    }

                    Log.d("SearchResult", "count from res: " + wRefBook.get().resultList.getCount());

                    wRefBook.get().bookListView.setItemsCanFocus(false);

                    // done loading so that next page can load
                    wRefBook.get().isLoading = false;

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wRefBook.get().bookListView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                        }
                    }, 200);
                }
            }
        }
    }

    private void getResultRequest(int page) {

        WebcallTask wt = new WebcallTask(getApplicationContext(), handler);

        String query = request.substring(0, request.length() - 1) + ",\"page\":\"" + page + "\"}";

        Log.d("Pagination", query);

        wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bookListView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 500);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initListeners() {

        clearTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchText.setText("");
                search();
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
             }
        });

        filtersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                initiatePopUpWindow(v);
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!searchText.getText().toString().equals("")) {
                    search();
                } else {
                }
            }
        });

        playBookListener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                    return;
                }

                String bookName = (String) view.getTag(R.string.Tags_NoticeNr);

                mLastClickTime = SystemClock.elapsedRealtime();

                if (lastPlayedImage != null) {
                    lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                    lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                    playing = false;
                }

                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                //intent.putExtra("context", currentType);
                intent.putExtra("bookNr", bookName);
                startActivity(intent);
            }
        };

        dialog2ClickListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        Intent dlIntent = new Intent(getBaseContext(), DownloadsActivity.class);
                        dlIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(dlIntent);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        // on y retourne
                        break;
                }
            }
        };

        bookListView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() >= 2) {
                    mLastClickTime = SystemClock.elapsedRealtime();
                }

                return false;
            }
        });

        bookListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //nothing
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (request != null && !isLoading) {
                    Log.d("SearchResult", firstVisibleItem + "---" + visibleItemCount + "---" + totalItemCount);
                    // add more items to listview when there are two items remaining at the scroll end.
                    if (firstVisibleItem + visibleItemCount >= totalItemCount - VISIBLE_THRESHOLD) {

                        if (resultCount > (page + 1) * ITEM_PER_PAGE) {
                            isLoading = true;
                            new_request = false;
                            page = page + 1;
                            Log.d("SearchResult", "Next page: " + page);
                            getResultRequest(page);
                        }
                    }
                }
            }
        });
    }

    private static class ViewHolder {

        private TextView titleText;
        private TextView authorText;
        private TextView mediaText;
        //private TextView synthText;

        private ImageView playImage;
        private ImageView wishlistImage;
        private ImageView dlImage;
        private ImageView coverImage;
    }

    class BookListAdapter extends BaseAdapter {

        ListeFiches bookList;

        BookListAdapter(ListeFiches bookList) {

            this.bookList = bookList;
        }

        private void addFromBookArray(Object [] l) {
            this.bookList.addFromBookArray(l);
        }

        @Override
        public int getCount() {
            return bookList.getCount();
        }

        @Override
        public FicheLivre getItem(int i) {
            return bookList.getItem(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            LayoutInflater li = (LayoutInflater) SearchActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
            assert (li != null);
            SearchActivity.ViewHolder holder;

            if (convertView == null) {
                switch (viewType) {
                    case "text":
                        convertView = li.inflate(R.layout.book_row_text, viewGroup, false);
                        break;
                    case "cover":
                        convertView = li.inflate(R.layout.book_row_cover, viewGroup, false);
                        break;
                    case "text_cover":
                        convertView = li.inflate(R.layout.book_row_text_cover, viewGroup, false);
                        break;
                }
                assert (convertView != null);

                holder = new SearchActivity.ViewHolder();

                if (viewType.equals("text") || viewType.equals("text_cover")) {
                    holder.titleText = convertView.findViewById(R.id.titleText);
                    holder.authorText = convertView.findViewById(R.id.authorText);
                    holder.mediaText = convertView.findViewById(R.id.mediaText);
                }

                //holder.synthText = convertView.findViewById(R.id.synthText);

                holder.playImage = convertView.findViewById(R.id.book_action_1);
                holder.wishlistImage = convertView.findViewById(R.id.book_action_2);
                holder.dlImage = convertView.findViewById(R.id.book_action_3);

                if (!viewType.equals("text")) {
                    holder.coverImage = convertView.findViewById(R.id.coverImage);
                }

                convertView.setTag(R.string.Tags_Holder, holder);
            } else {
                holder = (SearchActivity.ViewHolder) convertView.getTag(R.string.Tags_Holder);
                if (!viewType.equals("text")) {
                    holder.coverImage.setImageBitmap(null);
                }
            }

            final FicheLivre currentBook = bookList.getItem(i);
            Log.d("BOOLK", currentBook.getTitle() + " getview");
            final ImageView wlImage = holder.wishlistImage;
            final ImageView plImage = holder.playImage;
            holder.dlImage.setContentDescription(getResources().getString(R.string.Catalogue_Telecharger) + " " + currentBook.getTitle());
            holder.playImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait) + " " + getResources().getString(R.string.Global_De_livre) + " " + currentBook.getTitle());
            holder.dlImage.setEnabled(true);
            holder.dlImage.setAlpha(1f);

            View.OnClickListener wishlistListener = new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    String request;
                    if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                        request = "DeleteWish&bookNr=" + currentBook.getNoticeNr();
                        wlImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                        wlImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu));
                        mBib.removeWishFromList(currentBook.getNoticeNr());
                        Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuSupprime));
                    } else {
                        request = "AddWish&bookNr=" + currentBook.getNoticeNr();
                        wlImage.setImageResource(R.drawable.ic_star_white_48dp);
                        wlImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu));
                        mBib.addWishToList(currentBook);
                        Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuAjoute));
                    }
                    WebcallTask wt = new WebcallTask(getApplicationContext());
                    wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
                }
            };

            View.OnClickListener extraitListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    if (currentBook.getMp3Url() != null) {
                        String mp3URL = currentBook.getMp3Url().toString();

                        if (playing) {
                            mediaPlayer.stop();
                            lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                            lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                            playing = false;
                            if (plImage != lastPlayedImage) {
                                plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                                plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                                lastPlayedImage = plImage;
                                playExtrait(mp3URL);
                                playing = true;
                            }

                        } else {
                            lastPlayedImage = plImage;
                            playExtrait(mp3URL);

                            plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                            plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                            playing = true;
                        }
                    }
                }
            };

            View.OnClickListener dlListener = new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    mLastClickedView = view;
                    final String bookName = (String) view.getTag(R.string.Tags_NoticeNr);
                    final FicheLivre currentBook;
                    if (wishList != null) {
                        if (wishList.getItemByNumber(bookName) != null) {
                            currentBook = wishList.getItemByNumber(bookName);
                        } else {
                            currentBook = resultList.getItemByNumber(bookName);
                        }
                    } else {
                        currentBook = resultList.getItemByNumber(bookName);
                    }

                    mBib.addFiche(currentBook);
                    if (mBib.bookInLibrary(bookName)) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_LivreDejaPresent));
                        return;
                    }

                    long freeSpace = mBib.getFreeSpace();
                    long bookSize = currentBook.getZipSize();

                    if (freeSpace < bookSize * 2) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_PasAssezEspace));
                        return;
                    }

                    ConnectivityManager cm =
                            (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = null;

                    if (cm != null) {
                        activeNetwork = cm.getActiveNetworkInfo();
                    }

                    boolean isWiFi = false;
                    if (activeNetwork != null) {
                        isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                    }

                    final Intent dlIntent = new Intent(getApplicationContext(), DownloadService.class);
                    dlIntent.putExtra("zipUrl", currentBook.getZipUrl());
                    dlIntent.putExtra("noticeNr", currentBook.getNoticeNr());
                    dlIntent.putExtra("noticeNr", currentBook.getNoticeNr());
                    dlIntent.putExtra("description", currentBook.getTitle());

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    startDl(dlIntent, currentBook);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //Log.d("dialogClickListener", "Not downloading");
                                    break;
                            }
                        }
                    };

                    if (!isWiFi) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
                        // AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setMessage(getResources().getString(R.string.Catalogue_AvertissementReseau)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialogClickListener)
                                .setNegativeButton(getResources().getString(R.string.Global_Annuler), dialogClickListener);

                        AlertDialog alert = builder.create();
                        Utils.prepareAndShowAlert(getApplicationContext(), alert);

                    } else {
                        startDl(dlIntent, currentBook);
                    }
                }
            };

            if (viewType.equals("text") || viewType.equals("text_cover")) {
                holder.titleText.setText(currentBook.getTitle());
                holder.authorText.setText(currentBook.getAuthor());
                holder.mediaText.setText(Utils.formatDuration(currentBook.getMedia(), getApplicationContext()));
                holder.mediaText.setContentDescription(getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext()));
            }

            if (!viewType.equals("text")) {
                memoryCache.loadBitmapCoverImage(holder.coverImage, currentBook.getCover(), currentBook.getNoticeNr());
            }

            /*if (currentBook.getReader().equals("Manon")) {
                holder.synthText.setText(getResources().getString(R.string.Global_Voix_SyntheseCourt));
            } else {
                holder.synthText.setText("");
            }*/

            holder.playImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());
            holder.dlImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            if (checkBookActivity(currentBook.getNoticeNr())) {
                Log.d("boolk", "diabling: " + currentBook.getTitle());
                holder.dlImage.setEnabled(false);
                holder.dlImage.setAlpha(0.3f);
            }

            convertView.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            holder.playImage.setOnClickListener(extraitListener);
            holder.dlImage.setOnClickListener(dlListener);
            holder.wishlistImage.setOnClickListener(wishlistListener);

            String description = currentBook.getTitle() + " " + getResources().getString(R.string.Global_De) + " " + currentBook.getAuthor() + ", " + getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext());

            if (currentBook.getReader().equals("Manon") || currentBook.getReader().equals("Alice")) {
                description += ", " + getResources().getString(R.string.Global_Voix_Synthese);
            }

            convertView.setOnClickListener(playBookListener);
            convertView.setContentDescription(description);

            if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu) + " " + currentBook.getTitle());
            } else {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu) + " " + currentBook.getTitle());
            }

            return convertView;
        }
    }

    private void playExtrait(String url) {
        if(mediaPlayer == null){
            mediaPlayer= new MediaPlayer();
        }else {
            mediaPlayer.reset();
        }

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        } catch (IOException e) {
            Log.e("AudioService", "Exception IO");
            e.printStackTrace();
        }
    }

    public void onCompletion(MediaPlayer _mediaPlayer) {

        lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
        playing = false;
    }

    private void startDl(Intent intent, FicheLivre book) {

        mLastClickedView.setEnabled(false);
        mLastClickedView.setAlpha(0.3f);

        Calendar cal = Calendar.getInstance();
        book.setDownloadDate(cal.getTimeInMillis());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else{
            startService(intent);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
        builder.setMessage(getResources().getString(R.string.Catalogue_TelechargementDe) + " " + book.getTitle() + ".\n" + getResources().getString(R.string.Catalogue_AllerTelechargements)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialog2ClickListener)
                .setNegativeButton(getResources().getString(R.string.Global_Non), dialog2ClickListener);

        AlertDialog alert = builder.create();

        Utils.prepareAndShowAlert(getApplicationContext(), alert);
    }

    private boolean checkBookActivity(String noticeNr) {

        boolean downloading = false;
        boolean alreadyThere = false;
        for (int i = 0; i < DownloadsActivity.downloads.size(); i++) {
            if (DownloadsActivity.downloads.get(i).noticeNr.equals(noticeNr)) {
                Log.d("BOOLK", noticeNr + " déjà en cours de téléchargement");
                downloading = true;
            }
        }
        if (mBib.bookInLibraryByNoticeNr(noticeNr)) {
            Log.d("BOOLK", noticeNr + " dans la biblio");
            alreadyThere = true;
        }
        return downloading || alreadyThere;
    }

    private void initiatePopUpWindow(final View v) {

        LayoutInflater inflater = (LayoutInflater) SearchActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (inflater != null) {
            //Inflate the view from a predefined XML layout
            final View layout = inflater.inflate(R.layout.popup_search_menu, (ViewGroup) findViewById(R.id.popupMenuSearch));

            //layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.popup_show));

            // Full screen popupwindow
            final PopupWindow popupWindow = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, true);
            // display the popup in the center
            popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

            // Attribute of window
            final ImageButton backButton = layout.findViewById(R.id.previousBarButton);
            final Button applyButton = layout.findViewById(R.id.applyButton);
            TextView searchType = layout.findViewById(R.id.searchType);
            ImageButton searchTypeButton = layout.findViewById(R.id.searchTypeButton);

            TextView searchGenre = layout.findViewById(R.id.searchGenre);
            ImageButton searchGenreButton = layout.findViewById(R.id.searchGenreButton);

            TextView searchProducer = layout.findViewById(R.id.searchProducer);
            ImageButton searchProducerButton = layout.findViewById(R.id.searchProducerButton);

            TextView searchOther = layout.findViewById(R.id.searchOther);
            ImageButton searchOtherButton = layout.findViewById(R.id.searchOtherButton);

            final RelativeLayout searchGeneralMenu = layout.findViewById(R.id.searchGeneralMenu);
            final RelativeLayout searchTypeMenu= layout.findViewById(R.id.searchTypeMenu);
            final RelativeLayout searchGenreMenu = layout.findViewById(R.id.searchGenreMenu);
            final RelativeLayout searchProducerMenu = layout.findViewById(R.id.searchProducerMenu);
            final RelativeLayout searchOtherMenu = layout.findViewById(R.id.searchOtherMenu);
            final Switch youthSwitch = layout.findViewById(R.id.more_youth);
            final Switch excludeTTSSwitch = layout.findViewById(R.id.more_exclude_tts);

            listCheckBoxTypeAdapter = new SearchOptionsAdapter(listCheckBoxTypeId, "type");
            listCheckBoxGenreAdapter = new SearchOptionsAdapter(listCheckBoxGenreId, "genre");
            listCheckBoxProducerAdapter = new SearchOptionsAdapter(listCheckBoxProducerId, "producer");

            listCheckBoxType = layout.findViewById(R.id.listType);
            listCheckBoxType.setAdapter(listCheckBoxTypeAdapter);

            listCheckBoxGenre = layout.findViewById(R.id.listGenre);
            listCheckBoxGenre.setAdapter(listCheckBoxGenreAdapter);

            listCheckBoxProducer = layout.findViewById(R.id.listProducer);
            listCheckBoxProducer.setAdapter(listCheckBoxProducerAdapter);

            final Handler handler = new Handler();
            final Runnable run = new Runnable() {
                @Override
                public void run() {
                    applyButton.setVisibility(View.INVISIBLE);
                    popupWindow.dismiss();
                }
            };

            // Listeners
            final View.OnClickListener backButtonListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.popup_hide));
                    handler.postDelayed(run, 300);
                    search();
                    Log.d("SISMISS", "backbuttonlistener");
                }
            };


            View.OnClickListener typeMenuListener = new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    applyButton.setVisibility(View.VISIBLE);
                    searchGeneralMenu.setVisibility(View.INVISIBLE);
                    searchTypeMenu.setVisibility(View.VISIBLE);
                    backButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchGeneralMenu.setVisibility(View.VISIBLE);
                            searchTypeMenu.setVisibility(View.INVISIBLE);
                            applyButton.setVisibility(View.INVISIBLE);
                            backButton.setOnClickListener(backButtonListener);
                        }
                    });
                }
            };

            View.OnClickListener genreMenuListener = new View.OnClickListener() {

                @Override
                public void onClick(final View view) {

                    applyButton.setVisibility(View.VISIBLE);
                    searchGeneralMenu.setVisibility(View.INVISIBLE);
                    searchGenreMenu.setVisibility(View.VISIBLE);
                    backButton.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            searchGeneralMenu.setVisibility(View.VISIBLE);
                            searchGenreMenu.setVisibility(View.INVISIBLE);
                            applyButton.setVisibility(View.INVISIBLE);
                            backButton.setOnClickListener(backButtonListener);
                        }
                    });
                }
            };

            View.OnClickListener producerMenuListener = new View.OnClickListener() {

                @Override
                public void onClick(final View view) {

                    applyButton.setVisibility(View.VISIBLE);
                    searchGeneralMenu.setVisibility(View.INVISIBLE);
                    searchProducerMenu.setVisibility(View.VISIBLE);
                    backButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            applyButton.setVisibility(View.INVISIBLE);
                            searchGeneralMenu.setVisibility(View.VISIBLE);
                            searchProducerMenu.setVisibility(View.INVISIBLE);
                            backButton.setOnClickListener(backButtonListener);
                        }
                    });
                }
            };

            View.OnClickListener otherMenuListener = new View.OnClickListener() {

                @Override
                public void onClick(final View view) {

                    applyButton.setVisibility(View.VISIBLE);
                    searchGeneralMenu.setVisibility(View.INVISIBLE);
                    searchOtherMenu.setVisibility(View.VISIBLE);
                    backButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            applyButton.setVisibility(View.INVISIBLE);
                            searchGeneralMenu.setVisibility(View.VISIBLE);
                            searchOtherMenu.setVisibility(View.INVISIBLE);
                            backButton.setOnClickListener(backButtonListener);
                        }
                    });
                }
            };

            final View.OnClickListener applyButtonListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("SISMISS", "applyButtonListener");
                    layout.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.popup_hide));
                    handler.postDelayed(run, 300);
                    search();
                }
            };

            youthSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    youth_literature_only = youthSwitch.isChecked();
                    Log.d("Checkbox", "youth: " + youth_literature_only);
                    updateFilter("youth", R.string.Recherche_JeunesseSeulement);
                }
            });

            excludeTTSSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exclude_tts = excludeTTSSwitch.isChecked();
                    Log.d("Checkbox", "tts: " + exclude_tts);
                    updateFilter("exclude_tts", R.string.Recherche_NoTTS);
                }
            });

            // setting the switches
            if (youth_literature_only) {
                youthSwitch.setChecked(true);
            } else {
                youthSwitch.setChecked(false);
            }

            if (exclude_tts) {
                excludeTTSSwitch.setChecked(true);
            } else {
                excludeTTSSwitch.setChecked(false);
            }

            searchType.setOnClickListener(typeMenuListener);
            searchTypeButton.setOnClickListener(typeMenuListener);
            searchGenre.setOnClickListener(genreMenuListener);
            searchGenreButton.setOnClickListener(genreMenuListener);
            searchProducer.setOnClickListener(producerMenuListener);
            searchProducerButton.setOnClickListener(producerMenuListener);
            searchOther.setOnClickListener(otherMenuListener);
            searchOtherButton.setOnClickListener(otherMenuListener);
            backButton.setOnClickListener(backButtonListener);
            Log.d("SISMISS", "applylistener");
            applyButton.setOnClickListener(applyButtonListener);
            applyButton.setVisibility(View.INVISIBLE);
        }
    }

    private static class ViewHolderCheckbox {

        CheckBox checkBox;
    }

    private static class ViewHolderFilter {

        TextView filterName1;
        ImageButton filterCancel1;
        RelativeLayout layoutFilter2;
        TextView filterName2;
        ImageButton filterCancel2;
    }

    private class CheckBoxItem {

        private String id;
        private int textId;

        private CheckBoxItem(String id, int textId) {
            this.id = id;
            this.textId = textId;
        }

        public String getId() {
            return id;
        }

        private int getTextId() {
            return textId;
        }
    }

    class SearchOptionsAdapter extends BaseAdapter {

        List<CheckBoxItem> listCheckBox;
        String typeOfCheckBox;

        SearchOptionsAdapter(List<CheckBoxItem> listCheckBox, String type) {

            this.listCheckBox = listCheckBox;
            this.typeOfCheckBox = type;
        }

        @Override
        public int getCount() {
            return (listCheckBox != null) ? listCheckBox.size() : 0;
        }

        @Override
        public CheckBoxItem getItem(int i) {
            return listCheckBox.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View convertView, final ViewGroup viewGroup) {

            final SearchActivity.ViewHolderCheckbox holder;

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) SearchActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
                assert li != null;
                convertView = li.inflate(R.layout.search_menu_checkbox, viewGroup, false);
                holder = new SearchActivity.ViewHolderCheckbox();
                convertView.setTag(holder);
            } else {
                holder = (SearchActivity.ViewHolderCheckbox) convertView.getTag();
            }

            final CheckBoxItem checkBox = getItem(i);
            holder.checkBox = convertView.findViewById(R.id.idCheckbox);
            holder.checkBox.setText(checkBox.getTextId());
            holder.checkBox.setTag(checkBox.getId());

            // setting the checkbox true or false
            if ((typeOfCheckBox.equals("type") && query_type.equals(checkBox.getId()))) {
                holder.checkBox.setChecked(true);
            } else if (typeOfCheckBox.equals("producer") && query_producer.equals(checkBox.getId())) {
                holder.checkBox.setChecked(true);
            } else if (typeOfCheckBox.equals("genre") && queryGenreList.contains(checkBox.getId())) {
                holder.checkBox.setChecked(true);
            } else {
                holder.checkBox.setChecked(false);
            }

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSearchType(checkBox, holder.checkBox);
                }
            });

            return convertView;
        }

        private void setSearchType(CheckBoxItem cItem, CheckBox cB) {

            switch (typeOfCheckBox) {
                case "type":
                    query_type = cItem.getId();
                    updateFilter("type", cItem.getTextId());
                    break;
                case "producer":
                    query_producer = cItem.getId();
                    updateFilter("producer", cItem.getTextId());
                    break;
                default:
                    if (cItem.getId().equals("ALL")) {
                        queryGenreList.clear();
                        queryGenreList.add("ALL");
                    } else {
                        if (cB.isChecked()) {
                            if (queryGenreList.get(0).equals("ALL")) {
                                queryGenreList.remove("ALL");
                            }
                            queryGenreList.add(cItem.getId());
                        } else {
                            queryGenreList.remove(cItem.getId());
                            if (queryGenreList.size() == 0) {
                                queryGenreList.add("ALL");
                            }
                        }
                    }
                    updateFilter("genre", cItem.getTextId());
                    break;
            }

            notifyDataSetChanged();
            Log.d("Checkbox", "t: " + query_type + ", g: " + queryGenreList.toString() + ", p: " + query_producer);
        }
    }

    class FilterViewAdapter extends BaseAdapter {

        private ArrayList<Map.Entry<String, Integer>> listFilters;

        FilterViewAdapter(Map<String, Integer> listFilters) {
            this.listFilters = new ArrayList<>();
            this.listFilters.addAll(listFilters.entrySet());
        }

        @Override
        public int getCount() {
            return (listFilters != null) ? (int) Math.ceil(listFilters.size() / 2.0) : 0;
        }

        @Override
        public Map.Entry<String, Integer> getItem(int i) {

            if (i < listFilters.size()) {
                return listFilters.get(i);
            } else {
                return null;
            }
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View convertView, final ViewGroup viewGroup) {

            final SearchActivity.ViewHolderFilter holder;

            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) SearchActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
                assert li != null;
                convertView = li.inflate(R.layout.filter_row, viewGroup, false);
                holder = new SearchActivity.ViewHolderFilter();
                convertView.setTag(holder);
            } else {
                holder = (SearchActivity.ViewHolderFilter) convertView.getTag();
            }

            // first filter for this row
            final Map.Entry<String, Integer> filter1 = getItem(2 * i);
            if (filter1 != null) {
                convertView.setVisibility(View.VISIBLE);
                holder.filterName1 = convertView.findViewById(R.id.filterType);
                holder.filterName1.setText(setTextFilter(filter1));
                holder.filterName1.setContentDescription(setTextFilter(filter1));
                holder.filterName1.setMaxWidth((int)((widthScreen / 3.0)));
                holder.filterCancel1 = convertView.findViewById(R.id.clearFilter);
                holder.filterCancel1.setContentDescription("Supprimer filtre: "+setTextFilter(filter1));
                holder.filterCancel1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listFilters.remove(2 * i);
                        listFilterAll.remove(filter1.getKey());
                        updateCheckBoxes(filter1);
                        notifyDataSetChanged();
                        search();
                    }
                });

                // second filter for this row
                final Map.Entry<String, Integer> filter2 = getItem(2 * i + 1);
                holder.layoutFilter2 = convertView.findViewById(R.id.layoutFilter2);
                if (filter2 != null) {
                    holder.layoutFilter2.setVisibility(View.VISIBLE);
                    holder.filterName2 = convertView.findViewById(R.id.filterType2);
                    holder.filterName2.setText(setTextFilter(filter2));
                    holder.filterName2.setContentDescription(setTextFilter(filter2));
                    holder.filterName2.setMaxWidth((int)((widthScreen / 3.0)));
                    holder.filterCancel2 = convertView.findViewById(R.id.clearFilter2);
                    holder.filterCancel2.setContentDescription("Supprimer filtre: "+setTextFilter(filter2));
                    holder.filterCancel2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listFilters.remove(2 * i + 1);
                            listFilterAll.remove(filter2.getKey());
                            updateCheckBoxes(filter2);
                            notifyDataSetChanged();
                            search();
                        }
                    });
                } else {
                    Log.d("Filters", "filter2 is null");
                    holder.layoutFilter2.setVisibility(View.INVISIBLE);
                }
            } else {
                convertView.setVisibility(View.INVISIBLE);
            }

            return convertView;
        }

        private String setTextFilter(Map.Entry<String, Integer> f) {

            String result;

            switch (f.getKey()) {
                case "type":
                    result = getApplicationContext().getResources().getString(R.string.Recherche_By);
                    break;
                case "youth":
                    return "Jeunesse";
                case "exclude_tts":
                    return "Voix humaine";
                default:
                    return getApplicationContext().getResources().getString(f.getValue());
            }

            result += ": " + getApplicationContext().getResources().getString(f.getValue());

            return result;
        }

        private void updateMap(Map<String, Integer> m) {

            listFilters = new ArrayList<>();
            listFilters.addAll(m.entrySet());
        }

        private void updateCheckBoxes(Map.Entry<String, Integer> filter) {

            switch (filter.getKey()) {
                case "type":
                    query_type = "";
                    break;
                case "producer":
                    query_producer = "ALL";
                    break;
                case "exclude_tts":
                    exclude_tts = !exclude_tts;
                    break;
                case "youth":
                    youth_literature_only = !youth_literature_only;
                    break;
                default:
                    for(CheckBoxItem c : listCheckBoxGenreId) {
                        if (c.getTextId() == filter.getValue()) {
                            queryGenreList.remove(c.getId());
                        }
                    }

                    if (queryGenreList.size() == 0) {
                        queryGenreList.add("ALL");
                    }
                    break;
            }
        }
    }

    private void updateFilter(String type, int textId) {

        switch (type) {
            case "type":
                if (textId == R.string.Recherche_TypeAll) {
                    listFilterAll.remove(type);
                } else {
                    listFilterAll.put(type, textId);
                }
                break;
            case "producer":
                if (textId == R.string.Recherche_ProducerAny) {
                    listFilterAll.remove(type);
                } else {
                    listFilterAll.put(type, textId);
                }
                break;
            case "youth":
                if (youth_literature_only) {
                    listFilterAll.put(type, textId);
                } else {
                    listFilterAll.remove(type);
                }
                break;
            case "exclude_tts":
                if (exclude_tts) {
                    listFilterAll.put(type, textId);
                } else {
                    listFilterAll.remove(type);
                }
                break;
            default:
                if (textId == R.string.Recherche_GenreAll) {
                    //listFilterAll.entrySet().removeIf(entry -> condition); JAVA 1.8 or above
                    Iterator<Map.Entry<String, Integer>> entryIterator = listFilterAll.entrySet().iterator();
                    while(entryIterator.hasNext()) {
                        Map.Entry<String, Integer> entry = entryIterator.next();
                        if (entry.getKey().contains("genre")) {
                            entryIterator.remove();
                        }
                    }
                } else {
                    if (listFilterAll.remove("genre_" + textId) == null) {
                        listFilterAll.put("genre_" + textId, textId);
                    }
                }

                break;
        }
        Log.d("Filters", listFilterAll.toString());
        listFilterAdapterAll.updateMap(listFilterAll);
        listFilterAdapterAll.notifyDataSetChanged();
    }
}
