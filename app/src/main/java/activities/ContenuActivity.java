package activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;

import bibliothequesonore.ch.livresbsrandroid.R;
import utils.ContenuPager;
import utils.ResizableTabLayout;

public class ContenuActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private ResizableTabLayout tabLayout;
    private ViewPager viewPager;
    private static int lastSelectedTab = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_contenu);

        tabLayout = (ResizableTabLayout) findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.Lecteur_TableDesMatieres)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.Lecteur_Signets)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorHeight(20);
        viewPager = (ViewPager) findViewById(R.id.pager);

        ContenuPager adapter = new ContenuPager(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener(this);

        viewPager.addOnPageChangeListener (new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

                viewPager.setCurrentItem(position);
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                View mainTab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0);
                mainTab.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 500);


	}

	@Override
	public void onBackPressed() {

		Intent returnIntent = new Intent();
		setResult(RESULT_CANCELED, returnIntent);
		finish();
	}
	
    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            // do nothing
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        lastSelectedTab = tab.getPosition();
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    protected void onResume() {

       tabLayout.getTabAt(lastSelectedTab).select();
       super.onResume();
    }
}
