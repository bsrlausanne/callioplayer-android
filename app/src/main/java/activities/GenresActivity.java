package activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import bibliothequesonore.ch.livresbsrandroid.R;
import tasks.WebcallTask;
import utils.Bibliotheque;
import utils.JSonManager;
import utils.ListeFiches;
import utils.Utils;


public class GenresActivity extends AppCompatActivity {

    LinearLayout ll;
    LayoutParams lp;
    GenresActivity theActivity;
    Bibliotheque mBib;
    TextView listTitleView;
    ImageButton previousActivityButton;

    static class Genre {
        private String mCode;
        String mText;

        Genre(String code, String text){
            mCode = code;
            mText = text;
        }
    }

    private WeakReference<GenresActivity> weakReferenceThis = new WeakReference<>(this);
    private IncomingHandler handler = new IncomingHandler(weakReferenceThis);
    
    static class IncomingHandler extends Handler {

        private WeakReference<GenresActivity> wGenres;

        private IncomingHandler(WeakReference<GenresActivity> ga) {
            wGenres = ga;
        }

        @Override
        public void handleMessage(Message msg) {
            //Log.d("Genres", "handler!");

            String genresJson = (String) msg.obj;
            //Log.d("Genres", genresJson);

            JSONArray genreJSONArray = JSonManager.getResultArray(genresJson, "ListOfGenres");
            if (genreJSONArray == null) {
                return;
            }

            Object[] genresArray = genreJSONArray.toArray();
            ArrayList<Genre> genres = new ArrayList<>();

            for (Object genre : genresArray) {

                JSONObject currentGenre = (JSONObject) genre;

                String code = ((String) currentGenre.get("code"));
                Log.d("GENRES", code);
                String text = ((String) currentGenre.get("text"));

                genres.add(new Genre(code, text));
            }

            Collections.sort(genres, new Comparator<Genre>() {
                @Override
                public int compare(Genre g1, Genre g2)
                {
                    return  g1.mText.compareTo(g2.mText);
                }
            });

            Button generalButton = (Button) wGenres.get().getLayoutInflater().inflate(R.layout.genre_item, null);
            generalButton.setText(wGenres.get().getResources().getString(R.string.BSR_ToutesLesNouveautes));
            generalButton.setTag("all");
            generalButton.setOnClickListener(wGenres.get().genreListener);

            wGenres.get().ll.addView(generalButton, wGenres.get().lp);

            for (Genre genre : genres) {

                Button genreButton = (Button) wGenres.get().getLayoutInflater().inflate(R.layout.genre_item, null);
                genreButton.setText(genre.mText);
                genreButton.setTag(genre.mCode);
                genreButton.setOnClickListener(wGenres.get().genreListener);
                wGenres.get().ll.addView(genreButton, wGenres.get().lp);

            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    wGenres.get().listTitleView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                }
            }, 500);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_genres);

        previousActivityButton = findViewById(R.id.previousBarButton);

        previousActivityButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                GenresActivity.this.onBackPressed();
            }
        });

        mBib = Bibliotheque.getBibliotheque(getApplicationContext());

        ll = (LinearLayout) findViewById(R.id.genresLayout);
        lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        lp.setMargins(0,0,0,2);

        listTitleView = (TextView) findViewById(R.id.genreTitle);

        theActivity = this;
        String request = "ListOfGenres";

        Utils.isInternetAvailable(getApplicationContext(), false);
            /*Toast.makeText(getApplicationContext(), getResources().getString(R.string.Connexion_PasConnecte), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return;*/

        WebcallTask wt = new WebcallTask(getApplicationContext(), handler);
        wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);

    }

    View.OnClickListener genreListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            String code = (String) view.getTag();

            Intent intent;
            intent = new Intent(getApplicationContext(), ScrollBookListActivity.class);
            intent.putExtra("genre", ((Button) view).getText().toString());
            intent.putExtra("code", code);
            intent.putExtra("listType", ListeFiches.CATEGS_TYPE);

            startActivity(intent);

        }

    };

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            // do nothing
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {

        super.onResume();
        BookListActivity.currentPage = 1;
        mBib.login(null);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listTitleView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 500);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}