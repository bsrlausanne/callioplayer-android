package activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

import bibliothequesonore.ch.livresbsrandroid.R;
import services.DownloadService;
import tasks.WebcallTask;
import utils.Bibliotheque;
import utils.FicheLivre;
import utils.JSonManager;
import utils.ListeFiches;
import utils.MemoryCache;
import utils.Utils;


public class BookListActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    public final static int WCT = 1;

    private String viewType = "cover";
    // text = book_row_text, cover = book_row_cover, text_cover = book_row_text_cover

    ListView bookListView;
    TextView listTitleView;
    TextView pageAmount;
    LinearLayout paginLayout;
    ImageView imageView12;
    ImageButton previousActivityButton;
    ImageButton homeButton;
    ImageButton previousButton, nextButton;

    public static ListeFiches wishList = null;
    public static ListeFiches votationsList = null;
    ListeFiches resultList = null;
    int resultCount = 0;
    static int currentPage = 1;
    String currentTitle = "";

    static int currentType = -1;
    private static int listType = -1;
    private Intent intent;

    private static boolean playing = false;
    ImageView lastPlayedImage = null;

    private long mLastClickTime = 0;
    private Bibliotheque mBib;

    private String mResultTitle = null;
    static MediaPlayer mediaPlayer;

    private View mLastClickedView;

    private MemoryCache memoryCache;
    private static int paginationHeight;

    OnClickListener playBookListener = new OnClickListener() {
        @Override
        public void onClick(View view) {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                return;
            }

            String bookName = (String) view.getTag(R.string.Tags_NoticeNr);

            mLastClickTime = SystemClock.elapsedRealtime();

            if (lastPlayedImage != null) {
                lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                playing = false;
            }

            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            //intent.putExtra("context", currentType);
            intent.putExtra("bookNr", bookName);
            intent.putExtra("listType", listType);
            startActivity(intent);
        }

    };

    private WeakReference<BookListActivity> weakReferenceThis = new WeakReference<>(this);
    private IncomingHandler handler = new IncomingHandler(weakReferenceThis);

    static class IncomingHandler extends Handler {

        private WeakReference<BookListActivity> wRefBook;
        BookListAdapter bla;
        private IncomingHandler(WeakReference<BookListActivity> w) {
            wRefBook = w;
        }

        @Override
        public void handleMessage(Message msg) {

            Log.d("booolk", "handler");
            if (msg.what == WCT) {
                String jsonString = (String) msg.obj;
                // Toast.makeText(getApplicationContext(), "handler: "+jsonString, Toast.LENGTH_SHORT).show();
                if (!JSonManager.getError(jsonString).equals("")) {
                    Toast.makeText(wRefBook.get().getApplicationContext(), "Erreur: " + JSonManager.getError(jsonString), Toast.LENGTH_SHORT).show();
                } else {
                    //Log.d("BOOLK", JSonManager.getResultNode(jsonString, "NewSearch").toString());

                    JSONObject jResult;
                    JSONArray jBookArray;
                    Object[] bookArray;
                    Iterator it;

                    switch (currentType) {
                        case ListeFiches.SEARCH_TYPE:

                            // Toast.makeText(getApplicationContext(), "Name: " + JSonManager.getFunction(jsonString), Toast.LENGTH_SHORT).show();
                            jResult = JSonManager.getResultNode(jsonString, "NewSearch");
                            if (jResult == null) {
                                System.out.println("Erreur dans handler search: " + jsonString);
                                return;
                            }

                            Log.d("SearchResult", jResult.toJSONString());

                            wRefBook.get().resultCount = ((Long) jResult.remove("count")).intValue();
                            jResult.remove("facets");
                            if (wRefBook.get().mResultTitle != null) {
                                wRefBook.get().currentTitle = wRefBook.get().mResultTitle;
                            } else {
                                if (wRefBook.get().resultCount == 0) {
                                    wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Recherche_PasDeResultats));
                                } else if (wRefBook.get().resultCount == 1) {
                                    wRefBook.get().currentTitle = "1 " + wRefBook.get().getResources().getString(R.string.Catalogue_TitreTrouve);
                                } else {
                                    wRefBook.get().currentTitle = wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitresTrouves);
                                }
                            }

                            it = jResult.keySet().iterator();
                            Object[] resultArray = new Object[jResult.size()];

                            while (it.hasNext()) {
                                Object currentIndex = it.next();
                                // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                                int intIndex = Integer.parseInt(currentIndex.toString());
                                resultArray[intIndex] = jResult.get(currentIndex);
                            }

                            wRefBook.get().resultList = new ListeFiches(resultArray);
                            bla = wRefBook.get(). new BookListAdapter(wRefBook.get().resultList);
                            wRefBook.get().bookListView.setAdapter(bla);

                            if (wRefBook.get().resultCount > 0) {
                                wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));
                                wRefBook.get().pageAmount.setContentDescription((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur  " + wRefBook.get().lastPage()));
                                wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle));
                                //wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle + "\n" + wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " " + wRefBook.get().getResources().getString(R.string.Catalogue_Sur) + " " + wRefBook.get().lastPage()));
                            } else {
                                wRefBook.get().pageAmount.setText((""));
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Recherche_PasDeResultats));
                            }
                            //loadPictures(resultList);
                            break;

                        case ListeFiches.WISHES_TYPE:
                            jBookArray = JSonManager.getResultArray(jsonString, "GetWishes");
                            if (jBookArray == null || jBookArray.size() == 0) {
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.ListeDeVoeux_ListeVide));
                            } else {
                                Object[] wishesArray = jBookArray.toArray();
                                wRefBook.get().resultCount = wishesArray.length;
                                int lastIndex = currentPage * 10;
                                if (currentPage == wRefBook.get().lastPage()) {
                                    lastIndex = (currentPage - 1) * 10 + wRefBook.get().resultCount % 10;
                                    if( lastIndex == 0 ){lastIndex=10;}
                                }
                                Object[] rangeFiche = Arrays.copyOfRange(wishesArray, (currentPage - 1) * 10, lastIndex);
                                wishList = new ListeFiches(rangeFiche);
                                bla = wRefBook.get(). new BookListAdapter(wishList);
                                wRefBook.get().bookListView.setAdapter(bla);
                                wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " / " + wRefBook.get().lastPage()));
                                wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle));

                                //wRefBook.get().listTitleView.setText((wRefBook.get().getResources().getString(R.string.ListeDeVoeux_ListeDeVoeux) + "\n" + wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " " + wRefBook.get().getResources().getString(R.string.Catalogue_Sur) + " " + wRefBook.get().lastPage()));
                            }
                            // loadPictures(wishList);
                            break;
                        case ListeFiches.VOTATIONS_TYPE:
                            jBookArray = JSonManager.getResultArray(jsonString, "NextVotations");
                            if (jBookArray == null || jBookArray.size() == 0) {
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.ListeDeVoeux_ListeVide));
                            } else {
                                Object[] wishesArray = jBookArray.toArray();
                                wRefBook.get().resultCount = wishesArray.length;
                                int lastIndex = currentPage * 10;
                                if (currentPage == wRefBook.get().lastPage()) {
                                    lastIndex = (currentPage - 1) * 10 + wRefBook.get().resultCount % 10;
                                    if( lastIndex == 0 ){lastIndex=10;}
                                }
                                Object[] rangeFiche = Arrays.copyOfRange(wishesArray, (currentPage - 1) * 10, lastIndex);
                                votationsList = new ListeFiches(rangeFiche);
                                bla = wRefBook.get(). new BookListAdapter(votationsList);
                                wRefBook.get().bookListView.setAdapter(bla);
                                wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " / " + wRefBook.get().lastPage()));
                                wRefBook.get().listTitleView.setText("Prochaines votations");
                                //wRefBook.get().listTitleView.setText((wRefBook.get().getResources().getString(R.string.ListeDeVoeux_ListeDeVoeux) + "\n" + wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " " + wRefBook.get().getResources().getString(R.string.Catalogue_Sur) + " " + wRefBook.get().lastPage()));
                            }
                            // loadPictures(wishList);
                            break;
                        case ListeFiches.NEWBOOKS_TYPE:
                            jResult = JSonManager.getResultNode(jsonString, "NewSearch");
                            if (jResult == null) {
                                System.out.println("Erreur dans handler newbooks: " + jsonString);
                                return;
                            }
                            wRefBook.get().resultCount = ((Long) jResult.remove("count")).intValue();

                            jResult.remove("facets");
                            it = jResult.keySet().iterator();
                            bookArray = new Object[jResult.size()];

                            while (it.hasNext()) {
                                Object currentIndex = it.next();
                                // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                                int intIndex = Integer.parseInt(currentIndex.toString());
                                bookArray[intIndex] = jResult.get(currentIndex);
                            }

                            wRefBook.get().resultList = new ListeFiches(bookArray);
                            bla = wRefBook.get(). new BookListAdapter(wRefBook.get().resultList);
                            wRefBook.get().bookListView.setAdapter(bla);
                            Log.d("BOOLKSA", "setAdapter");
                            wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));
                            wRefBook.get().pageAmount.setContentDescription((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));
                            wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle));
                            wRefBook.get().listTitleView.setContentDescription((wRefBook.get().currentTitle)+(wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));

                            //wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle + " (" + currentPage + "\n" + wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " " + wRefBook.get().getResources().getString(R.string.Catalogue_Sur) + " " + wRefBook.get().lastPage()));
                            //loadPictures(resultList);
                            break;
                        case ListeFiches.FILMS_TYPE:
                            jResult = JSonManager.getResultNode(jsonString, "NewSearch");

                            if (jResult == null) {
                                System.out.println("Erreur dans handler films: " + jsonString);
                                return;
                            }

                            wRefBook.get().resultCount = ((Long) jResult.remove("count")).intValue();
                            jResult.remove("facets");
                            it = jResult.keySet().iterator();
                            bookArray = new Object[jResult.size()];

                            while (it.hasNext()) {
                                Object currentIndex = it.next();
                                // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                                int intIndex = Integer.parseInt(currentIndex.toString());
                                bookArray[intIndex] = jResult.get(currentIndex);
                            }

                            wRefBook.get().resultList = new ListeFiches(bookArray);
                            bla = wRefBook.get(). new BookListAdapter(wRefBook.get().resultList);
                            wRefBook.get().bookListView.setAdapter(bla);
                            Log.d("BOOLKSA", "setAdapter films");
                            wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " / " + wRefBook.get().lastPage()));
                            wRefBook.get().pageAmount.setContentDescription((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));
                            wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.BSR_AudioDescription));
                            wRefBook.get().listTitleView.setContentDescription(wRefBook.get().getResources().getString(R.string.BSR_AudioDescription)+(wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));
                            break;
                        case ListeFiches.CATEGS_TYPE:
                            jResult = JSonManager.getResultNode(jsonString, "NewSearch");

                            if (jResult == null) {
                                System.out.println("Erreur dans handler categs: " + jsonString);
                                return;
                            }

                            wRefBook.get().resultCount = ((Long) jResult.remove("count")).intValue();
                            jResult.remove("facets");
                            it = jResult.keySet().iterator();
                            bookArray = new Object[jResult.size()];

                            while (it.hasNext()) {
                                Object currentIndex = it.next();
                                // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                                int intIndex = Integer.parseInt(currentIndex.toString());
                                bookArray[intIndex] = jResult.get(currentIndex);
                            }

                            wRefBook.get().resultList = new ListeFiches(bookArray);
                            bla = wRefBook.get(). new BookListAdapter(wRefBook.get().resultList);
                            wRefBook.get().bookListView.setAdapter(bla);
                            Log.d("BOOLKSA", "setAdapter");
                            wRefBook.get().pageAmount.setText((wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));

                            wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle));
                            wRefBook.get().listTitleView.setContentDescription((wRefBook.get().currentTitle)+", "+(wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " sur " + wRefBook.get().lastPage()));

                            //wRefBook.get().listTitleView.setText((wRefBook.get().currentTitle + "\n" + wRefBook.get().getResources().getString(R.string.Catalogue_Page) + " " + currentPage + " " + wRefBook.get().getResources().getString(R.string.Catalogue_Sur) + " " + wRefBook.get().lastPage()));
                            // loadPictures(resultList);
                            break;

                        case ListeFiches.SIMILAR_TYPE:
                            Log.d("MOREL", jsonString);
                            jBookArray = JSonManager.getResultArray(jsonString, "MoreLikeThisByCode");
                            if (jBookArray == null || jBookArray.size() == 0) {
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Catalogue_Pas_De_Similaire));
                            } else {
                                Object[] similarArray = jBookArray.toArray();
                                wRefBook.get().resultCount = similarArray.length;
                                /*int lastIndex = currentPage*10;
                                if (currentPage == lastPage()) {
                                    lastIndex = (currentPage-1) * 10 + resultCount % 10;
                                }*/
                                // Object[] rangeFiche = Arrays.copyOfRange(wishesArray, (currentPage-1)*10, lastIndex);
                                wishList = new ListeFiches(similarArray);

                                bla = wRefBook.get(). new BookListAdapter(wishList);
                                wRefBook.get().bookListView.setAdapter(bla);
                                Log.d("BOOLKSA", "setAdapter");
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Catalogue_Similaires));
                            }
                            break;
                        case ListeFiches.TRIAL_TYPE:
                            jBookArray = JSonManager.getResultArray(jsonString, "GetFreeBooks");
                            if (jBookArray == null || jBookArray.size() == 0) {
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Catalogue_Pas_De_Similaire));
                            } else {
                                Object[] freeBooksArray = jBookArray.toArray();
                                wRefBook.get().resultCount = freeBooksArray.length;
                                /*int lastIndex = currentPage*10;
                                if (currentPage == lastPage()) {
                                    lastIndex = (currentPage-1) * 10 + resultCount % 10;
                                }*/
                                // Object[] rangeFiche = Arrays.copyOfRange(wishesArray, (currentPage-1)*10, lastIndex);

                                wRefBook.get().resultList = new ListeFiches(freeBooksArray);
                                bla = wRefBook.get(). new BookListAdapter(wRefBook.get().resultList);
                                wRefBook.get().bookListView.setAdapter(bla);
                                Log.d("BOOLKSA", "setAdapter");
                                wRefBook.get().listTitleView.setText(wRefBook.get().getResources().getString(R.string.Catalogue_LivresDessai));
                            }
                            break;
                    }

                    if(wRefBook.get().resultCount > 10) {
                        wRefBook.get().imageView12.setVisibility(View.VISIBLE);
                        wRefBook.get().paginLayout.setVisibility(View.VISIBLE);
                        if(paginationHeight > 0) {
                            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) wRefBook.get().paginLayout.getLayoutParams();
                            layoutParams.height = paginationHeight;
                            wRefBook.get().paginLayout.setLayoutParams(layoutParams);
                        }

                    } else {
                        wRefBook.get().imageView12.setVisibility(View.INVISIBLE);
                        wRefBook.get().paginLayout.setVisibility(View.INVISIBLE);

                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)wRefBook.get().paginLayout.getLayoutParams();
                        paginationHeight = layoutParams.height;
                        layoutParams.height = 0;
                        wRefBook.get().paginLayout.setLayoutParams(layoutParams);
                    }

                    if (currentPage == 1 && wRefBook.get().resultCount <= 10) {
                        wRefBook.get().nextButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage == wRefBook.get().lastPage()) {
                        wRefBook.get().nextButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage != 1) {
                        wRefBook.get().previousButton.setVisibility(View.VISIBLE);
                    }

                    wRefBook.get().bookListView.setItemsCanFocus(false);
                    // listTitleView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wRefBook.get().listTitleView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                    }
                }, 200);
            }
        }
    }

    OnTouchListener listListener = new OnTouchListener() {

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (event.getPointerCount() >= 2) {
                mLastClickTime = SystemClock.elapsedRealtime();
            }
            return false;
        }
    };

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        viewType = HomeActivity.getViewType();

        /* INIT CACHE FOR COVER IMAGES */
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        memoryCache = new MemoryCache(getApplicationContext());

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_book_list);

        bookListView = findViewById(R.id.bookListView);
        listTitleView = findViewById(R.id.bookListTitle);
        pageAmount = findViewById(R.id.pageAmount);
        paginLayout = findViewById(R.id.paginLayout);
        imageView12 = findViewById(R.id.imageView12);

        intent = getIntent();

        listType = intent.getIntExtra("listType", -1);

        String title = intent.getStringExtra("title");

        if (title != null) {
            mResultTitle = title;
        }

        bookListView.setOnTouchListener(listListener);
        previousButton = findViewById(R.id.previousButton);
        homeButton = findViewById(R.id.homeBarButton);

        nextButton = findViewById(R.id.nextButton);
        previousActivityButton = findViewById(R.id.previousBarButton);

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPage--;
                if (playing) {
                    mediaPlayer.pause();
                    playing = false;
                }
                nextButton.setVisibility(View.VISIBLE);

                if (currentPage == 1) {
                    previousButton.setVisibility(View.INVISIBLE);
                }
                refreshList();
            }
        });

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intents = new Intent(getApplicationContext(), HomeActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                getApplicationContext().startActivity(intents);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currentPage++;
                if (playing) {
                    mediaPlayer.pause();
                    playing = false;
                }

                previousButton.setVisibility(View.VISIBLE);
                if (currentPage == lastPage()) {
                    nextButton.setVisibility(View.INVISIBLE);
                }

                refreshList();
            }
        });

        previousActivityButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                BookListActivity.this.onBackPressed();
            }
        });

    }

    private int lastPage() {

        int lastPage = resultCount / 10;
        if (resultCount % 10 > 0) {
            lastPage++;
        }

        return lastPage;
    }

    @Override
    protected void onPause() {

        super.onPause();
        if(mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        playing = false;
    }

    @Override
    protected void onResume() {

        super.onResume();
        listTitleView = findViewById(R.id.bookListTitle);
        Utils.isInternetAvailable(getApplicationContext(), false);

        try {
            listType = getIntent().getIntExtra("listType", -1);
            mBib = Bibliotheque.getBibliotheque(getApplicationContext());
            refreshList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshList() {

        Log.d("BOOOLK", "refreshlist");
        WebcallTask wt = new WebcallTask(getApplicationContext(), handler);
        if (currentType != listType) {
            currentPage = 1;
            currentType = listType;
        }

        switch (listType) {
            case ListeFiches.WISHES_TYPE:
                currentTitle = getResources().getString(R.string.ListeDeVoeux_ListeDeVoeux);
                // listTitleView.setText(R.string.ListeDeVoeux_ListeDeVoeux);

                String query = "GetWishes";
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);

                break;

            case ListeFiches.VOTATIONS_TYPE:
                currentTitle = getResources().getString(R.string.ListeDeVoeux_ListeDeVoeux);
                // listTitleView.setText(R.string.ListeDeVoeux_ListeDeVoeux);

                query = "NextVotations";
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);

                break;
            case ListeFiches.FILMS_TYPE:
                currentTitle = getResources().getString(R.string.Recherche_RechercheDeLivres);
                query = "NewSearch&values={\"genre\":\"AUT AU\",\"page\":\"" + (currentPage - 1) + "\"}";
                Log.d("Films audiod.", query);

                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
                break;
            case ListeFiches.SEARCH_TYPE:
                currentTitle = getResources().getString(R.string.Recherche_RechercheDeLivres);
                // listTitleView.setText(getResources().getString(R.string.Recherche_RechercheEnCours));
                query = intent.getStringExtra("query");
                query = query.substring(0, query.length() - 1) + ",\"page\":\"" + (currentPage - 1) + "\"}";
                Log.d("Pagination", query);

                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
                break;

            case ListeFiches.CATEGS_TYPE:
                currentTitle = intent.getStringExtra("genre");
                String code = intent.getStringExtra("code");

                if (code.equals("all")) {
                    query = "NewSearch&values=" + "{\"count\":\"10\",\"page\":\"" + (currentPage - 1) + "\"}";
                } else {
                    query = "NewSearch&values=" + "{\"genre\":{\"" + code + "\":\"" + code + "\"},\"count\":\"10\",\"page\":\"" + (currentPage - 1) + "\"}";
                }
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
                break;

            case ListeFiches.SIMILAR_TYPE:
                currentTitle = getResources().getString(R.string.Catalogue_Similaires);
                // listTitleView.setText(R.string.ListeDeVoeux_ListeDeVoeux);

                query = intent.getStringExtra("query");
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);

                break;
            case ListeFiches.TRIAL_TYPE:
                currentTitle = getResources().getString(R.string.Catalogue_LivresDessai);
                // listTitleView.setText(R.string.ListeDeVoeux_ListeDeVoeux);

                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "GetFreeBooks");

                break;
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("BOOOLK", "refreshlist focus");
                listTitleView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 500);
    }

    private static class ViewHolder {

        private TextView titleText;
        private TextView authorText;
        private TextView mediaText;
        //private TextView synthText;

        private ImageView playImage;
        private ImageView wishlistImage;
        private ImageView dlImage;
        private ImageView coverImage;
    }

    class BookListAdapter extends BaseAdapter {

        ListeFiches bookList;

        BookListAdapter(ListeFiches bookList) {

            this.bookList = bookList;
        }

        @Override
        public int getCount() {
            return bookList.getCount();
        }

        @Override
        public FicheLivre getItem(int i) {
            return bookList.getItem(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater li = (LayoutInflater) BookListActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
            assert (li != null);
            ViewHolder holder;

            if (convertView == null) {
                switch (viewType) {
                    case "text":
                        convertView = li.inflate(R.layout.book_row_text, viewGroup, false);
                        break;
                    case "cover":
                        convertView = li.inflate(R.layout.book_row_cover, viewGroup, false);
                        break;
                    case "text_cover":
                        convertView = li.inflate(R.layout.book_row_text_cover, viewGroup, false);
                        break;
                }
                assert (convertView != null);

                holder = new ViewHolder();

                if (viewType.equals("text") || viewType.equals("text_cover")) {
                    holder.titleText = convertView.findViewById(R.id.titleText);
                    holder.authorText = convertView.findViewById(R.id.authorText);
                    holder.mediaText = convertView.findViewById(R.id.mediaText);
                }

                // holder.synthText = convertView.findViewById(R.id.synthText);

                holder.playImage = convertView.findViewById(R.id.book_action_1);
                holder.wishlistImage = convertView.findViewById(R.id.book_action_2);
                holder.dlImage = convertView.findViewById(R.id.book_action_3);

                if (!viewType.equals("text")) {
                    holder.coverImage = convertView.findViewById(R.id.coverImage);
                }
                if(listType == ListeFiches.TRIAL_TYPE){
                    holder.wishlistImage.setVisibility(View.GONE);
                } else {
                    holder.wishlistImage.setVisibility(View.VISIBLE);
                }
                convertView.setTag(R.string.Tags_Holder, holder);
            } else {
                holder = (ViewHolder) convertView.getTag(R.string.Tags_Holder);
                if (!viewType.equals("text")) {
                    holder.coverImage.setImageBitmap(null);
                }
            }

            final FicheLivre currentBook = bookList.getItem(i);
            final ImageView wlImage = holder.wishlistImage;
            final ImageView plImage = holder.playImage;
            holder.dlImage.setContentDescription(getResources().getString(R.string.Catalogue_Telecharger) + " " + currentBook.getTitle());
            holder.playImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait) + " " + getResources().getString(R.string.Global_De_livre) + " " + currentBook.getTitle());
            holder.dlImage.setEnabled(true);
            holder.dlImage.setAlpha(1f);

            OnClickListener wishlistListener = new OnClickListener() {
                @Override
                public void onClick(View view) {

                    String request;
                    if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                        request = "DeleteWish&bookNr=" + currentBook.getNoticeNr();
                        wlImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                        wlImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu));
                        mBib.removeWishFromList(currentBook.getNoticeNr());
                        Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuSupprime));
                    } else {
                        request = "AddWish&bookNr=" + currentBook.getNoticeNr();
                        wlImage.setImageResource(R.drawable.ic_star_white_48dp);
                        wlImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu));
                        mBib.addWishToList(currentBook);
                        Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuAjoute));
                    }
                    WebcallTask wt = new WebcallTask(getApplicationContext());
                    wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
                }
            };
            OnClickListener extraitListener = new OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    if (currentBook.getMp3Url() != null) {
                        String mp3URL = currentBook.getMp3Url().toString();

                        if (playing) {
                            mediaPlayer.stop();
                            lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                            lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                            playing = false;
                            if (plImage != lastPlayedImage) {
                                plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                                plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                                lastPlayedImage = plImage;
                                playExtrait(mp3URL);
                                playing = true;
                            }

                        } else {
                            lastPlayedImage = plImage;
                            playExtrait(mp3URL);

                            plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                            plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                            playing = true;
                        }
                    }
                }

            };

            OnClickListener dlListener = new OnClickListener() {
                @Override
                public void onClick(View view) {

                    mLastClickedView = view;
                    final String bookName = (String) view.getTag(R.string.Tags_NoticeNr);
                    final FicheLivre currentBook;

                    if (votationsList != null) {
                        if (votationsList.getItemByNumber(bookName) != null) {
                            currentBook = votationsList.getItemByNumber(bookName);
                        } else {
                            currentBook = votationsList.getItemByNumber(bookName);
                        }
                    }
                    else if (wishList != null) {
                        if (wishList.getItemByNumber(bookName) != null) {
                            currentBook = wishList.getItemByNumber(bookName);
                        } else {
                            currentBook = resultList.getItemByNumber(bookName);
                        }
                    } else {
                        currentBook = resultList.getItemByNumber(bookName);
                    }

                    mBib.addFiche(currentBook);

                    if (mBib.bookInLibrary(bookName)) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_LivreDejaPresent));
                        return;
                    }

                    long freeSpace = mBib.getFreeSpace();
                    long bookSize = currentBook.getZipSize();

                    if (freeSpace < bookSize * 2) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_PasAssezEspace));
                        return;
                    }

                    ConnectivityManager cm =
                            (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                    NetworkInfo activeNetwork = null;

                    if (cm != null) {
                        activeNetwork = cm.getActiveNetworkInfo();
                    }

                    boolean isWiFi = false;
                    if (activeNetwork != null) {
                        isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                    }

                    final Intent dlIntent = new Intent(getApplicationContext(), DownloadService.class);
                    dlIntent.putExtra("zipUrl", currentBook.getZipUrl());
                    dlIntent.putExtra("noticeNr", currentBook.getNoticeNr());
                    dlIntent.putExtra("noticeNr", currentBook.getNoticeNr());
                    dlIntent.putExtra("description", currentBook.getTitle());

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    startDl(dlIntent, currentBook);
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    //Log.d("dialogClickListener", "Not downloading");
                                    break;
                            }
                        }
                    };

                    if (!isWiFi) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(BookListActivity.this);
                        // AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setMessage(getResources().getString(R.string.Catalogue_AvertissementReseau)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialogClickListener)
                                .setNegativeButton(getResources().getString(R.string.Global_Annuler), dialogClickListener);

                        AlertDialog alert = builder.create();
                        Utils.prepareAndShowAlert(getApplicationContext(), alert);

                    } else {
                        startDl(dlIntent, currentBook);
                    }
                }
            };

            if (viewType.equals("text") || viewType.equals("text_cover")) {
                holder.titleText.setText(currentBook.getTitle());
                holder.authorText.setText(currentBook.getAuthor());
                holder.mediaText.setText(Utils.formatDuration(currentBook.getMedia(), getApplicationContext()));
                holder.mediaText.setContentDescription(getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext()));
            }

            if (!viewType.equals("text")) {
                memoryCache.loadBitmapCoverImage(holder.coverImage, currentBook.getCover(), currentBook.getNoticeNr());
            }

            /*if (currentBook.getReader().equals("Manon")) {
                holder.synthText.setText(getResources().getString(R.string.Global_Voix_SyntheseCourt));
            } else {
                holder.synthText.setText("");
            }*/

            holder.playImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());
            holder.dlImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            if (checkBookActivity(currentBook.getNoticeNr())) {
                holder.dlImage.setClickable(false);
                holder.dlImage.setImageAlpha(100);
            } else{
                holder.dlImage.setClickable(true);
                holder.dlImage.setImageAlpha(255);
            }

            convertView.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            holder.playImage.setOnClickListener(extraitListener);
            holder.dlImage.setOnClickListener(dlListener);
            holder.wishlistImage.setOnClickListener(wishlistListener);

            String description = currentBook.getTitle() + " " + getResources().getString(R.string.Global_De) + " " + currentBook.getAuthor() + ", " + getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext());

            if (currentBook.getReader().equals("Manon") || currentBook.getReader().equals("Alice")) {
                description += ", " + getResources().getString(R.string.Global_Voix_Synthese);
            }

            convertView.setOnClickListener(playBookListener);
            convertView.setContentDescription(description);

            if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu) + " " + currentBook.getTitle());
            } else {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu) + " " + currentBook.getTitle());
            }

            return convertView;
        }
    }

    private void playExtrait(String url) {
        if(mediaPlayer == null){
            mediaPlayer= new MediaPlayer();
        }else {
            mediaPlayer.reset();
        }

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        } catch (IOException e) {
            Log.e("AudioService", "Exception IO");
            e.printStackTrace();
        }
    }

    public void onCompletion(MediaPlayer _mediaPlayer) {

        lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
        playing = false;
    }

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // do nothing
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    DialogInterface.OnClickListener dialog2ClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    finish();
                    Intent dlIntent = new Intent(getBaseContext(), DownloadsActivity.class);
                    dlIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(dlIntent);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    // on y retourne
                    break;
            }
        }
    };

    private void startDl(Intent intent, FicheLivre book) {

        mLastClickedView.setEnabled(false);
        mLastClickedView.setAlpha(0.3f);

        Calendar cal = Calendar.getInstance();
        book.setDownloadDate(cal.getTimeInMillis());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else{
            startService(intent);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(BookListActivity.this);
        builder.setMessage(getResources().getString(R.string.Catalogue_TelechargementDe) + " " + book.getTitle() + ".\n\n" + getResources().getString(R.string.Catalogue_AllerTelechargements)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialog2ClickListener)
                .setNegativeButton(getResources().getString(R.string.Global_Non), dialog2ClickListener);

        AlertDialog alert = builder.create();

        Utils.prepareAndShowAlert(getApplicationContext(), alert);
    }

    private boolean checkBookActivity(String noticeNr) {

        boolean downloading = false;
        boolean alreadyThere = false;
        for (int i = 0; i < DownloadsActivity.downloads.size(); i++) {
            if (DownloadsActivity.downloads.get(i).noticeNr.equals(noticeNr)) {
                Log.d("BOOLK", noticeNr + " déjà en cours de téléchargement");
                downloading = true;
            }
        }
        if (mBib.bookInLibraryByNoticeNr(noticeNr)) {
            Log.d("BOOLK", noticeNr + " dans la biblio");
            alreadyThere = true;
        }
        return downloading || alreadyThere;
    }
}