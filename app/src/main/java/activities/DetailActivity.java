package activities;

import static fragments.LoginFragment.PREF_TAG;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Calendar;

import bibliothequesonore.ch.livresbsrandroid.R;
import services.DownloadService;
import tasks.WebcallTask;
import utils.Bibliotheque;
import utils.FicheLivre;
import utils.JSonManager;
import utils.ListeFiches;
import utils.MemoryCache;
import utils.Utils;

public class DetailActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private FicheLivre mFiche;
    private boolean mPlaying;
    private Intent mDlIntent;
    private Bibliotheque mBib;
    private String bookNr;
    private MenuItem mWishMenu;
    private MenuItem mSampleMenu;
    private MenuItem mStreamMenu;
    private static MenuItem mDownloadMenu;
    private String viewType = "no cover"; // cover or no cover
    private int listType;

    public final static int WCT = 1;

    private WeakReference<DetailActivity> weakReferenceThis = new WeakReference<>(this);
    private IncomingHandler handler = new DetailActivity.IncomingHandler(weakReferenceThis);

    class IncomingHandler extends Handler {

        private WeakReference<DetailActivity> wRefBook;
        BookListActivity.BookListAdapter bla;

        private IncomingHandler(WeakReference<DetailActivity> w) {
            wRefBook = w;
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == WCT) {
                if(msg.obj == null){
                    finish();
                }
                String jsonString = (String) msg.obj;
                if (!JSonManager.getError(jsonString).equals("")) {
                    Toast.makeText(wRefBook.get().getApplicationContext(), "Erreur: " + JSonManager.getError(jsonString), Toast.LENGTH_SHORT).show();
                } else {
                    JSONObject jResult = JSonManager.getResultNode(jsonString, "FindBook");

                    if (jResult == null) {
                        Log.e("booolk", "Erreur dans handler search: " + jsonString);
                        return;
                    }
                    Log.d("booolk", ": " + jsonString);
                    Object[] bookArray = new Object[1];
                    bookArray[0] = jResult;
                    Log.d("booolk", ": " + bookArray[0]);
                    mFiche = new ListeFiches(bookArray).getItem(0);
                    updateContent();
                }
            }

        }
    }

    static MediaPlayer mediaPlayer;

    @Override
	protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        viewType = HomeActivity.getViewType();

        if (viewType.equals("cover") || viewType.equals("text_cover")) {
            setContentView(R.layout.activity_detail_cover);

        } else {
            setContentView(R.layout.activity_detail);
        }

        Toolbar myToolbar = findViewById(R.id.player_toolbar);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_prev_toolbar_black_48dp);
        getSupportActionBar().setHomeActionContentDescription("Retour");

        mBib = Bibliotheque.getBibliotheque(getApplicationContext());
        Intent intent = getIntent();
        String appLinkAction = intent.getAction();
        String appLinkData = "";
        if(intent.getData() != null) {
            appLinkData = intent.getData().toString();
        }

        if(appLinkAction != null && appLinkAction.equals("android.intent.action.VIEW")){
            Log.d("DetailActivity", appLinkData);
            bookNr = appLinkData.substring(appLinkData.lastIndexOf('/') + 1).trim();
            bookNr = bookNr.replaceFirst("^0+(?!$)", "");
        } else {

            bookNr = intent.getStringExtra("bookNr");
            listType = intent.getIntExtra("listType", 2);
            Log.d("DetailActivity", "b" + bookNr);
        }

        mFiche = ListeFiches.getTheList().getItemByNumber(bookNr);

        if (mFiche == null) {
            updateContent();
            if(bookNr != null && !bookNr.equals("")){
                WebcallTask wt = new WebcallTask(getApplicationContext(), handler);
                String query = "FindBook&values="+ bookNr;
                Log.d("DetailActivity", query);
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
            } else{
                Log.e("DetailActivity", "Erreur: pas de numéro de livre");
            }
        } else{
            updateContent();
        }
    }

    private void updateContent() {
        TextView titleText = findViewById(R.id.detailTitle);
        TextView authorText = findViewById(R.id.detailAuthorText);
        TextView summaryText = findViewById(R.id.detailSummaryText);
        TextView editorText = findViewById(R.id.detailEditorText);
        TextView readerText = findViewById(R.id.detailReaderText);
        TextView mediaText = findViewById(R.id.detailMediaText);
        TextView genreText = findViewById(R.id.detailGenreText);
        TextView producerText = findViewById(R.id.detailProducerText);
        TextView numeroText = findViewById(R.id.detailNumeroText);
        ImageView coverImage;

        if (mFiche == null) {
            titleText.setText("");
            authorText.setText("");
            summaryText.setText("");
            editorText.setText("");
            readerText.setText("");
            mediaText.setText("");
            if(genreText!=null) {
                genreText.setText("");
                producerText.setText("");
            }
            numeroText.setText("");
        } else {
            authorText.setText(mFiche.getAuthor());
            authorText.setContentDescription(getResources().getString(R.string.Global_De_auteur) + ": " + mFiche.getAuthor());
            titleText.setText(mFiche.getTitle());
            summaryText.setText(mFiche.getSummary());
            summaryText.setContentDescription(getResources().getString(R.string.Catalogue_Resume) + ": " + mFiche.getSummary());

            if (mFiche.getReader().equals("Manon")) {
                readerText.setText((getResources().getString(R.string.Global_LuPar) + " " + mFiche.getReader() + " (" + getResources().getString(R.string.Global_Voix_SyntheseCourt) + ")"));
            } else {
                readerText.setText((getResources().getString(R.string.Global_LuPar) + " " + mFiche.getReader()));
            }
            if(mFiche.getGenre().contains("Audiodescription")){
                readerText.setText((getResources().getString(R.string.Global_AudiodecritPar) + " " + mFiche.getReader()));
            }

            if (viewType.equals("cover") || viewType.equals("text_cover")) {
                coverImage = findViewById(R.id.coverImage);
                MemoryCache memoryCache = new MemoryCache(this);
                memoryCache.loadBitmapCoverImage(coverImage, mFiche.getCover(), mFiche.getNoticeNr());
            }

            mediaText.setText(Utils.formatDuration(mFiche.getMedia(), getApplicationContext()));
            mediaText.setContentDescription(getResources().getString(R.string.Global_Duree) + " :" + Utils.formatAccessibleDuration(mFiche.getMedia(), getApplicationContext()));

            if(genreText!=null) {
                genreText.setText(mFiche.getGenre());
                genreText.setContentDescription(getResources().getString(R.string.Catalogue_Genre) + " :" + mFiche.getGenre());
                producerText.setText(mFiche.getProducer());
                producerText.setContentDescription(getResources().getString(R.string.Catalogue_Producteur) + " :" + mFiche.getProducer());
            }
            editorText.setText(mFiche.getEditor());
            editorText.setContentDescription(getResources().getString(R.string.Catalogue_Editeur) + ": " + mFiche.getEditor());

            numeroText.setText(("N° " + mFiche.getNoticeNr()));
            numeroText.setContentDescription(getResources().getString(R.string.Catalogue_NumeroNotice) + ": " + mFiche.getNoticeNr());

            Utils.sendTBMessage(this, mFiche.getTitle() + ", " + getResources().getString(R.string.Global_De_auteur) + ": " + mFiche.getAuthor());
        }
    }

    public void onResume() {

        super.onResume();

        checkBookActivity();
    }

    private void checkBookActivity() {

        boolean downloading = false;
        boolean alreadyThere = false;

        for(int i = 0 ; i<DownloadsActivity.downloads.size();i++) {
            if (mFiche != null && DownloadsActivity.downloads.get(i).noticeNr.equals(mFiche.getNoticeNr())) {
                downloading = true;
            }
        }
        if (mBib.bookInLibraryByNoticeNr(bookNr)) {
            alreadyThere = true;
        }
        if (downloading || alreadyThere) {
            disableDownloadButton();
        } else {
            enableDownloadButton();
        }
    }


    protected void onPause(){

        if (mPlaying) {
            mediaPlayer.release();
            mediaPlayer = null;
            mSampleMenu.setIcon(R.drawable.ic_play_arrow_black_48dp);
            mSampleMenu.setTitle(getResources().getString(R.string.Catalogue_EcouterExtrait));
            mPlaying = false;
        }
        bookNr=null;
        super.onPause();
    }

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU ) {
            // do nothing
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.detail_actions, menu);

        mWishMenu = menu.findItem(R.id.action_wish);

        if(listType == ListeFiches.TRIAL_TYPE){
            mWishMenu.setVisible(false);
            menu.findItem(R.id.action_similar).setVisible(false);
            menu.findItem(R.id.action_author).setVisible(false);
            menu.findItem(R.id.action_narrator).setVisible(false);

        } else {
            mWishMenu.setVisible(true);
            menu.findItem(R.id.action_similar).setVisible(true);
            menu.findItem(R.id.action_author).setVisible(true);
            menu.findItem(R.id.action_narrator).setVisible(true);
            if (mBib.isOnWishList(bookNr)) {
                mWishMenu.setIcon(R.drawable.ic_star_black_48dp);
                mWishMenu.setTitle(getResources().getString(R.string.Catalogue_SupprimerVoeu));
            } else {
                mWishMenu.setIcon(R.drawable.ic_star_border_black_48dp);
                mWishMenu.setTitle(getResources().getString(R.string.Catalogue_AjouterVoeu));
            }
        }

        mSampleMenu = menu.findItem(R.id.action_sample);
        mDownloadMenu = menu.findItem(R.id.action_download);
        mStreamMenu = menu.findItem(R.id.action_stream);

        if(mFiche != null && mFiche.getGenre().contains("Audiodescription")){
            mStreamMenu.setVisible(true);
        }
        /*if(mContext == ListeFiches.WISHES_TYPE) {
            // mWishMenu.setIcon(R.drawable.ic_bookmark_black_48dp);
        }*/

        checkBookActivity();
		return super.onCreateOptionsMenu(menu);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_author:
                String author = mFiche.getAuthor().replace(" ", "%20");
                String arequest = "NewSearch&values="+"{\"queryText\":\""+author+"\",\"queryType\":\"author\",\"count\":\"10\"}";
                Intent aintent = new Intent(getApplicationContext(), BookListActivity.class);
                aintent.putExtra("listType", ListeFiches.SEARCH_TYPE);
                aintent.putExtra("query", arequest);
                startActivity(aintent);
                break;

            case R.id.action_narrator:
                if (mFiche.getReader() == null) {
                    return false;
                }

                String narrator = mFiche.getReader().replace(" ", "%20");
                String nrequest = "NewSearch&values="+"{\"reader\":\""+narrator+"\",\"count\":\"10\"}";
                Intent nintent = new Intent(getApplicationContext(), BookListActivity.class);
                nintent.putExtra("listType", ListeFiches.SEARCH_TYPE);
                nintent.putExtra("query", nrequest);
                nintent.putExtra("title", getResources().getString(R.string.Global_LuPar)+" "+mFiche.getReader());
                startActivity(nintent);
                break;

            case R.id.action_similar:
                String code = mFiche.getNoticeNr();
                String srequest = "MoreLikeThisByCode&code="+code+"&count="+10;
                Log.d("MOREL", srequest);
                Intent sintent = new Intent(getApplicationContext(), BookListActivity.class);
                sintent.putExtra("listType", ListeFiches.SIMILAR_TYPE);
                sintent.putExtra("query", srequest);
                sintent.putExtra("title", getResources().getString(R.string.Catalogue_Similaires));
                startActivity(sintent);
                break;

            case R.id.action_sample:
                if(mFiche.getMp3Url()!= null) {
                    if (mPlaying) {
                        mediaPlayer.stop();
                        mSampleMenu.setIcon(R.drawable.ic_play_arrow_black_48dp);
                        mSampleMenu.setTitle(getResources().getString(R.string.Catalogue_EcouterExtrait));
                        mPlaying = false;
                    } else {
                        playExtrait(mFiche.getMp3Url().toString());
                        mSampleMenu.setIcon(R.drawable.ic_pause_black_48dp);
                        mSampleMenu.setTitle(getResources().getString(R.string.Lecteur_MettrePause));
                        mPlaying = true;
                    }
                }
                break;
            case R.id.action_stream:
                WebcallTask wt_LOG = new WebcallTask(getApplicationContext(), null);
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
                String lastUsername = prefs.getString("username", null);
                String query = "AddDownloadLog&client=android_dvd_stream&login="+lastUsername+"&code="+mFiche.getNoticeNr();
                wt_LOG.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
                Log.d("STREAM", query);
                Intent intent2 = new Intent(Intent.ACTION_VIEW );
                intent2.setDataAndType(Uri.parse("https://medias.bibliothequesonore.ch/dvds/"+mFiche.getNoticeNr()+".mp4"), "video/*");
                startActivity(intent2);
                break;

            case R.id.action_wish:
                String request;
                if (mBib.isOnWishList(mFiche.getNoticeNr())) {
                    request = "DeleteWish&bookNr=" + mFiche.getNoticeNr();
                    mWishMenu.setIcon(R.drawable.ic_star_border_black_48dp);
                    mWishMenu.setTitle(getResources().getString(R.string.Catalogue_AjouterVoeu));
                    mBib.removeWishFromList(mFiche.getNoticeNr());
                    Utils.showToast(getApplicationContext(), mFiche.getTitle()+" "+getResources().getString(R.string.Catalogue_VoeuSupprime));

                } else {
                    request = "AddWish&bookNr=" + mFiche.getNoticeNr();
                    mWishMenu.setIcon(R.drawable.ic_star_black_48dp);
                    mWishMenu.setTitle(getResources().getString(R.string.Catalogue_SupprimerVoeu));
                    mBib.addWishToList(mFiche);
                    Utils.showToast(getApplicationContext(), mFiche.getTitle()+" "+getResources().getString(R.string.Catalogue_VoeuAjoute));
                }

                WebcallTask wt = new WebcallTask(getApplicationContext());
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
                break;

            case R.id.action_download:
                String bookName = mFiche.getNoticeNr();
                mBib.addFiche(mFiche);
                if (mBib.bookInLibrary(bookName)) {
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_LivreDejaPresent));
                    break;
                }

                long bookSize = mFiche.getZipSize();
                long freeSpace = mBib.getFreeSpace();

                if (freeSpace < bookSize*2.1 ){
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_PasAssezEspace));
                    break;
                }

                ConnectivityManager cm =
                        (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = null;
                if (cm != null) {
                    activeNetwork = cm.getActiveNetworkInfo();
                }

                boolean isWiFi = false;
                if (activeNetwork != null) {
                    isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                }

                mDlIntent = new Intent(getApplicationContext(), DownloadService.class);
                mDlIntent.putExtra("zipUrl", mFiche.getZipUrl());
                mDlIntent.putExtra("noticeNr", mFiche.getNoticeNr());
                mDlIntent.putExtra("description", mFiche.getTitle());
                if(!isWiFi){
                    AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);

                    builder.setMessage(getResources().getString(R.string.Catalogue_AvertissementReseau)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialogClickListener)
                            .setNegativeButton(getResources().getString(R.string.Global_Annuler), dialogClickListener);

                    AlertDialog alert = builder.create();
                    Utils.prepareAndShowAlert(getApplicationContext(), alert);

                } else {
                    startDl();
                }
                break;
            case android.R.id.home:
                this.finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void playExtrait(String url) {
        if(mediaPlayer == null){
            mediaPlayer= new MediaPlayer();
        }else {
            mediaPlayer.reset();
        }

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mPlaying = true;
        } catch (IOException e) {
            Log.e("AudioService", "Exception IO");
            e.printStackTrace();
            mPlaying = false;
        }
    }

    public void onCompletion(MediaPlayer _mediaPlayer) {

        mSampleMenu.setIcon(R.drawable.ic_play_arrow_black_48dp);
        mSampleMenu.setTitle(getResources().getString(R.string.Catalogue_EcouterExtrait));
        mPlaying = false;
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                startDl();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
        }
    };

    private void startDl(){

        disableDownloadButton();
        Calendar cal = Calendar.getInstance();
        mFiche.setDownloadDate(cal.getTimeInMillis());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(mDlIntent);
        }else{
            startService(mDlIntent);
        }

        Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_TelechargementDe)+ " "+mFiche.getTitle());

        AlertDialog.Builder builder = new AlertDialog.Builder(DetailActivity.this);

        builder.setMessage(getResources().getString(R.string.Catalogue_AllerTelechargements)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialog2ClickListener)
                .setNegativeButton(getResources().getString(R.string.Global_Non), dialog2ClickListener);

        AlertDialog alert = builder.create();
        Utils.prepareAndShowAlert(getApplicationContext(), alert);
    }

    DialogInterface.OnClickListener dialog2ClickListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {

            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    finish();
                    Intent dlIntent = new Intent(getBaseContext(), DownloadsActivity.class);
                    dlIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(dlIntent);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    // on y retourne
                    break;
            }
        }
    };

    public void disableDownloadButton(){

        if(mDownloadMenu != null) {
            mDownloadMenu.setEnabled(false);
            mDownloadMenu.getIcon().setAlpha(130);
        }
    }

    public void enableDownloadButton() {

        if (mDownloadMenu != null) {
            mDownloadMenu.setEnabled(true);
            mDownloadMenu.getIcon().setAlpha(255);
        }
    }

    class DownloadCoverImageTask extends AsyncTask<String, Void, Bitmap> {
        // Inner class to download cover image as an async class to avoid that the activity
        // waits on the task to be completed

        private ImageView bmImage;

        private DownloadCoverImageTask(ImageView bmImage) {

            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {

            String urldisplay = urls[0];
            Bitmap bitmap = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {

            bmImage.setImageBitmap(result);
        }
    }

    /*private DetailActivity.IncomingHandler handler = new DetailActivity.IncomingHandler(weakReferenceThis);

    static class IncomingHandler extends Handler {

        private WeakReference<DetailActivity> wRefBook;

        private IncomingHandler(WeakReference<DetailActivity> w) {
            wRefBook = w;
        }

        @Override
        public void handleMessage(Message msg) {

            Log.d("APPLINK", "handler");
            if (msg.what == WCT) {
                String jsonString = (String) msg.obj;
                if (!JSonManager.getError(jsonString).equals("")) {
                    Toast.makeText(wRefBook.get().getApplicationContext(), "Erreur: " + JSonManager.getError(jsonString), Toast.LENGTH_SHORT).show();
                } else {
                    // Log.d("BOOLK", JSonManager.getResultNode(jsonString, "NewSearch").toString());
                    JSONObject jResult;

                    // Toast.makeText(getApplicationContext(), "Name: " + JSonManager.getFunction(jsonString), Toast.LENGTH_SHORT).show();
                    jResult = JSonManager.getResultNode(jsonString, "FindBook");
                    if (jResult == null) {
                        System.out.println("Erreur dans handler search: " + jsonString);
                    } else{
                        Log.d("APPLINK", jResult.toJSONString());
                    }

                }
            }
        }
    }*/
}