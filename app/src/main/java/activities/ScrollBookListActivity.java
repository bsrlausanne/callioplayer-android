package activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.accessibility.AccessibilityEvent;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Objects;

import bibliothequesonore.ch.livresbsrandroid.R;
import services.DownloadService;
import tasks.WebcallTask;
import utils.Bibliotheque;
import utils.FicheLivre;
import utils.JSonManager;
import utils.ListeFiches;
import utils.MemoryCache;
import utils.Utils;

public class ScrollBookListActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener {

    private final static int WCT = 1;

    // For endless scrolling
    private final static int VISIBLE_THRESHOLD = 4;
    private final static int ITEM_PER_PAGE = 10;
    private int page = 0;
    private boolean isLoading = false;
    private boolean new_request = true;
    private String request;

    public static ListeFiches wishList = null;
    private static boolean playing = false;
    public static MediaPlayer mediaPlayer;

    Bibliotheque mBib;
    ListeFiches resultList = null;
    int resultCount = 0;
    private long mLastClickTime = 0;
    private View mLastClickedView;
    private MemoryCache memoryCache;
    private String viewType = "cover";

    View.OnClickListener playBookListener;
    DialogInterface.OnClickListener dialog2ClickListener;

    ListView bookListView;
    TextView pageAmount;
    ImageView lastPlayedImage = null;

    String genreText;
    String genreCode;


    public ScrollBookListActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //Screen size
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);

        Intent intent = getIntent();
        genreText = intent.getStringExtra("genre");
        genreCode = intent.getStringExtra("code");

        memoryCache = new MemoryCache(this);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_scroll_book_list);

        mBib = Bibliotheque.getBibliotheque(getApplicationContext());

        bookListView = findViewById(R.id.bookListView);
        pageAmount = findViewById(R.id.pageAmount);

        initListeners();
    }

    @Override
    protected void onResume() {

        super.onResume();

        // init the view type which can be cover, text or mixed of the two
        viewType = HomeActivity.getViewType();

        Utils.isInternetAvailable(getApplicationContext(), true);
        mBib.login(null);

        search();

    }

    @Override
    protected void onPause() {

        if(mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        playing = false;
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // override to prevent crash with physical menu button on some phones.

        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            // do nothing
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    private void search() {

        request = "";
        new_request = true;
        page = 0;

        request += "NewSearch&values={\"count\":\"10\",\"genre\":[\"" + genreCode + "\"]";


        // end of request
        request += "}";

        Log.d("NewBookListQuery", "Query: " + request);

        getResultRequest(0);
    }

    private final WeakReference<ScrollBookListActivity> weakReferenceThis = new WeakReference<>(this);

    private final IncomingHandler handler = new IncomingHandler(weakReferenceThis);

    static class IncomingHandler extends Handler {

        private final WeakReference<ScrollBookListActivity> wRefBook;
        BookListAdapter bla;

        private IncomingHandler(WeakReference<ScrollBookListActivity> w) {
            wRefBook = w;
        }

        @Override
        public void handleMessage(Message msg) {
            if(msg.obj == null) { return; }
            Log.d("BRSS",  msg.obj.toString());
            if (msg.what == WCT) {
                String jsonString = (String) msg.obj;

                if (!JSonManager.getError(jsonString).equals("")) {
                    Toast.makeText(wRefBook.get().getApplicationContext(), "Erreur: " + JSonManager.getError(jsonString), Toast.LENGTH_SHORT).show();
                } else {

                    JSONObject jResult;
                    Iterator it;

                    jResult = JSonManager.getResultNode(jsonString, "NewSearch");
                    if (jResult == null) {
                        System.out.println("Erreur dans handler search: " + jsonString);
                        return;
                    }

                    Log.d("SearchResult", jResult.toJSONString());

                    wRefBook.get().resultCount = ((Long) Objects.requireNonNull(jResult.remove("count"))).intValue();
                    jResult.remove("facets");

                    if (wRefBook.get().resultCount > 0) {
                        wRefBook.get().bookListView.setVisibility(View.VISIBLE);
                        if (wRefBook.get().resultCount == 1) {
                            wRefBook.get().pageAmount.setText((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitreTrouve)));
                            wRefBook.get().pageAmount.setContentDescription((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitreTrouve)));

                        } else {
                            wRefBook.get().pageAmount.setText((wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitresTrouves)));
                        }
                        Utils.sendTBMessage(wRefBook.get(), (wRefBook.get().resultCount + " " + wRefBook.get().getResources().getString(R.string.Catalogue_TitresTrouves)));
                    } else {
                        wRefBook.get().pageAmount.setText(wRefBook.get().getResources().getString(R.string.Recherche_PasDeResultats));
                        wRefBook.get().bookListView.setVisibility(View.INVISIBLE);
                    }
                    it = jResult.keySet().iterator();
                    Object[] resultArray = new Object[jResult.size()];

                    while (it.hasNext()) {
                        Object currentIndex = it.next();
                        // pas trés élégant, mais les éléments ne sont pas dans l'ordre dans l'itérateur. on récupère l'index.
                        int intIndex = Integer.parseInt(currentIndex.toString());
                        resultArray[intIndex] = jResult.get(currentIndex);
                    }
                    Log.d("BRSS", "newrequest: " + wRefBook.get().new_request);
                    if (wRefBook.get().new_request) {
                        wRefBook.get().resultList = new ListeFiches(resultArray);
                        bla = wRefBook.get().new BookListAdapter(wRefBook.get().resultList);
                        wRefBook.get().bookListView.setAdapter(bla);
                    } else {
                        wRefBook.get().resultList.addFromBookArray(resultArray);
                        bla.addFromBookArray(resultArray);
                        bla.notifyDataSetChanged();
                    }

                    Log.d("SearchResult", "count from res: " + wRefBook.get().resultList.getCount());

                    wRefBook.get().bookListView.setItemsCanFocus(false);

                    // done loading so that next page can load
                    wRefBook.get().isLoading = false;

                    final Handler handler = new Handler();
                    handler.postDelayed(() -> wRefBook.get().bookListView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED), 200);
                }
            }
        }
    }

    private void getResultRequest(int page) {

        WebcallTask wt = new WebcallTask(getApplicationContext(), handler);

        String query = request.substring(0, request.length() - 1) + ",\"page\":\"" + page + "\"}";

        Log.d("Pagination", query);

        wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);

        final Handler handler = new Handler();
        handler.postDelayed(() -> bookListView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED), 500);
    }

    private void initListeners() {

        playBookListener = view -> {

            if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                return;
            }

            String bookName = (String) view.getTag(R.string.Tags_NoticeNr);

            mLastClickTime = SystemClock.elapsedRealtime();

            if (lastPlayedImage != null) {
                lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                playing = false;
            }

            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            //intent.putExtra("context", currentType);
            intent.putExtra("bookNr", bookName);
            startActivity(intent);
        };

        dialog2ClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    finish();
                    Intent dlIntent = new Intent(getBaseContext(), DownloadsActivity.class);
                    dlIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(dlIntent);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    // on y retourne
                    break;
            }
        };

        bookListView.setOnTouchListener((v, event) -> {

            if (event.getPointerCount() >= 2) {
                mLastClickTime = SystemClock.elapsedRealtime();
            }

            return false;
        });

        bookListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //nothing
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (request != null && !isLoading) {
                    Log.d("SearchResult", firstVisibleItem + "---" + visibleItemCount + "---" + totalItemCount);
                    // add more items to listview when there are two items remaining at the scroll end.
                    if (firstVisibleItem + visibleItemCount >= totalItemCount - VISIBLE_THRESHOLD) {

                        if (resultCount > (page + 1) * ITEM_PER_PAGE) {
                            isLoading = true;
                            new_request = false;
                            page = page + 1;
                            Log.d("SearchResult", "Next page: " + page);
                            getResultRequest(page);
                        }
                    }
                }
            }
        });
    }

    private static class ViewHolder {

        private TextView titleText;
        private TextView authorText;
        private TextView mediaText;
        //private TextView synthText;

        private ImageView playImage;
        private ImageView wishlistImage;
        private ImageView dlImage;
        private ImageView coverImage;
    }

    class BookListAdapter extends BaseAdapter {

        ListeFiches bookList;

        BookListAdapter(ListeFiches bookList) {

            this.bookList = bookList;
        }

        private void addFromBookArray(Object [] l) {
            this.bookList.addFromBookArray(l);
        }

        @Override
        public int getCount() {
            return bookList.getCount();
        }

        @Override
        public FicheLivre getItem(int i) {
            return bookList.getItem(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            LayoutInflater li = (LayoutInflater) ScrollBookListActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
            assert (li != null);
            ScrollBookListActivity.ViewHolder holder;

            if (convertView == null) {
                switch (viewType) {
                    case "text":
                        convertView = li.inflate(R.layout.book_row_text, viewGroup, false);
                        break;
                    case "cover":
                        convertView = li.inflate(R.layout.book_row_cover, viewGroup, false);
                        break;
                    case "text_cover":
                        convertView = li.inflate(R.layout.book_row_text_cover, viewGroup, false);
                        break;
                }
                assert (convertView != null);

                holder = new ScrollBookListActivity.ViewHolder();

                if (viewType.equals("text") || viewType.equals("text_cover")) {
                    holder.titleText = convertView.findViewById(R.id.titleText);
                    holder.authorText = convertView.findViewById(R.id.authorText);
                    holder.mediaText = convertView.findViewById(R.id.mediaText);
                }

                //holder.synthText = convertView.findViewById(R.id.synthText);

                holder.playImage = convertView.findViewById(R.id.book_action_1);
                holder.wishlistImage = convertView.findViewById(R.id.book_action_2);
                holder.dlImage = convertView.findViewById(R.id.book_action_3);

                if (!viewType.equals("text")) {
                    holder.coverImage = convertView.findViewById(R.id.coverImage);
                }

                convertView.setTag(R.string.Tags_Holder, holder);
            } else {
                holder = (ScrollBookListActivity.ViewHolder) convertView.getTag(R.string.Tags_Holder);
                if (!viewType.equals("text")) {
                    holder.coverImage.setImageBitmap(null);
                }
            }

            final FicheLivre currentBook = bookList.getItem(i);
            Log.d("BOOLK", currentBook.getTitle() + " getview");
            final ImageView wlImage = holder.wishlistImage;
            final ImageView plImage = holder.playImage;
            holder.dlImage.setContentDescription(getResources().getString(R.string.Catalogue_Telecharger) + " " + currentBook.getTitle());
            holder.playImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait) + " " + getResources().getString(R.string.Global_De_livre) + " " + currentBook.getTitle());
            holder.dlImage.setEnabled(true);
            holder.dlImage.setAlpha(1f);

            View.OnClickListener wishlistListener = view -> {

                String request;
                if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                    request = "DeleteWish&bookNr=" + currentBook.getNoticeNr();
                    wlImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                    wlImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu));
                    mBib.removeWishFromList(currentBook.getNoticeNr());
                    Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuSupprime));
                } else {
                    request = "AddWish&bookNr=" + currentBook.getNoticeNr();
                    wlImage.setImageResource(R.drawable.ic_star_white_48dp);
                    wlImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu));
                    mBib.addWishToList(currentBook);
                    Utils.showToast(getApplicationContext(), currentBook.getTitle() + " " + getResources().getString(R.string.Catalogue_VoeuAjoute));
                }
                WebcallTask wt = new WebcallTask(getApplicationContext());
                wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);
            };

            View.OnClickListener extraitListener = view -> {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 200) {
                    return;
                }

                mLastClickTime = SystemClock.elapsedRealtime();

                if (currentBook.getMp3Url() != null) {
                    String mp3URL = currentBook.getMp3Url().toString();

                    if (playing) {
                        mediaPlayer.stop();
                        lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
                        lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
                        playing = false;
                        if (plImage != lastPlayedImage) {
                            plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                            plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                            lastPlayedImage = plImage;
                            playExtrait(mp3URL);
                            playing = true;
                        }

                    } else {
                        lastPlayedImage = plImage;
                        playExtrait(mp3URL);

                        plImage.setImageResource(R.drawable.ic_pause_white_48dp);
                        plImage.setContentDescription(getResources().getString(R.string.Lecteur_MettrePause));
                        playing = true;
                    }
                }
            };

            View.OnClickListener dlListener = view -> {

                mLastClickedView = view;
                final String bookName = (String) view.getTag(R.string.Tags_NoticeNr);
                final FicheLivre currentBook1;
                if (wishList != null) {
                    if (wishList.getItemByNumber(bookName) != null) {
                        currentBook1 = wishList.getItemByNumber(bookName);
                    } else {
                        currentBook1 = resultList.getItemByNumber(bookName);
                    }
                } else {
                    currentBook1 = resultList.getItemByNumber(bookName);
                }

                mBib.addFiche(currentBook1);
                if (mBib.bookInLibrary(bookName)) {
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_LivreDejaPresent));
                    return;
                }

                long freeSpace = mBib.getFreeSpace();
                long bookSize = currentBook1.getZipSize();

                if (freeSpace < bookSize * 2) {
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.Catalogue_PasAssezEspace));
                    return;
                }

                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = null;

                if (cm != null) {
                    activeNetwork = cm.getActiveNetworkInfo();
                }

                boolean isWiFi = false;
                if (activeNetwork != null) {
                    isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                }

                final Intent dlIntent = new Intent(getApplicationContext(), DownloadService.class);
                dlIntent.putExtra("zipUrl", currentBook1.getZipUrl());
                dlIntent.putExtra("noticeNr", currentBook1.getNoticeNr());
                dlIntent.putExtra("noticeNr", currentBook1.getNoticeNr());
                dlIntent.putExtra("description", currentBook1.getTitle());

                DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            startDl(dlIntent, currentBook1);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //Log.d("dialogClickListener", "Not downloading");
                            break;
                    }
                };

                if (!isWiFi) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScrollBookListActivity.this);
                    // AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setMessage(getResources().getString(R.string.Catalogue_AvertissementReseau)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialogClickListener)
                            .setNegativeButton(getResources().getString(R.string.Global_Annuler), dialogClickListener);

                    AlertDialog alert = builder.create();
                    Utils.prepareAndShowAlert(getApplicationContext(), alert);

                } else {
                    startDl(dlIntent, currentBook1);
                }
            };

            if (viewType.equals("text") || viewType.equals("text_cover")) {
                holder.titleText.setText(currentBook.getTitle());
                holder.authorText.setText(currentBook.getAuthor());
                holder.mediaText.setText(Utils.formatDuration(currentBook.getMedia(), getApplicationContext()));
                holder.mediaText.setContentDescription(getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext()));
            }

            if (!viewType.equals("text")) {
                memoryCache.loadBitmapCoverImage(holder.coverImage, currentBook.getCover(), currentBook.getNoticeNr());
            }

            /*if (currentBook.getReader().equals("Manon")) {
                holder.synthText.setText(getResources().getString(R.string.Global_Voix_SyntheseCourt));
            } else {
                holder.synthText.setText("");
            }*/

            holder.playImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());
            holder.dlImage.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            if (checkBookActivity(currentBook.getNoticeNr())) {
                Log.d("boolk", "diabling: " + currentBook.getTitle());
                holder.dlImage.setEnabled(false);
                holder.dlImage.setAlpha(0.3f);
            }

            convertView.setTag(R.string.Tags_NoticeNr, currentBook.getNoticeNr());

            holder.playImage.setOnClickListener(extraitListener);
            holder.dlImage.setOnClickListener(dlListener);
            holder.wishlistImage.setOnClickListener(wishlistListener);

            String description = currentBook.getTitle() + " " + getResources().getString(R.string.Global_De) + " " + currentBook.getAuthor() + ", " + getResources().getString(R.string.Global_Duree) + ": " + Utils.formatAccessibleDuration(currentBook.getMedia(), getApplicationContext());

            if (currentBook.getReader().equals("Manon") || currentBook.getReader().equals("Alice")) {
                description += ", " + getResources().getString(R.string.Global_Voix_Synthese);
            }

            convertView.setOnClickListener(playBookListener);
            convertView.setContentDescription(description);

            if (mBib.isOnWishList(currentBook.getNoticeNr())) {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_SupprimerVoeu) + " " + currentBook.getTitle());
            } else {
                holder.wishlistImage.setImageResource(R.drawable.ic_star_border_white_48dp);
                holder.wishlistImage.setContentDescription(getResources().getString(R.string.Catalogue_AjouterVoeu) + " " + currentBook.getTitle());
            }

            return convertView;
        }
    }

    private void playExtrait(String url) {
        if(mediaPlayer == null){
            mediaPlayer= new MediaPlayer();
        }else {
            mediaPlayer.reset();
        }

        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(MediaPlayer::start);
        } catch (IOException e) {
            Log.e("AudioService", "Exception IO");
            e.printStackTrace();
        }
    }

    public void onCompletion(MediaPlayer _mediaPlayer) {

        lastPlayedImage.setImageResource(R.drawable.ic_play_arrow_white_48dp);
        lastPlayedImage.setContentDescription(getResources().getString(R.string.Catalogue_EcouterExtrait));
        playing = false;
    }

    private void startDl(Intent intent, FicheLivre book) {

        mLastClickedView.setEnabled(false);
        mLastClickedView.setAlpha(0.3f);

        Calendar cal = Calendar.getInstance();
        book.setDownloadDate(cal.getTimeInMillis());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else{
            startService(intent);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ScrollBookListActivity.this);
        builder.setMessage(getResources().getString(R.string.Catalogue_TelechargementDe) + " " + book.getTitle() + ".\n" + getResources().getString(R.string.Catalogue_AllerTelechargements)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialog2ClickListener)
                .setNegativeButton(getResources().getString(R.string.Global_Non), dialog2ClickListener);

        AlertDialog alert = builder.create();

        Utils.prepareAndShowAlert(getApplicationContext(), alert);
    }

    private boolean checkBookActivity(String noticeNr) {

        boolean downloading = false;
        boolean alreadyThere = false;
        for (int i = 0; i < DownloadsActivity.downloads.size(); i++) {
            if (DownloadsActivity.downloads.get(i).noticeNr.equals(noticeNr)) {
                Log.d("BOOLK", noticeNr + " déjà en cours de téléchargement");
                downloading = true;
            }
        }
        if (mBib.bookInLibraryByNoticeNr(noticeNr)) {
            Log.d("BOOLK", noticeNr + " dans la biblio");
            alreadyThere = true;
        }
        return downloading || alreadyThere;

    }
}
