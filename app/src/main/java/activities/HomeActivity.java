package activities;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.InReadingFragment;
import fragments.LoginFragment;
import fragments.MyLibraryFragment;
import services.DownloadService;
import services.LecteurService;
import tasks.CleanupTask;
import utils.Bibliotheque;
import utils.HomePager;
import utils.ResizableTabLayout;
import utils.Utils;

public class HomeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    //This is our tablayout
    private static ResizableTabLayout tabLayout;
    //This is our viewPager
    private ViewPager viewPager;
    private static HomePager adapter;
    public final static String PREF_TAG = "ANDROID_BSR";
    static private LocalBroadcastManager localBM = null;
    private static String viewType = "text_cover";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        Utils.showTTToast(getApplicationContext(), "app démarrée ");
        super.onCreate(savedInstanceState);
        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home);
        Utils.isInternetAvailable(getApplicationContext(), true);
        SharedPreferences sharedPrefs = getSharedPreferences("LibrarySettings", Context.MODE_PRIVATE);
        viewType = sharedPrefs.getString("viewType", "");
        if(viewType.equals("")){
            viewType = "text_cover";
        }
        checkPermissions();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(LecteurService.CHANNEL_ID, LecteurService.name, importance);
            mChannel.setDescription(LecteurService.Description);
            mChannel.setShowBadge(false);
            mChannel.setVibrationPattern(new long[]{ 0 });
            mChannel.enableVibration(true);
            mChannel.setSound(null, null);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }

        localBM = LocalBroadcastManager.getInstance(this);

        //Initializing the tablayout
        tabLayout = findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.Accueil_LectureEnCours)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.Accueil_Bibliotheque)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.Accueil_Catalogue)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorHeight(20);
        //Initializing viewPager
        viewPager = findViewById(R.id.pager);

        //Creating our pager adapter
        adapter = new HomePager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                viewPager.setCurrentItem(position);
                tabLayout.getTabAt(position).select();

                if(position == 2){
                    LoginFragment.focusOnTitle();
                }
                if(position == 1){
                    Utils.sendTBMessage(getApplicationContext(), Bibliotheque.getBibliotheque(getApplicationContext()).size() + " livres dans votre bibliothèque.");
                    MyLibraryFragment.focusOnTitle();
                    Utils.isInternetAvailable(getApplicationContext(), true);
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getIntExtra("tabToSelect", 0)>0) {
                tabLayout.getTabAt(intent.getIntExtra("tabToSelect", 0)).select();
            }
        }

        if (Bibliotheque.getBibliotheque(getApplicationContext()).size() == 0) {
            if (intent != null) {
                tabLayout.getTabAt(intent.getIntExtra("tabToSelect", 2)).select();
            }
        }

        if (Bibliotheque.getBibliotheque(getApplicationContext()).getLivresEnLecture().size() == 0) {
            if (intent != null) {
                tabLayout.getTabAt(intent.getIntExtra("tabToSelect", 1)).select();
            }
        }

        CleanupTask ct = new CleanupTask(getApplicationContext());
        ct.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        // cancel the sleep timer if it was still active
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("sleepDuration", 0);
        editor.apply();

        if(!sharedPrefs.getBoolean("showInReading", true)){
            ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0).setVisibility(View.GONE);
            tabLayout.getTabAt(1).select();
        }

    }


    public static void showInReading(){
        ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0).setVisibility(View.VISIBLE);
    }
    public static void hideInReading(){
        ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0).setVisibility(View.GONE);
    }
    public static String getViewType() {
        return viewType;
    }

    public static void setViewType(String s) {
        viewType = s;
    }

    /* Initiate pin Shortcut */
    @TargetApi(26)
    public static void initShortCutManager(final Context mContext, boolean forceCreate) {

        ShortcutManager mShortcutManager = mContext.getSystemService(ShortcutManager.class);

        if (mShortcutManager != null ? mShortcutManager.isRequestPinShortcutSupported() : false) {

            ShortcutInfo pinShortcutInfo;
            if (mShortcutManager.getPinnedShortcuts().size() < 1 || forceCreate) {
                Log.d("Shortcut", "creating");
                Intent intentShortcutActivity = new Intent(Intent.ACTION_MAIN, Uri.EMPTY, mContext, LecteurActivity.class);
                intentShortcutActivity.putExtra("action", "unconditional_play");
                pinShortcutInfo =
                        new ShortcutInfo.Builder(mContext, "shortcutToPlayer")
                                .setShortLabel(mContext.getString(R.string.Accueil_Shortcut_Short))
                                .setLongLabel(mContext.getString(R.string.Accueil_Shortcut_Long))
                                .setIcon(Icon.createWithResource(mContext, R.drawable.ic_play_arrow_black_48dp))
                                .setIntent(intentShortcutActivity)
                                .build();

                // Create the PendingIntent object only if your app needs to be notified
                // that the user allowed the shortcut to be pinned. Note that, if the
                // pinning operation fails, your app isn't notified. We assume here that the
                // app has implemented a method called createShortcutResultIntent() that
                // returns a broadcast intent.
                Intent pinnedShortcutCallbackIntent =
                        mShortcutManager.createShortcutResultIntent(pinShortcutInfo);

                // Configure the intent so that your app's broadcast receiver gets
                // the callback successfully.For details, see PendingIntent.getBroadcast().
                PendingIntent successCallback = PendingIntent.getBroadcast(mContext, 0,
                        pinnedShortcutCallbackIntent, PendingIntent.FLAG_IMMUTABLE);

                mShortcutManager.requestPinShortcut(pinShortcutInfo,
                        successCallback.getIntentSender());
            } else {
                // Shortcut already exists, asks if still wants add
                final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                initShortCutManager(mContext, true);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                // on y retourne
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(mContext.getResources().getString(R.string.Accueil_ConfirmationShortcut)).setPositiveButton(mContext.getResources().getString(R.string.Global_Oui), dialogClickListener)
                        .setNegativeButton(mContext.getResources().getString(R.string.Global_Non), dialogClickListener);

                AlertDialog alert = builder.create();
                Utils.prepareAndShowAlert(mContext, alert);
            }
        }
    }

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // do nothing
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.Accueil_QuitterApplication)).setPositiveButton(getResources().getString(R.string.Global_Oui), dialogClickListener)
                    .setNegativeButton(getResources().getString(R.string.Global_Non), dialogClickListener);

            AlertDialog alert = builder.create();
            try {
                Utils.prepareAndShowAlert(this, alert);
            } catch(Exception e) {
                // aucune idée mais c'est arrivé 1x...
            }

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

        switch (which){
            case DialogInterface.BUTTON_POSITIVE:
                sendToService("closeApp");

                DownloadService.cancellAll(getApplicationContext());
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask();
                } else{
                    finish();
                }

                break;

            case DialogInterface.BUTTON_NEGATIVE:
                // on y retourne
                break;
        }
        }
    };

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        assert imm != null;

        if (getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        if (tab.getPosition() == 0) {
            //InReadingFragment irFragment = (InReadingFragment) adapter.getItem(0);
            //irFragment.updateList();
            InReadingFragment.focusOnTitle();

        }

        if (tab.getPosition() == 1) {
            // MyLibraryFragment mlFragment = (MyLibraryFragment) adapter.getItem(1);
            // mlFragment.updateList();
            MyLibraryFragment.focusOnTitle();
        }

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    public void checkPermissions() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(getApplicationContext(),
                permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) ||
            (ContextCompat.checkSelfPermission(getApplicationContext(),
                permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.POST_NOTIFICATIONS}, 1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == 1) if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Bibliotheque.getBibliotheque(getApplicationContext()).scanLivres();

        }
    }

    private void sendToService(String action) {

        if (LecteurService.isRunning) {
            sendLocalBroadCast(action);
        }
    }

    private static void sendLocalBroadCast(String intentName) {

        Intent intent = new Intent(intentName);
        localBM.sendBroadcast(intent);
    }

    public static void updateAllLists() {

        if (adapter == null) {
            return;
        }
        MyLibraryFragment mlFragment = (MyLibraryFragment) adapter.getItem(1);

        if(mlFragment != null) {
            mlFragment.updateList();
        }

        InReadingFragment irFragment = (InReadingFragment) adapter.getItem(0);

        if(irFragment != null) {
            irFragment.updateList();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*public void manageMemoryAccess(){
        Log.d("MEMORYY", "package:" + BuildConfig.APPLICATION_ID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R ) {

            Intent intent2 = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
            final int APP_STORAGE_ACCESS_REQUEST_CODE = 501; // Any value
            startActivityForResult(intent2, APP_STORAGE_ACCESS_REQUEST_CODE);
        }
    }*/
}
