package activities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.math.BigDecimal;

import bibliothequesonore.ch.livresbsrandroid.R;
import fragments.InReadingFragment;
import services.LecteurService;
import utils.Bibliotheque;
import utils.Chapitre;
import utils.LivreDaisy;
import utils.NotificationReceiver;
import utils.SimpleTwoFingerDoubleTapDetector;
import utils.Utils;

public class LecteurActivity extends AppCompatActivity {

    private final static int TITLE_MAX_LENGTH = 150;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            if ((action != null) && action.equals("update_ui")) {
                Log.d("LCT", "updatefrom mMessageReceiver" + mCurrentBook.getCurrentPosition());
                updateUI();
            }

            if (action != null && action.equals("cancel_player")) {
                finish();
            }
        }
    };

    public final static String PREF_TAG = "ANDROID_BSR";
    private ImageView playImage;

    MenuItem currentSpeedItem;
    MenuItem speedMenuItem;
    MenuItem levelMenuItem;
    MenuItem jumpMenuItem;
    MenuItem toneMenuItem;
    MenuItem sleepMenuItem;

    private TextView titleView;
    private TextView chapterView;
    private TextView chapterPosView;

    private TextView timePosView;

    private LivreDaisy mCurrentBook;
    private Bibliotheque mBib;
    private boolean wasPlayingBeforeMenu;
    static private LocalBroadcastManager localBM = null;
    private static IntentFilter remoteFilter;
    NotificationReceiver notifReceiver;

    private static AudioManager audioManager;
    public static boolean talkbackOn = false;

    private boolean contenuOpened = false;
    private long lastMenuClosed;
    private long lastMenuOpened;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lecteur);
        Toolbar myToolbar = findViewById(R.id.player_toolbar);
        setSupportActionBar(myToolbar);

        // if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_prev_toolbar_black_48dp);
            getSupportActionBar().setHomeActionContentDescription("Retour à l'accueil");
        // }
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

    }

    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            String action = (String) view.getTag();

            switch (action) {
                case "play":
                    sendToService("play");
                    wasPlayingBeforeMenu = false;
                    Log.d("play", "onClick: play détecté");
                    break;

                case "next":
                    if (LecteurService.isRunning) {
                        sendToService("next");
                    } else {
                        Chapitre nextChapitre = mCurrentBook.getNextChapitre(false);
                        if (nextChapitre != null) {
                            mCurrentBook.setCurrentChapitre(mCurrentBook.getNextChapitre(false));
                            Log.d("LCT", "updatefrom clicklistener1" + mCurrentBook.getCurrentPosition());
                            updateUI();
                        }
                    }
                    break;

                case "prev":

                    if (LecteurService.isRunning) {
                        sendToService("prev");
                    } else {
                        Chapitre prevChapitre = mCurrentBook.getPreviousChapitre();
                        if (prevChapitre != null) {
                            mCurrentBook.setCurrentChapitre(mCurrentBook.getPreviousChapitre());
                        } else {
                            mCurrentBook.setAbsolutePosition(0);
                        }
                        Log.d("LCT", "updatefrom clicklistener2" + mCurrentBook.getCurrentPosition());
                        updateUI();
                    }
                    break;

                case "jumpPrev":
                    sendToService("jumpPrev");
                    break;

                case "jumpNext":
                    sendToService("jumpNext");
                    break;

                case "tonality":
                case "mSpeed":
                case "sleep":
                    break;

            }
        }
    };

    private void sendToService(String action) {
        sendToService(action, "");
    }

    private void sendToService(String action, String parameter) {

        if(LecteurService.isRunning) {
            sendLocalBroadCast(action, parameter);
        } else {
            LecteurService.isRunning = true;
            Intent lecteurServiceIntent = new Intent(getApplicationContext(), LecteurService.class);
            if(!action.equals("")){
                lecteurServiceIntent.putExtra("action", action);
            }
            if(!parameter.equals("")){
                lecteurServiceIntent.putExtra("new_chapter", parameter);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(lecteurServiceIntent);
            }else{
                startService(lecteurServiceIntent);
            }
        }
    }

    private static void sendLocalBroadCast(String intentName, String parameter) {

        Intent intent = new Intent(intentName);
        if (!parameter.equals("")) {
            intent.putExtra("new_chapter", parameter);
        }

        localBM.sendBroadcast(intent);
    }

    private void showToc() {

        contenuOpened = true;
        if(LecteurService.isPlaying) {
            wasPlayingBeforeMenu = true;
            sendToService("play");
        }

        Intent tocIntent = new Intent(getApplicationContext(), ContenuActivity.class);
        tocIntent.putExtra("bookName", mCurrentBook.getId());
        startActivityForResult(tocIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            String chapterId = data.getStringExtra("chapterId");
            if (chapterId == null) {
                long signetPos = data.getLongExtra("position", 0);
                if (signetPos != 0) {
                    sendToService("goto_position", signetPos+"");
                }
            } else {
                sendToService("goto_chapter", chapterId);
            }
            focusOnPlayButton();
        } else {
            if (wasPlayingBeforeMenu) {
                sendToService("play");
                Log.d("MNUS", "relance contenu");

            }

            wasPlayingBeforeMenu = false;
            contenuOpened = false;
        }
    }

    protected void updateSpeed(float newValue) {

        mCurrentBook.setSpeed(newValue);
        Intent intent = new Intent("update_setting");
        intent.putExtra("update_setting", "speed");
        intent.putExtra("new_value", newValue);
        localBM.sendBroadcast(intent);

//        sauvegarde d'une vitesse par défaut, à implémenter
//        SharedPreferences prefs = getApplicationContext().getSharedPreferences("ANDROID_BSR", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putFloat("DefaultSpeed", newValue);
//        editor.apply();
//        float defaultSpeed = prefs.getFloat("DefaultSpeed", 0);
//        Log.d("defaultSpeed", "defaultSpeed : " + defaultSpeed);

    }

    protected void updateTonality(float newValue) {

        mCurrentBook.setPitch(newValue);
        toneMenuItem.setTitle(getResources().getString(R.string.Lecteur_TonaliteVoix)+": "+getPitchText(mCurrentBook.getPitch()));
        Intent intent = new Intent("update_setting");
        intent.putExtra("update_setting", "tonality");
        intent.putExtra("new_value", newValue);
        localBM.sendBroadcast(intent);
    }

    private void updateLevel(int newlevel) {

        mCurrentBook.setNavigLevel(newlevel);
        levelMenuItem.setTitle(getResources().getString(R.string.Lecteur_NiveauNavigation)+": "+mCurrentBook.getNavigLevel());
    }

    protected void updateJumpSize(int newValue) {

        mCurrentBook.setJumpSize(newValue);
        jumpMenuItem.setTitle(getResources().getString(R.string.Lecteur_SautTemporel)+": "+getJumpSizeText(mCurrentBook.getJumpSize()));
    }

    protected void updateSleep(int newDuration) {
        Log.d("Snooze", "updateSleep: "+newDuration);
        if(newDuration == 0){
            sleepMenuItem.setTitle(getResources().getString(R.string.Lecteur_MiseEnSommeil)+": "+ getResources().getString(R.string.Lecteur_Desactivee));
        } else {
            sleepMenuItem.setTitle(getResources().getString(R.string.Lecteur_MiseEnSommeil)+": " + newDuration + getResources().getString(R.string.Global_Minutes));
        }

        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("sleepDuration", newDuration);
        editor.apply();

        Intent intent = new Intent("update_setting");
        intent.putExtra("update_setting", "sleep");
        intent.putExtra("new_value", newDuration);
        localBM.sendBroadcast(intent);

    }

    private void updateUI() {

        // je ne sais pas dans quel cas, mais ça arrive qu'il soit nul, peut-être en rapport avec la notification-lecteur
        if(mCurrentBook == null){
            return;
        }
        boolean reading = mCurrentBook.isPlaying();
        String progressPercent = round(((float)mCurrentBook.getCurrentPosition() / (float)mCurrentBook.getDuration()*100), 1)+"%";
        timePosView.setText(progressPercent);
        timePosView.setContentDescription(getResources().getString(R.string.Global_Progression)+progressPercent);
        titleView.setText( Utils.truncate(mCurrentBook.getTitle(), TITLE_MAX_LENGTH));
        titleView.setContentDescription(getResources().getString(R.string.Global_Titre)+mCurrentBook.getTitle());
        chapterView.setText(mCurrentBook.getCurrentChapitreTitle());
        chapterView.setContentDescription(getResources().getString(R.string.Lecteur_Section)+mCurrentBook.getCurrentChapitreTitle());
        chapterPosView.setText((getResources().getString(R.string.Global_Duree) + " " + LivreDaisy.posToLongTexte(mCurrentBook.getDuration(), false)));
        Log.d("POSITION", mCurrentBook.getCurrentPosition()+"/"+(float)mCurrentBook.getCurrentPosition());
        if (mCurrentBook.getCurrentPosition() == 0) {
            Log.d("POSITION", "a");
            chapterPosView.setContentDescription(getResources().getString(R.string.Lecteur_Debut)+", "+getResources().getString(R.string.Global_Duree)+LivreDaisy.posToLongTexte(mCurrentBook.getDuration(), true));
        } else {
            Log.d("POSITION", getResources().getString(R.string.Global_Position)+LivreDaisy.posToDescription(mCurrentBook.getCurrentPosition(), false) + getResources().getString(R.string.Global_Sur) + LivreDaisy.posToDescription(mCurrentBook.getDuration(), false));
            chapterPosView.setContentDescription(getResources().getString(R.string.Global_Position)+LivreDaisy.posToDescription(mCurrentBook.getCurrentPosition(), true) + getResources().getString(R.string.Global_Sur) + LivreDaisy.posToDescription(mCurrentBook.getDuration(), false));
            // chapterPosView.setContentDescription("bonjour");
        }

        if (!reading) {
            if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
                playImage.setImageResource(R.drawable.ic_play_arrow_black_48dp);
            }else {
                playImage.setImageResource(R.drawable.ic_play_arrow_black_big_48dp);
            }
            //playImage.setContentDescription(getString(R.string.Lecteur_Ecouter));

        } else {
            if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
                playImage.setImageResource(R.drawable.ic_pause_black_48dp);
            } else {
                playImage.setImageResource(R.drawable.ic_pause_black_big_48dp);
            }
            //playImage.setContentDescription(getString(R.string.Lecteur_MettrePause));
        }
    }


    @Override
    protected void onNewIntent(Intent intents) {

        super.onNewIntent(intents);
        setIntent(intents);
    }

    @Override
    protected void onResume() {

        super.onResume();

        if (localBM == null) {
            localBM = LocalBroadcastManager.getInstance(this);
        }

        if (mBib == null) {
            mBib = Bibliotheque.getBibliotheque(getApplicationContext());
        }
        if (audioManager == null) {
            audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        }

        IntentFilter filter = new IntentFilter("track_done");
        filter.addAction("update_ui");
        filter.addAction("cancel_player");

        // catch intent play for pinned shortcut
        Intent intent = getIntent();

        if (intent != null) {

            int bookName = intent.getIntExtra("bookName", 0);
            if (bookName != 0) {
                mBib.setActiveLivre(bookName);
            }
        }

        mCurrentBook = mBib.getActiveLivre();

        if(mCurrentBook == null){
            onBackPressed();
        }
        if (intent != null) {
            String action = intent.getStringExtra("action");
            if (action != null && !action.equals("")) {
                intent.putExtra("action", "");
                Log.d("Shortcut", action);
                if (mCurrentBook != null) {
                    sendToService(action);
                } else {
                    Log.d("Shortcut", "No active book");
                    this.finish();
                    Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(homeIntent);
                    return;
                }
            }
        }

        if (localBM != null) {
            localBM.registerReceiver(mMessageReceiver, filter);
        }

        initPlayer();
        updateUI();
        sendToService("update_service");

        AccessibilityManager accessManager = (AccessibilityManager) getApplicationContext()
                .getSystemService(Service.ACCESSIBILITY_SERVICE);
        if (accessManager != null) {
            talkbackOn = accessManager.isEnabled() || accessManager.isTouchExplorationEnabled();
        }
        Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_EcouteDuLivre)+" "+mCurrentBook.getTitle());

        // relancer lecture seulement si on est pas dans le cas d'un sous-menu (onpanelclosed suivi direct d'un onmenuopened).

        focusOnPlayButton();
    }

    @Override
    protected void onPause() {

        super.onPause();
        mBib.storeLivres();
        InReadingFragment.update();
        localBM.unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onDestroy(){
        Log.d("onDestroy", "onDestroy");

        if(remoteFilter!= null){
            unregisterReceiver(notifReceiver);
            remoteFilter = null;
        }

        super.onDestroy();
    }

    private void initPlayer(){

        titleView = findViewById(R.id.bookTitle);
        chapterView= findViewById(R.id.currentChapter);
        chapterPosView= findViewById(R.id.duration);
        timePosView= findViewById(R.id.progression);

        playImage = findViewById(R.id.skipForward);
        ImageView nextImage = findViewById(R.id.nextButton);
        ImageView prevImage = findViewById(R.id.prevButton);

        ImageView jumpNextImage = findViewById(R.id.nextJumpButton);
        ImageView jumpPrevImage = findViewById(R.id.prevJumpButton);

        //bookmarkButton = (Button) findViewById(R.id.bookmarkButton);
        //newBMButton = (Button) findViewById(R.id.newBMButton);

        playImage.setTag("play");
        prevImage.setTag("prev");
        nextImage.setTag("next");

        jumpNextImage.setTag("jumpNext");
        jumpPrevImage.setTag("jumpPrev");

        //bookmarkButton.setTag("bookmarks");
        //newBMButton.setTag("newBookmark");

        playImage.setOnClickListener(clickListener);
        nextImage.setOnClickListener(clickListener);
        prevImage.setOnClickListener(clickListener);

        //bookmarkButton.setOnClickListener(clickListener);
        //newBMButton.setOnClickListener(clickListener);

        jumpNextImage.setOnClickListener(clickListener);
        jumpPrevImage.setOnClickListener(clickListener);
    }

    // override to prevent crash with physical menu button on some phones.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_MENU) {
            // do nothing
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    // overrides backpress
    @Override
    public void onBackPressed() {

        Log.d("CDA", "onBackPressed Called");

        //if(isTaskRoot()) {
            Intent setIntent = new Intent(Intent.ACTION_MAIN, Uri.EMPTY, getApplicationContext(), HomeActivity.class);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(setIntent);
       //} else{
            super.onBackPressed();
        //}
    }

    public static float round(float d, int decimalPlace) {

        try {
            BigDecimal bd = new BigDecimal(Float.toString(d));
            bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
            return bd.floatValue();
        } catch(NumberFormatException nfe) {
            Log.e("LecteurActivity", "round: valeur incorrecte: "+d);
            return 0.0f;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d("MENU", "oncreateoptionsmenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.player_actions, menu);

        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();
            spanString.setSpan(new RelativeSizeSpan(3f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);
        }

        speedMenuItem = menu.findItem(R.id.action_speed);
        speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");

        float speed = mCurrentBook.getSpeed();
        MenuItem speedSelection = null;

        if (speed == 0.5f) {
            speedSelection = menu.findItem(R.id.speed_50);
        }
        if (speed == 0.6f) {
            speedSelection = menu.findItem(R.id.speed_60);
        }
        if (speed == 0.75f) {
            speedSelection = menu.findItem(R.id.speed_75);
        }
        if (speed == 0.9f) {
            speedSelection = menu.findItem(R.id.speed_90);
        }
        if (speed == 1.0f) {
            speedSelection = menu.findItem(R.id.speed_100);
        }
        if (speed == 1.15f) {
            speedSelection = menu.findItem(R.id.speed_115);
        }
        if (speed == 1.25f) {
            speedSelection = menu.findItem(R.id.speed_125);
        }
        if (speed == 1.5f) {
            speedSelection = menu.findItem(R.id.speed_150);
        }
        if (speed == 2.0f) {
            speedSelection = menu.findItem(R.id.speed_200);
        }
        if (speed == 2.5f) {
            speedSelection = menu.findItem(R.id.speed_250);
        }
        if (speed == 3.0f) {
            speedSelection = menu.findItem(R.id.speed_300);
        }

        if (speedSelection != null) {
            currentSpeedItem = speedSelection;
            speedSelection.setCheckable(true);
            speedSelection.setChecked(true);
        }

        levelMenuItem = menu.findItem(R.id.action_navig_level);
        levelMenuItem.setTitle(getResources().getString(R.string.Lecteur_NiveauNavigation)+": "+mCurrentBook.getNavigLevel());

        jumpMenuItem = menu.findItem(R.id.action_jump_size);
        jumpMenuItem.setTitle(getResources().getString(R.string.Lecteur_SautTemporel)+": "+getJumpSizeText(mCurrentBook.getJumpSize()));

        toneMenuItem = menu.findItem(R.id.action_tone);
        toneMenuItem.setTitle(getResources().getString(R.string.Lecteur_TonaliteVoix)+": "+getPitchText(mCurrentBook.getPitch()));

        SharedPreferences prefs = getApplicationContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        int duration = prefs.getInt("sleepDuration", 0);

        sleepMenuItem = menu.findItem(R.id.action_sleep);
        if(duration != 0) {
            sleepMenuItem.setTitle(getResources().getString(R.string.Lecteur_MiseEnSommeil) + ": " + duration + " " + getResources().getString(R.string.Global_Minutes));
        }else{
            sleepMenuItem.setTitle(getResources().getString(R.string.Lecteur_MiseEnSommeil) + ": " + getResources().getString(R.string.Lecteur_Desactivee));
        }

        return super.onCreateOptionsMenu(menu);
    }

    private String getPitchText(Float pitch){

        if (pitch ==  0.9f) {
            return getResources().getString(R.string.Lecteur_PlusGrave);
        }
        if (pitch ==  0.95f) {
            return getResources().getString(R.string.Lecteur_Grave);
        }
        if (pitch ==  1.0f) {
            return getResources().getString(R.string.Lecteur_Neutre);
        }
        if (pitch ==  1.1f) {
            return getResources().getString(R.string.Lecteur_Aigu);
        }
        if (pitch ==  1.2f) {
            return getResources().getString(R.string.Lecteur_PlusAigu);
        }
        return "";
    }

    private String getJumpSizeText(int jumpSize){

        if (jumpSize < 60) {
            return jumpSize+" "+getResources().getString(R.string.Global_Secondes);
        } else if (jumpSize == 60) {
            return 1+" "+getResources().getString(R.string.Global_Minute);
        } else {
            return jumpSize+" "+getResources().getString(R.string.Global_Minutes);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.speed_50:
                updateSpeed(0.5f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;
            case R.id.speed_60:
                updateSpeed(0.6f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;
            case R.id.speed_75:
                updateSpeed(0.75f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_90:
                updateSpeed(0.90f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_100:
                updateSpeed(1f);
                if(currentSpeedItem != null){
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;
            case R.id.speed_115:
                updateSpeed(1.15f);
                if(currentSpeedItem != null){
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;
            case R.id.speed_125:
                updateSpeed(1.25f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_150:
                updateSpeed(1.5f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_175:
                updateSpeed(1.75f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_200:
                updateSpeed(2f);
                if(currentSpeedItem != null){ currentSpeedItem.setCheckable(false); }
                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_225:
                updateSpeed(2.25f);
                if(currentSpeedItem != null){ currentSpeedItem.setCheckable(false); }
                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_250:
                updateSpeed(2.5f);
                if (currentSpeedItem != null) {
                    currentSpeedItem.setCheckable(false);
                }
                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.speed_300:
                updateSpeed(3f);
                if(currentSpeedItem != null){
                    currentSpeedItem.setCheckable(false);
                }

                item.setCheckable(true);
                item.setChecked(true);
                currentSpeedItem = item;
                speedMenuItem.setTitle(getResources().getString(R.string.Lecteur_VitesseLecture)+": "+mCurrentBook.getSpeed()*100+"%");
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_VitesseLecture)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+mCurrentBook.getSpeed()*100+"%");
                break;

            case R.id.action_content:
                contenuOpened = true;
                showToc();
                break;

            case R.id.sleep_0:
                updateSleep(0);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_MiseEnSommeil)+" "+ getResources().getString(R.string.Lecteur_Desactivee));
                break;

            case R.id.sleep_15:
                updateSleep(15);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_MiseEnSommeil)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 15 "+getResources().getString(R.string.Global_Minutes));
                break;

            case R.id.sleep_30:
                updateSleep(30);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_MiseEnSommeil)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 30 "+getResources().getString(R.string.Global_Minutes));
                break;

            case R.id.sleep_45:
                updateSleep(45);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_MiseEnSommeil)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 45 "+getResources().getString(R.string.Global_Minutes));
                break;

            case R.id.sleep_60:
                updateSleep(60);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_MiseEnSommeil)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 1 "+getResources().getString(R.string.Global_Heure));
                break;

            case R.id.tone_m1:
                updateTonality(0.95f);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_TonaliteVoix)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Lecteur_Grave));
                break;

            case R.id.tone_m2:
                updateTonality(0.9f);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_TonaliteVoix)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Lecteur_PlusGrave));
                break;

            case R.id.tone_0:
                updateTonality(1.0f);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_TonaliteVoix)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Lecteur_Neutre));
                break;

            case R.id.tone_p1:
                updateTonality(1.1f);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_TonaliteVoix)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Lecteur_Aigu));
                break;

            case R.id.tone_p2:
                updateTonality(1.2f);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_TonaliteVoix)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Lecteur_PlusAigu));
                break;

            case R.id.jump_15:
                updateJumpSize(15);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_SautTemporel)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 15 " +getResources().getString(R.string.Global_Secondes));
                break;

            case R.id.jump_30:
                updateJumpSize(30);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_SautTemporel)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 30 " +getResources().getString(R.string.Global_Secondes));
                Utils.sendTBMessage(this, "Saut dans le temps réglé sur: 30 secondes");
                break;

            case R.id.jump_60:
                updateJumpSize(60);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_SautTemporel)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 1 " +getResources().getString(R.string.Global_Minute));
                break;

            case R.id.jump_300:
                updateJumpSize(300);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_SautTemporel)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 5 " +getResources().getString(R.string.Global_Minutes));
                break;

            case R.id.jump_600:
                updateJumpSize(600);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_SautTemporel)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 10 " +getResources().getString(R.string.Global_Minutes));
                break;

            case R.id.level_1:
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 1");
                updateLevel(1);
                break;

            case R.id.level_2:
                updateLevel(2);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 2");
                break;

            case R.id.level_3:
                updateLevel(3);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 3");
                break;

            case R.id.level_4:
                updateLevel(4);
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 4");
                break;

            case R.id.level_5:
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 5");
                updateLevel(5);
                break;

            case R.id.level_6:
                Utils.sendTBMessage(this, getResources().getString(R.string.Lecteur_NiveauNavigation)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": 6");
                updateLevel(6);
                break;

            case R.id.action_bookmark:
                mCurrentBook.addSignet();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.Lecteur_SignetAjoute));
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }

        focusOnPlayButton();
        return true;
    }

    public boolean onMenuOpened(int featureId, Menu menu) {

        boolean result = super.onMenuOpened(featureId, menu);

        if (menu != null ) {//|| menu.getItem(0).toString().equals(getResources().getString(R.string.Lecteur_Contenu))){
            lastMenuOpened = System.currentTimeMillis();
            if (LecteurService.isPlaying) {
                wasPlayingBeforeMenu = true;
                sendToService("pause");
            }
        }
        return result;
    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {

        if (wasPlayingBeforeMenu && !contenuOpened) {
            lastMenuClosed = System.currentTimeMillis();

            final Handler handler = new Handler();

            // relancer lecture seulement si on est pas dans le cas d'un sous-menu (onpanelclosed suivi direct d'un onmenuopened).
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(Math.abs(lastMenuOpened-lastMenuClosed) > 80 ) {
                        focusOnPlayButton();
                        if (wasPlayingBeforeMenu) {
                            sendToService("play");
                            wasPlayingBeforeMenu = false;
                        }
                    }
                }
            }, 100);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        int level = mCurrentBook.getMaxLevel();
        MenuItem item;
        String niveauText = getResources().getString(R.string.Lecteur_Niveau);
        String minutesText = getResources().getString(R.string.Global_Minutes);
        String minuteText = getResources().getString(R.string.Global_Minute);
        String secondsText = getResources().getString(R.string.Global_Secondes);

        item = menu.findItem(R.id.level_6);
        item.setTitle(niveauText +" 6");
        item = menu.findItem(R.id.level_5);
        item.setTitle(niveauText +" 5");
        item = menu.findItem(R.id.level_4);
        item.setTitle(niveauText +" 4");
        item = menu.findItem(R.id.level_3);
        item.setTitle(niveauText +" 3");
        item = menu.findItem(R.id.level_2);
        item.setTitle(niveauText +" 2");
        item = menu.findItem(R.id.level_1);
        item.setTitle(niveauText +" 1");

        item = menu.findItem(R.id.sleep_15);
        item.setTitle("15 "+minutesText);
        item = menu.findItem(R.id.sleep_30);
        item.setTitle("30 "+minutesText);
        item = menu.findItem(R.id.sleep_45);
        item.setTitle("45 "+minutesText);
        item = menu.findItem(R.id.sleep_60);
        item.setTitle("60 "+minutesText);

        item = menu.findItem(R.id.jump_15);
        item.setTitle("15 "+secondsText);
        item = menu.findItem(R.id.jump_30);
        item.setTitle("30 "+secondsText);
        item = menu.findItem(R.id.jump_60);
        item.setTitle("1 "+minuteText);
        item = menu.findItem(R.id.jump_300);
        item.setTitle("5 "+minutesText);
        item = menu.findItem(R.id.jump_600);
        item.setTitle("10 "+minutesText);

        if (level < 6) {
            item = menu.findItem(R.id.level_6);
            item.setVisible(false);
        }
        if (level < 5) {
            item = menu.findItem(R.id.level_5);
            item.setVisible(false);
        }
        if (level < 4) {
            item = menu.findItem(R.id.level_4);
            item.setVisible(false);
        }
        if (level < 3) {
            item = menu.findItem(R.id.level_3);
            item.setVisible(false);
        }
        if (level < 2) {
            item = menu.findItem(R.id.level_2);
            item.setVisible(false);
        }

        super.onPrepareOptionsMenu(menu);

        return true;
    }

    SimpleTwoFingerDoubleTapDetector multiTouchListener = new SimpleTwoFingerDoubleTapDetector() {
        @Override
        public void onTwoFingerDoubleTap() {
            sendToService("play");
        }
    };

    private void focusOnPlayButton(){

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playImage.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 500);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(multiTouchListener.onTouchEvent(event)) {
            return true;
        }

        return super.onTouchEvent(event);
    }
}