package fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;

import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Bibliotheque;
import utils.HomeBookListAdapter;

public class InReadingFragment extends ListFragment{
    @SuppressLint("StaticFieldLeak")
    private static TextView titleLabel;
    @SuppressLint("StaticFieldLeak")
    private static InReadingFragment theFragment;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        titleLabel = (TextView) getActivity().findViewById(R.id.inReadingTitle);
        Bibliotheque mBib = Bibliotheque.getBibliotheque(getContext());
        HomeBookListAdapter odA = new HomeBookListAdapter(mBib.getLivresEnLecture(), getContext(), 0);
        setListAdapter(odA);
        theFragment = this;
    }
    @Override
    public void onResume(){
        super.onResume();
        updateList();
        focusOnTitle();
    }

    public static void update(){
        if(theFragment != null)
            theFragment.updateList();
    }

    public void updateList(){
        try {
            HomeBookListAdapter odA = new HomeBookListAdapter(Bibliotheque.getBibliotheque(getContext()).getLivresEnLecture(), getContext(), 0);
            setListAdapter(odA);
            if(isAdded()) {
                if (Bibliotheque.getBibliotheque(getContext()).getLivresEnLecture().size() == 0) {
                    titleLabel.setContentDescription(getResources().getString(R.string.Accueil_PasLivreEnLecture));
                } else {
                    titleLabel.setContentDescription(getResources().getString(R.string.Accueil_LectureEnCours));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e("HomeActivity", "exception dans update list à l'affichage des livres en cours.");
        }
        focusOnTitle();
    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_in_reading, container, false);
    }

    public static void focusOnTitle(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(titleLabel != null) {
                    titleLabel.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                }
            }
        }, 100);
    }

}