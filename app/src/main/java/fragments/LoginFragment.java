package fragments;

import static activities.BookListActivity.WCT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import activities.BookListActivity;
import activities.GenresActivity;
import activities.SearchActivity;
import bibliothequesonore.ch.livresbsrandroid.R;
import tasks.WebcallTask;
import utils.Auditeur;
import utils.Bibliotheque;
import utils.JSonManager;
import utils.ListeFiches;
import utils.Utils;

public class LoginFragment extends Fragment {

    public final static String PREF_TAG = "ANDROID_BSR";

    private RelativeLayout loginLayout;
    private RelativeLayout bsrLayout;
    @SuppressLint("StaticFieldLeak")
    protected static Button loginButton;
    protected static Button trialButton;

    @SuppressLint("StaticFieldLeak")
    static private EditText loginText;
    @SuppressLint("StaticFieldLeak")
    static private EditText passwordText;
    private TextView auditeurLabel;
    @SuppressLint("StaticFieldLeak")
    static private TextView introLabel;
    @SuppressLint("StaticFieldLeak")
    private static TextView titleLabel;
    private static TextView versionLabel;
    private static Button mVotationsButton;
    private boolean clickedLogin = false;
    private Auditeur mAuditeur;
    Bibliotheque mBib;
    @SuppressLint("StaticFieldLeak")
    private static Context mContext;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initWSClient();
        mContext = getContext();
        mBib = Bibliotheque.getBibliotheque(getContext());
        loginLayout = (RelativeLayout) getView().findViewById(R.id.loginFragment);
        bsrLayout = (RelativeLayout) getView().findViewById(R.id.bsrFragment);

        loginButton = (Button) getView().findViewById(R.id.connexionButton);
        loginButton.setOnClickListener(loginListener);


        trialButton = (Button) getView().findViewById(R.id.trialButton);
        trialButton.setOnClickListener(view -> {

            Intent intent;
            intent = new Intent(getContext(), BookListActivity.class);
            intent.putExtra("listType", ListeFiches.TRIAL_TYPE);

            startActivity(intent);

        });

        loginText = (EditText) getView().findViewById(R.id.loginText);
        passwordText = (EditText) getView().findViewById(R.id.passwordText);

        introLabel = (TextView) getView().findViewById(R.id.introText);
        introLabel.setPaintFlags(introLabel.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        introLabel.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.bibliothequesonore.ch"));
            startActivity(browserIntent);
        });

        auditeurLabel = (TextView) getView().findViewById(R.id.nomAuditeurText);
        versionLabel = (TextView) getView().findViewById(R.id.appVersion);

        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            versionLabel.setText("V. "+pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Button logoutButton = (Button) getView().findViewById(R.id.disconnectButton);
        logoutButton.setOnClickListener(logoutListener);

        titleLabel = (TextView) getActivity().findViewById(R.id.catalogTitle);

        SharedPreferences prefs = getContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        String lastUsername = prefs.getString("username", null);
        String lastPassword = prefs.getString("password", null);

        loginText.setText(lastUsername);
        passwordText.setText(lastPassword);

        disableLogin();

        passwordText.setOnEditorActionListener(SoftKBListener);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Button searchButton = (Button) getView().findViewById(R.id.searchButton);
        Button wishListButton = (Button) getView().findViewById(R.id.wishListButton);
        Button genreNewsButton = (Button) getView().findViewById(R.id.genreNewsButton);
        mVotationsButton = (Button) getView().findViewById(R.id.votationsButton);
        Button mAudioDescButton = (Button) getView().findViewById(R.id.audioDescButton);

        searchButton.setOnClickListener(menuListener);
        wishListButton.setOnClickListener(menuListener);
        genreNewsButton.setOnClickListener(menuListener);
        mVotationsButton.setOnClickListener(menuListener);
        mAudioDescButton.setOnClickListener(menuListener);

        final View activityRootView = getActivity().findViewById(R.id.main_layout);
        final View pagerView = getActivity().findViewById(R.id.tabLayout);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - r.height();
                if (heightDiff > 0.25*activityRootView.getRootView().getHeight()) { // if more than 25% of the screen, its probably a keyboard...
                    introLabel.setVisibility(View.GONE);
                    pagerView.setVisibility(View.GONE);
                } else{
                    introLabel.setVisibility(View.VISIBLE);
                    pagerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clickedLogin = true;
            hideKeyboard((AppCompatActivity) getActivity());
            login();
        }

    };

    View.OnClickListener logoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SharedPreferences.Editor editor = getContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
            editor.putString("password", "");
            editor.putString("username", "");
            editor.putString("cookie", "");
            editor.putString("auditeur", "");
            loginText.setText("");
            passwordText.setText("");
            editor.apply();

            loginLayout.setVisibility(View.VISIBLE);
            bsrLayout.setVisibility(View.GONE);
            Utils.sendTBMessage(getActivity().getApplicationContext(), mAuditeur.getFirstname()+ " "+ mAuditeur.getLastname()+ " " +getResources().getString(R.string.Connexion_AEteDeconnecte));
        }

    };

    private void login(){
        loginButton.setEnabled(false);

        SharedPreferences prefs = getContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);
        String appVersion = prefs.getString("app_version", "");
        String androidVersion = prefs.getString("android_version", "");

        String login = loginText.getText().toString();
        String password = passwordText.getText().toString();
        storeLogin(login, password);

        String[] paramsList = new String[3];
        paramsList[0]= login;
        paramsList[1]= passwordText.getText().toString();
        paramsList[2]= "BSR_android_v"+appVersion + "_android_"+androidVersion;

        StringBuilder params = new StringBuilder();
        for (int i=0; i < paramsList.length ; i++){
            params.append("&p").append(i).append("=").append(paramsList[i]);
        }
        String request = "Authenticate"+params;

        WebcallTask wt = new WebcallTask(getContext(), handler);
        wt.execute(request);
    }



    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @SuppressLint("SetTextI18n")
        @Override
        public void handleMessage(Message msg) {
            String mUserInfo = (String) msg.obj;

            if(mUserInfo ==null){
                Log.e("LoginActivity", "login jsonstring null");

                loginLayout.setVisibility(View.VISIBLE);
                loginButton.setEnabled(true);
                return;
            }
            if(mUserInfo.equals("login_failed") || !JSonManager.getError(mUserInfo).equals("")){
                if(clickedLogin) {
                    Utils.showToast(getContext(), getResources().getString(R.string.Connexion_ConnexionEchouee));
                    // Utils.showSnack(loginButton, "hello world échoué");
                    clickedLogin = false;
                } else{
                    loginLayout.setVisibility(View.VISIBLE);
                }
                loginButton.setEnabled(true);
                return;
            }
            if (msg.what == WCT) {
                String jsonString = (String) msg.obj;
                if (!JSonManager.getError(jsonString).equals("")) {
                    Utils.showToast(getContext(), "Erreur: " + JSonManager.getError(jsonString));
                    return;
                } else {
                    JSONArray jBookArray = JSonManager.getResultArray(jsonString, "GetWishes");
                    if(jBookArray != null) {
                        if (jBookArray.size() == 0) {
                            return;
                        }
                        Object[] wishesArray = jBookArray.toArray();
                        Bibliotheque.setWishList(new ListeFiches(wishesArray));
                        return;
                    }
                }
            }

            // initialise la liste de voeux
            WebcallTask wt = new WebcallTask(getContext(), handler);
            wt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "GetWishes");

            loginButton.setEnabled(true);
            loginLayout.setVisibility(View.GONE);
            bsrLayout.setVisibility(View.VISIBLE);

            JSONObject auditeurObject = JSonManager.getResultNode(mUserInfo, "Authenticate");
            mAuditeur = new Auditeur(auditeurObject);
            auditeurLabel.setText(mAuditeur.getFirstname()+ " "+ mAuditeur.getLastname());
            if(clickedLogin) {
                Utils.sendTBMessage(getActivity().getApplicationContext(), getResources().getString(R.string.Accueil_Bienvenue) + mAuditeur.getFirstname());
            }
            if(!mAuditeur.getCountry().equals("Suisse")){
                mVotationsButton.setVisibility(View.GONE);
            }
            focusOnTitle();


        }
    };
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null) {
            Utils.isInternetAvailable(getActivity().getApplicationContext(), true);
        }
    }
    @Override
    public void onResume() {
        super.onResume();

        mBib.login(handler);
        focusOnTitle();
    }

    public static void enableLogin(){
        try {
            if(loginButton == null){
                return;
            }
            loginButton.setEnabled(true);
            loginText.setEnabled(true);
            passwordText.setEnabled(true);

            loginText.setAlpha(1f);
            loginButton.setAlpha(1f);
            passwordText.setAlpha(1f);

            introLabel.setText(mContext.getResources().getString(R.string.Connexion_Explication));

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void disableLogin(){
        try{
            loginButton.setEnabled(false);
            loginText.setEnabled(false);
            passwordText.setEnabled(false);

            loginText.setAlpha(0.3f);
            loginButton.setAlpha(0.3f);
            passwordText.setAlpha(0.3f);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    private void storeLogin(String username, String password){
        SharedPreferences.Editor editor = getContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.apply();
    }

    private void initWSClient() {
        String versionName = "";
        try {
            versionName = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
            Log.i("LoginActivity", "version: "+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("LoginActivity", "No version code found, returning -1");
        }
        String androidVersion = android.os.Build.VERSION.RELEASE;
        SharedPreferences.Editor editor = getContext().getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE).edit();
        editor.putString("app_version",  versionName);
        editor.putString("android_version",  androidVersion);
        editor.apply();
    }

    TextView.OnEditorActionListener SoftKBListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            int result = actionId & EditorInfo.IME_MASK_ACTION;

            switch (result) {
                case EditorInfo.IME_ACTION_DONE:
                case EditorInfo.IME_ACTION_GO:
                case EditorInfo.IME_ACTION_SEND:
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    clickedLogin = true;
                    hideKeyboard((AppCompatActivity) getActivity());
                    login();
                    break;
                case EditorInfo.IME_ACTION_NEXT:
                    /*InputMethodManager imm2 = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(getActivity().getCurrentFocus() != null) {
                        imm2.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    }*/
                    break;
            }
            return true;
        }
    };

    public static void hideKeyboard( AppCompatActivity activity ) {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService( Context.INPUT_METHOD_SERVICE );
        View f = activity.getCurrentFocus();
        if( null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom( f.getClass() ) )
            imm.hideSoftInputFromWindow( f.getWindowToken(), 0 );
        else
            activity.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
    }

    View.OnClickListener menuListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            Intent intent;
            switch(view.getId()){
                case R.id.searchButton:
                    intent = new Intent(getContext().getApplicationContext(), SearchActivity.class);
                    startActivity(intent);
                    break;
                case R.id.wishListButton:
                    intent = new Intent(getContext().getApplicationContext(), BookListActivity.class);
                    intent.putExtra("listType", ListeFiches.WISHES_TYPE);
                    startActivity(intent);
                    break;
                case R.id.genreNewsButton:
                    intent = new Intent(getContext().getApplicationContext(), GenresActivity.class);
                    startActivity(intent);
                    break;
                case R.id.votationsButton:
                    intent = new Intent(getContext().getApplicationContext(), BookListActivity.class);
                    intent.putExtra("listType", ListeFiches.VOTATIONS_TYPE);
                    startActivity(intent);
                    break;

                case R.id.audioDescButton:
                    intent = new Intent(getContext().getApplicationContext(), BookListActivity.class);
                    intent.putExtra("listType", ListeFiches.FILMS_TYPE);
                    startActivity(intent);

                    break;

            }
        }
    };

    public static void focusOnTitle(){
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if(titleLabel != null) {
                titleLabel.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
            }
        }, 100);
    }

}