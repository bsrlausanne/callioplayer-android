package fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Bibliotheque;
import utils.Chapitre;
import utils.LivreDaisy;

public class TocFragment extends ListFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bibliotheque mBib = Bibliotheque.getBibliotheque(getContext());

        LivreDaisy currentBook = mBib.getActiveLivre();

        ChapterListAdapter clA = new ChapterListAdapter(currentBook.getChapters());
        setListAdapter(clA);

    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_toc, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    class ChapterListAdapter extends BaseAdapter {
        ArrayList<Chapitre> mChapitres;

        ChapterListAdapter(ArrayList<Chapitre> chapitres) {
            this.mChapitres = chapitres;
        }

        @Override
        public int getCount() {
            return mChapitres.size();
        }

        @Override
        public Chapitre getItem(int i) {
            return mChapitres.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            assert li != null;
            @SuppressLint("ViewHolder") View bookRowView = li.inflate(R.layout.layout_item_contenu, viewGroup, false);
            TextView titleText = (TextView) bookRowView.findViewById(R.id.tocChapterView);
            TextView durationText = (TextView) bookRowView.findViewById(R.id.tocDurationView);

            final Chapitre currentChapitre = getItem(i);

            int level = currentChapitre.getNiveau();
            titleText.setPadding(10+(level-1)*50, 10, 10, 10);

            titleText.setText(currentChapitre.getTitle());
            titleText.setContentDescription(currentChapitre.getTitle()+". "+getResources().getString(R.string.Global_Duree)+": "+LivreDaisy.posToDescription(currentChapitre.getDuration(), true)+": "+getResources().getString(R.string.Lecteur_Niveau)+currentChapitre.getNiveau());
            durationText.setText(LivreDaisy.posToShortTexte(currentChapitre.getDuration()));
            //String description =currentChapitre.getTexteDurée().replace("0:0", "").replace("0:", "");
            //description = description.replace(":", getResources().getString(R.string.Global_Minutes)).concat(getResources().getString(R.string.Global_Secondes)) ;
            durationText.setContentDescription(LivreDaisy.posToDescription(currentChapitre.getDuration(), true));
            bookRowView.setTag(currentChapitre.getId());

            View.OnClickListener playChapterListener = new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("chapterId", ""+view.getTag());

                    getActivity().setResult(Activity.RESULT_OK, returnIntent);
                    getActivity().finish();
                }

            };
            bookRowView.setOnClickListener(playChapterListener);

            return bookRowView;
        }
    }

}

