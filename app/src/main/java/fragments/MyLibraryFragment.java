package fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageButton;
import androidx.appcompat.widget.PopupMenu;
import android.widget.TextView;

import activities.DownloadsActivity;
import activities.HomeActivity;
import activities.LecteurActivity;
import bibliothequesonore.ch.livresbsrandroid.BuildConfig;
import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Bibliotheque;
import utils.HomeBookListAdapter;
import utils.Utils;

public class MyLibraryFragment extends ListFragment {
    @SuppressLint("StaticFieldLeak")
    static ImageButton continueButton;
    @SuppressLint("StaticFieldLeak")
    static ImageButton downloadsButton;

    @SuppressLint("StaticFieldLeak")
    private static TextView titleLabel;
    @SuppressLint("StaticFieldLeak")
    private static MyLibraryFragment theFragment;
    private SharedPreferences sharedPrefs;
    public static boolean internetOn = false;
    private PopupMenu popup;
    private MenuPopupHelper menuHelper;
    private HomeBookListAdapter odA;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        final Bibliotheque mBib = Bibliotheque.getBibliotheque(getContext());
        odA = new HomeBookListAdapter(mBib.getAllLivres(), getContext(), 1);
        setListAdapter(odA);

        sharedPrefs = getContext().getSharedPreferences("LibrarySettings", Context.MODE_PRIVATE);

        String sortBy = sharedPrefs.getString("sortBy", "");
        String viewType = sharedPrefs.getString("viewType", "");

        if(!viewType.equals("")){
            HomeActivity.setViewType(viewType);
        }else{
            HomeActivity.setViewType("text_cover");
        }
        if(sortBy.equals("dl_date") || sortBy.equals("")){
            HomeBookListAdapter.setSortBy(R.id.search_dl_date);
        }
        if(sortBy.equals("title")){
            HomeBookListAdapter.setSortBy(R.id.search_title);
        }
        if(sortBy.equals("author")){
            HomeBookListAdapter.setSortBy(R.id.search_author);
        }
        if(sortBy.equals("")){
            HomeBookListAdapter.setSortBy(R.id.search_dl_date);
        }

        titleLabel = getActivity().findViewById(R.id.libraryTitle);

        downloadsButton = getActivity().findViewById(R.id.downloadsButton);

        continueButton= getActivity().findViewById(R.id.continueButton);

        downloadsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dlIntent = new Intent(getContext(), DownloadsActivity.class);
                dlIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(dlIntent);
            }
        });
        downloadsButton.setEnabled(false);
        downloadsButton.setAlpha(0.3f);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBib.getActiveLivre() == null){
                    Utils.showToast(getContext(), getResources().getString(R.string.Accueil_PasLivreEnLecture));
                    return;
                }
                Intent lecteurIntent = new Intent(getContext(), LecteurActivity.class);
                lecteurIntent.putExtra("action", "unconditional_play");
                startActivity(lecteurIntent);
            }
        });

        theFragment = this;

        // More options menu
        final ImageButton moreOptionButton = getActivity().findViewById(R.id.moreOptions);
        moreOptionButton.setContentDescription(getResources().getString(R.string.Accueil_Parametres));
        popup = new PopupMenu(getContext(), moreOptionButton);
        popup.getMenuInflater()
                .inflate(R.menu.library_menu, popup.getMenu());

        if(sharedPrefs.getBoolean("showInReading", true)){
            popup.getMenu().getItem(2).setTitle(R.string.Accueil_CacherEnLecture);
        }else{
            popup.getMenu().getItem(2).setTitle(R.string.Accueil_MontrerEnLecture);
        }
        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                SharedPreferences.Editor editor = sharedPrefs.edit();
                int id = item.getItemId();
                if (id == R.id.search_dl_date || id == R.id.search_author || id == R.id.search_title ) {
                    // Sorting books
                    HomeBookListAdapter.setSortBy(id);

                    switch (id) {
                        case R.id.search_dl_date:
                            editor.putString("sortBy", "dl_date");
                            Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_OrdreTri)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_DateTelechargement));
                            break;
                        case R.id.search_author:
                            editor.putString("sortBy", "author");
                            Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_OrdreTri)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_Auteur));
                            break;
                        case R.id.search_title:
                            editor.putString("sortBy", "title");
                            Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_OrdreTri)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_Titre));
                            break;
                    }
                    editor.apply();
                    Log.d("UPDATELIST", "onmenuclicklistener ");
                    updateList();

                    return true;
                } else if (id == R.id.create_shortcut) {
                    // Shortcut creator
                    HomeActivity.initShortCutManager(getContext(), false);
                    return true;
                } else if (id == R.id.refresh) {
                    Bibliotheque.getBibliotheque(getContext()).scanLivres();
                }/*else if (id == R.id.manage_memory) {
                    (   (HomeActivity)getActivity()).manageMemoryAccess();
                }*/
                else if (id == R.id.inreading_toggle) {
                    if(sharedPrefs.getBoolean("showInReading", true)){
                        item.setTitle(R.string.Accueil_MontrerEnLecture);
                        HomeActivity.hideInReading();
                        editor.putBoolean("showInReading", false);
                        editor.commit();
                    } else{
                        item.setTitle(R.string.Accueil_CacherEnLecture);
                        HomeActivity.showInReading();
                        editor.putBoolean("showInReading", true);
                        editor.commit();
                    }

                    return true;
                    /*} else if (id == R.id.qr_scanner) {
                    //Scan QR code
                    startActivity(new Intent(getContext(), QRCodeScannerActivity.class));
                    return true;*/
                } else if (id == R.id.view_grid_text || id == R.id.view_grid_mixed ){ //|| id == R.id.view_grid_image) {

                    if (id == R.id.view_grid_text) {
                        editor.putString("viewType", "text");
                        HomeActivity.setViewType("text");
                        Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_View)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_View_Grid_Text));

                    } else if (id == R.id.view_grid_mixed) {
                        HomeActivity.setViewType("text_cover");
                        editor.putString("viewType", "text_cover");
                        Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_View)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_View_Grid_Mixed));

                    } else {
                        HomeActivity.setViewType("cover");
                        editor.putString("viewType", "cover");
                        Utils.sendTBMessage(getContext(), getResources().getString(R.string.Accueil_View)+" "+getResources().getString(R.string.Lecteur_RegleSur)+": "+getResources().getString(R.string.Accueil_View_Grid_Cover));
                    }
                    InReadingFragment.update();

                    editor.apply();

                    refreshFragment();
                }

                return false;
            }
        });
        // Making sure Icons show up
        menuHelper = new MenuPopupHelper(getContext(), (MenuBuilder) popup.getMenu(), moreOptionButton);
        moreOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuHelper.setForceShowIcon(true);
                menuHelper.show();
            }
        });
        //updateList();
    }

    private void refreshFragment() {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_library, container, false);
    }

    public static void update(){
        Log.d("UPDATELIST", "mylibraryfragment update");
        if(theFragment != null)
            theFragment.updateList();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(DownloadsActivity.downloads.size() == 0){
            disableDownloadsButton();
        } else{
            enableDownloadsButton();
        }

        focusOnTitle();
    }

    public void updateList(){
        // contrôle que le fragement est bien attaché à l'activité avant de faire qqchose dessus
        if(isAdded() ) {
            Log.d("MyLibraryFragment", "refresh de la liste.");
            try {
                odA.updateList();

                if (Bibliotheque.getBibliotheque(getContext()).getAllLivres().size() == 0) {
                    titleLabel.setContentDescription(getContext().getResources().getString(R.string.Accueil_PasLivreSurAppareil));
                } else {
                    titleLabel.setContentDescription(getContext().getResources().getString(R.string.Accueil_Bibliotheque));
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("MyLibraryFragment", "exception dans update list à l'affichage de la bibliotheque.");
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public static void enableDownloadsButton(){
        Log.d("MyLibrary", "enableDownloadsButton");
        if(downloadsButton != null) {
            downloadsButton.setEnabled(true);
            downloadsButton.setAlpha(1f);
        }
    }

    public static void disableDownloadsButton(){
        Log.d("MyLibrary", "disableDownloadsButton");
        if(downloadsButton != null) {
            downloadsButton.setEnabled(false);
            downloadsButton.setAlpha(0.3f);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public static void focusOnTitle(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(titleLabel != null) {
                    titleLabel.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_FOCUSED);
                }
            }
        }, 100);
    }
}

