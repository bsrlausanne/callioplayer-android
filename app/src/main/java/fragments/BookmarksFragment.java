package fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.ListFragment;

import java.util.ArrayList;

import bibliothequesonore.ch.livresbsrandroid.R;
import utils.Bibliotheque;
import utils.LivreDaisy;
import utils.Signet;
import utils.Utils;

public class BookmarksFragment extends ListFragment {
    private static LivreDaisy mCurrentBook;
    @SuppressLint("StaticFieldLeak")
    private static BookmarksFragment theFragment;
    private static long posToDelete;
    @SuppressLint("StaticFieldLeak")
    private static Context mContext;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getContext();
        Bibliotheque mBib = Bibliotheque.getBibliotheque(getContext());

        mCurrentBook = mBib.getActiveLivre();
        theFragment = this;
        updateList();
    }

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bookmarks, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    static class BookmarksAdapter extends BaseAdapter {
        ArrayList<Signet> mBookmarks;

        BookmarksAdapter( ArrayList<Signet> bookmarks) {
            mBookmarks = bookmarks;
        }

        @Override
        public int getCount() {
            return mBookmarks.size();
        }

        @Override
        public Signet getItem(int i) {
            return mBookmarks.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater li = (LayoutInflater) theFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            @SuppressLint("ViewHolder") View bookmarkView = li.inflate(R.layout.layout_bookmark, viewGroup, false);
            TextView titleText = (TextView) bookmarkView.findViewById(R.id.bmTitle);
            TextView positionText = (TextView) bookmarkView.findViewById(R.id.bmPosition);
            ImageButton deleteButton = (ImageButton) bookmarkView.findViewById(R.id.signetDelete);

            final Signet currentSignet = getItem(i);

            titleText.setText(currentSignet.title);
            positionText.setText(LivreDaisy.posToShortTexte(currentSignet.position));

            bookmarkView.setContentDescription(Bibliotheque.getString(R.string.Global_A)+", "+LivreDaisy.posToDescription(currentSignet.position, true)+"; "+currentSignet.title);
            bookmarkView.setTag(currentSignet.position);
            deleteButton.setTag(currentSignet.position);
            deleteButton.setContentDescription(Bibliotheque.getString(R.string.Lecteur_SupprimerSignet)+" "+Bibliotheque.getString(R.string.Global_A)+LivreDaisy.posToLongTexte(currentSignet.position, true)+"; "+currentSignet.title);

            View.OnClickListener playChapterListener = new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("position", (long)view.getTag());
                    theFragment.getActivity().setResult(Activity.RESULT_OK, returnIntent);
                    theFragment.getActivity().finish();
                }

            };
            bookmarkView.setOnClickListener(playChapterListener);

            final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            mCurrentBook.removeSignet(posToDelete);
                            BookmarksFragment.updateList();
                            Utils.sendTBMessage(mContext, mContext.getResources().getString(R.string.Lecteur_SuppressionSignetConfirme)+" "+mContext.getResources().getString(R.string.Global_A)+" "+LivreDaisy.posToDescription(currentSignet.position, true));
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            // on y retourne
                            break;
                    }}};

            View.OnClickListener deleteListener = new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    posToDelete = (long)view.getTag();

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(mContext.getResources().getString(R.string.Lecteur_ConfirmationSuppressionSignet)+"?").setPositiveButton(mContext.getResources().getString(R.string.Global_Oui), dialogClickListener)
                            .setNegativeButton(mContext.getResources().getString(R.string.Global_Non), dialogClickListener);

                    AlertDialog alert = builder.create();
                    Utils.prepareAndShowAlert(mContext, alert);

                }

            };
            deleteButton.setOnClickListener(deleteListener);
            return bookmarkView;
        }
    }

    private static void updateList(){
        BookmarksAdapter bmA = new BookmarksAdapter(mCurrentBook.getSignets());
        theFragment.setListAdapter(bmA);
    }
}

