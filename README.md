# README #

Cette application android a été developée avec Android Studio pour windows.

La description du produit final sur le playstore:
[playstore](https://play.google.com/store/apps/details?id=bibliothequesonore.ch.livresbsrandroid&hl=fr)

Il s'agit en bref d'un lecteur audio spécialisé ([format DAISY](http://www.daisy.org/daisypedia/daisy-digital-talking-book)) avec accès authentifié à [notre collection de livres audio](http://www.bibliothequesonore.ch/). Il permet de lire en principe n'importe quel livre daisy placé sur l'appareil. 

Il a été réalisé par un relatif débutant en développement android qui supporte la critique: sschule@bibliothequesonore.ch.

Egalement débutant en contrôle de source, alors je peux garantir que tous les fichiers sources écrit pour le projet sont disponibles ici, mais pas forcément tous les fichiers du projet android studio.

Le code est disponible ici pour d'autres institutions qui souhaiteraient faire quelque chose de comparable et qui auraient l'utilité d'un exemple fonctionnel.

Dépendances:

Gson pour le parsing du webservice 

https://github.com/fangyidong/json-simple/tree/master/src/main/java/org/json/simple


Exoplayer pour la base de la lecture audio

https://github.com/google/ExoPlayer


Librairie de modification de la vitesse et du pitch de lecture

https://github.com/waywardgeek/sonic


L'application se connecte à notre webservice qui récupère les données de notre bibliothèque. Son code n'est pas public. 

La base de données des livres utilise [Apache Solr](http://lucene.apache.org/solr/), et le webservice transmet les requêtes directement.